<?php

//Super Admin

// Home
Breadcrumbs::for('admin.home', function ($trail) {
    $trail->push('Dashboard', route('admin.home'));
});

// User
Breadcrumbs::for('admin.user', function ($trail) {
    $trail->push('Pengguna', route('admin.user.index'));
});
// User/ Create
Breadcrumbs::for('admin.user.create', function ($trail) {
    $trail->parent('admin.user');
    $trail->push('Tambah', route('admin.user.create'));
});

// Department
Breadcrumbs::for('admin.department', function ($trail) {
    $trail->push('Jurusan', route('admin.department.index'));
});
// Department/ Create
Breadcrumbs::for('admin.department.create', function ($trail) {
    $trail->parent('admin.department');
    $trail->push('Tambah', route('admin.department.create'));
});
// Department/ Edit -
Breadcrumbs::for('admin.department.edit', function ($trail, $department) {
    $trail->parent('admin.department');
    $trail->push('Ubah', route('admin.department.edit', $department->id));
});

// Organization
Breadcrumbs::for('admin.organization', function ($trail) {
    $trail->push('Organisasi', route('admin.organization.index'));
});
// Organization/ Create
Breadcrumbs::for('admin.organization.create', function ($trail) {
    $trail->parent('admin.organization');
    $trail->push('Tambah', route('admin.organization.create'));
});
// Organization/ Edit -
Breadcrumbs::for('admin.organization.edit', function ($trail, $organization) {
    $trail->parent('admin.organization');
    $trail->push('Ubah data', route('admin.organization.edit', $organization->id));
});

// Event
Breadcrumbs::for('admin.events', function ($trail) {
    $trail->push('Kegiatan', route('admin.events.index'));
});
// Event/ Create
Breadcrumbs::for('admin.events.create', function ($trail) {
    $trail->parent('admin.events');
    $trail->push('Tambah', route('admin.events.create'));
});
// Event/ Edit -
Breadcrumbs::for('admin.events.edit', function ($trail, $events) {
    $trail->parent('admin.events');
    $trail->push('Ubah', route('admin.events.edit', $events->id));
});

//Mahasiswa

// Home
Breadcrumbs::for('mahasiswa.home', function ($trail) {
    $trail->push('Dashboard', route('mahasiswa.home'));
});
// Proposal
// Get Option
Breadcrumbs::for('mahasiswa.proposal.option', function ($trail) {
    $trail->push('Pilih jenis proposal', route('mahasiswa.proposal.option'));
});
// Create
Breadcrumbs::for('mahasiswa.proposal.create', function ($trail) {
    $trail->parent('mahasiswa.proposal.option');
    $trail->push('Pengajuan proposal', route('mahasiswa.proposal.create'));
});
// Proposal on Progress
Breadcrumbs::for('mahasiswa.proposal.index', function ($trail) {
    $trail->push('Data proposal', route('mahasiswa.proposal.index'));
});
// Proposal/ Edit
Breadcrumbs::for('mahasiswa.proposal.edit', function ($trail, $proposal) {
    $trail->parent('mahasiswa.proposal.index');
    $trail->push('Ubah proposal', route('mahasiswa.proposal.edit', $proposal->id));
});
// Proposal/ Tim Edit
Breadcrumbs::for('mahasiswa.proposal.edit.team', function ($trail, $proposal_id) {
    $trail->parent('mahasiswa.proposal.index');
    $trail->push('Ubah tim', route('mahasiswa.proposal.edit.team', $proposal_id));
});
// Proposal/ Documentation
Breadcrumbs::for('documentation.index', function ($trail, $proposalId) {
    $trail->parent('mahasiswa.proposal.index');
    $trail->push('Dokumentasi', route('documentation.index', $proposalId));
});
// Proposal/ History
Breadcrumbs::for('proposal.history.index', function ($trail) {
    if(Auth::user()->hasRole('Mahasiswa')){
        $trail->parent('mahasiswa.proposal.index');
    }
    $trail->push('Riwayat proposal', route('proposal.history.index'));
});
// Proposal/ Documentation/ Create
Breadcrumbs::for('mahasiswa.documentation.create', function ($trail, $proposalId) {
    $trail->parent('documentation.index', $proposalId);
    $trail->push('Tambah Dokumentasi', route('mahasiswa.documentation.create', $proposalId));
});
// Proposal/ Achievement
Breadcrumbs::for('achievement.index', function ($trail, $proposalId) {
    $trail->parent('mahasiswa.proposal.index');
    $trail->push('Penghargaan', route('achievement.index', $proposalId));
});
// Proposal/ Achievement/ Create
Breadcrumbs::for('mahasiswa.achievement.create', function ($trail, $proposalId) {
    $trail->parent('achievement.index', $proposalId);
    $trail->push('Tambah Penghargaan', route('mahasiswa.achievement.create', $proposalId));
});
// LPJ
Breadcrumbs::for('mahasiswa.lpj.create', function ($trail, $id) {
    $trail->parent('mahasiswa.proposal.index');
    $trail->push('Laporan pertanggungjawaban', route('mahasiswa.lpj.create', $id));
});
// Achievement/ Create
Breadcrumbs::for('mahasiswa.achievement-request', function ($trail) {
    $trail->push('Ajukan Prestasi', route('mahasiswa.achievement-request'));
});

//Dekan

// Home
Breadcrumbs::for('dekan.home', function ($trail) {
    $trail->push('Dashboard', route('dekan.home'));
});
// Home/ User History
Breadcrumbs::for('dekan.user.history.index', function ($trail, $proposalId) {
    $trail->parent('dekan.home');
    $trail->push('Riwayat pengajuan', route('dekan.user.history.index', $proposalId));
});

//Wakil Dekan II

// Home
Breadcrumbs::for('wadek2.home', function ($trail) {
    $trail->push('Dashboard', route('wadek2.home'));
});

//Wakil Dekan III

// Home
Breadcrumbs::for('wadek3.home', function ($trail) {
    $trail->push('Dashboard', route('wadek3.home'));
});

// Achievements
Breadcrumbs::for('user-achievements', function ($trail) {
    $trail->push('Data prestasi', route('user-achievements'));
});

//Kepala Bagian TU

// Home
Breadcrumbs::for('kabag.home', function ($trail) {
    $trail->push('Dashboard', route('kabag.home'));
});

//Kepala Sub Bagian Keuangan

// Home
Breadcrumbs::for('kasubag.home', function ($trail) {
    $trail->push('Dashboard', route('kasubag.home'));
});
// Permintaan Surat
Breadcrumbs::for('kasubag.mail.request', function ($trail) {
    $trail->push('Permintaan surat', route('kasubag.mail.request'));
});

//Sekretaris Dekan

// Home
Breadcrumbs::for('sekdek.home', function ($trail) {
    $trail->push('Dashboard', route('sekdek.home'));
});
// Mail Request
Breadcrumbs::for('sekdek.mail.request', function ($trail) {
    $trail->push('Permintaan surat', route('sekdek.mail.request'));
});
// Mail Request/ Create
Breadcrumbs::for('sekdek.mail.create', function ($trail, $proposal) {
    $trail->parent('sekdek.mail.request');
    $trail->push('Create', route('sekdek.mail.create', $proposal->id));
});
// Proposal Review Request
Breadcrumbs::for('sekdek.proposal.review', function ($trail) {
    $trail->push('Review', route('sekdek.proposal.review'));
});
// Proposal Review On-review
Breadcrumbs::for('sekdek.proposal.on-review', function ($trail) {
    $trail->parent('sekdek.proposal.review');
    $trail->push('Dalam proses revisi', route('sekdek.proposal.on-review'));
});
// Proposal Review Detail
Breadcrumbs::for('sekdek.proposal.review.detail', function ($trail, $proposalId) {
    $trail->parent('sekdek.proposal.review');
    $trail->push('Detail', route('sekdek.proposal.review.detail', $proposalId));
});


//Dekan, WD II, Kabag, Kasubag

// Disposisi
Breadcrumbs::for('disposition.index', function ($trail) {
    $trail->push('Disposisi proposal', route('disposition.index'));
});
// Department/ Detail
Breadcrumbs::for('disposition.show', function ($trail, $proposal_id) {
    $trail->parent('disposition.index');
    $trail->push('Detail', route('disposition.show', $proposal_id));
});
// Department/ Detail
Breadcrumbs::for('disposition.history.index', function ($trail) {
    $trail->parent('disposition.index');
    $trail->push('Riwayat disposisi', route('disposition.history.index'));
});

// Kasubag & Sekretaris Dekan

// Surat
Breadcrumbs::for('mail.index', function ($trail) {
    $trail->push('Permintaan surat', route('mail.index'));
});

// Surat/ Revisi
Breadcrumbs::for('mail.show.revision', function ($trail, $mail) {
    $trail->parent('mail.index');
    $trail->push('Revisi', route('mail.show.revision', $mail->id));
});

// Surat/ Riwayat
Breadcrumbs::for('mail.history.index', function ($trail) {
    if(Auth::user()->hasRole('Dekan')){
        $trail->parent('mail.index');
    }
    $trail->push('Riwayat surat', route('mail.history.index'));
});

// Kasubag

//LPJ
Breadcrumbs::for('kasubag.lpj.index', function ($trail) {
    $trail->push('Laporan pertanggungjawaban', route('kasubag.lpj.index'));
});

//LPJ/ revision or history
Breadcrumbs::for('kasubag.lpj.revision', function ($trail) {
    $trail->parent('kasubag.lpj.index');
    if(Request::get('status') === 'revision'){
        $trail->push('Data revisi LPJ', route('kasubag.lpj.index'));
    }else{
        $trail->push('Data riwayat LPJ', route('kasubag.lpj.index'));
    }
});

//LPJ/ Riwayat Proposal
Breadcrumbs::for('kasubag.lpj.history', function ($trail) {
    $trail->parent('kasubag.lpj.index');
    $trail->push('Riwayat', route('proposal.history.index'));
});

// LPJ/ Documentation
Breadcrumbs::for('documentation.kasubag.index', function ($trail) {
    $trail->parent('kasubag.lpj.index');
    $trail->push('Dokumentasi');
});
// LPJ/ Achievement
Breadcrumbs::for('achievement.kasubag.index', function ($trail) {
    if(Auth::user()->hasRole('Kasubag')){
        $trail->parent('kasubag.lpj.index');
    }
    $trail->push('Penghargaan');
});