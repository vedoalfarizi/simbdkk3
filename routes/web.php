<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/users-json', 'ProposalController@getStudentsJson');

Route::group(['middleware' => ['role:Super Admin'], 'prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'SuperAdmin'], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/users', 'UserController@index')->name('user.index');
    Route::post('/users', 'UserController@store')->name('user.store');
    Route::get('/users/create', 'UserController@create')->name('user.create');
    Route::get('/users/{user}', 'UserController@edit')->name('user.edit');
    Route::put('/users/{user}', 'UserController@update')->name('user.update');
    Route::put('/users/{user}/status', 'UserController@updateStatus')->name('user.update.status');
    Route::delete('/users/{user}/destroy', 'UserController@destroy')->name('user.destroy');

    Route::get('/departments', 'DepartmentController@index')->name('department.index');
    Route::post('/departments', 'DepartmentController@store')->name('department.store');
    Route::get('/departments/create', 'DepartmentController@create')->name('department.create');
    Route::get('/departments/{department}', 'DepartmentController@edit')->name('department.edit');
    Route::put('/departments/{department}', 'DepartmentController@update')->name('department.update');
    Route::delete('/departments/{department}/destroy', 'DepartmentController@destroy')->name('department.destroy');

    Route::get('/organizations', 'OrganizationController@index')->name('organization.index');
    Route::post('/organizations', 'OrganizationController@store')->name('organization.store');
    Route::get('/organizations/create', 'OrganizationController@create')->name('organization.create');
    Route::get('/organizations/{organization}', 'OrganizationController@edit')->name('organization.edit');
    Route::put('/organizations/{organization}', 'OrganizationController@update')->name('organization.update');
    Route::delete('/organizations/{organization}/destroy', 'OrganizationController@destroy')->name('organization.destroy');

    Route::get('/events', 'EventController@index')->name('events.index');
    Route::post('/events', 'EventController@store')->name('events.store');
    Route::get('/events/create', 'EventController@create')->name('events.create');
    Route::get('/events/{event}', 'EventController@edit')->name('events.edit');
    Route::put('/events/{event}', 'EventController@update')->name('events.update');
    Route::delete('/events/{event}/destroy', 'EventController@destroy')->name('events.destroy');
});

Route::group(['middleware' => ['role:Mahasiswa'], 'prefix' => 'mahasiswa', 'as' => 'mahasiswa.'], function () {
    Route::get('/home', 'MahasiswaController@index')->name('home');

    Route::get('/proposals', 'ProposalController@index')->name('proposal.index');
    Route::get('/proposals/create-option', 'ProposalController@getOption')->name('proposal.option');
    Route::get('/proposals/create', 'ProposalController@create')->name('proposal.create');
    Route::post('/proposals', 'ProposalController@store')->name('proposal.store');
    Route::get('/proposals/{proposal}', 'ProposalController@edit')->name('proposal.edit');
    Route::put('/proposals/{proposal}', 'ProposalController@update')->name('proposal.update');
    Route::get('/proposals/{proposal}/team', 'ProposalController@editTeam')->name('proposal.edit.team');
    Route::put('/proposals/{proposal}/team', 'ProposalController@updateTeam')->name('proposal.update.team');
    Route::put('/proposals/{proposal}/revision', 'ProposalController@confirmRevision')->name('proposal.revision');
    Route::delete('/proposals/{proposal}/destroy', 'ProposalController@destroy')->name('proposal.destroy');

    Route::get('/lpj/{proposal}/create', 'LpjController@create')->name('lpj.create');
    Route::post('/lpj', 'LpjController@store')->name('lpj.store');
    Route::get('/lpj/{proposal}/edit', 'LpjController@edit')->name('lpj.edit');
    Route::put('/lpj/{proposal}/edit', 'LpjController@update')->name('lpj.update');

    Route::get('/documentation/{proposal}/create', 'DocumentationController@create')->name('documentation.create');
    Route::post('/documentation/{proposal}', 'DocumentationController@store')->name('documentation.store');
    Route::delete('/documentation/{proposal}/destroy/{photo}', 'DocumentationController@destroy')->name('documentation.destroy');

    Route::get('/achievement/{proposal}/create', 'AchievementController@create')->name('achievement.create');
    Route::post('/achievement/{proposal}', 'AchievementController@store')->name('achievement.store');
    Route::delete('/achievement/{proposal}/destroy/{certificate}', 'AchievementController@destroy')->name('achievement.destroy');

    Route::get('/achievement-request', 'AchievementController@createRequest')->name('achievement-request');
    Route::post('/achievement-upload', 'AchievementController@storeRequest')->name('achievement-upload');

});

Route::group(['middleware' => ['role:Dekan'], 'prefix' => 'dekan', 'as' => 'dekan.'], function () {
    Route::get('/home', 'DekanController@index')->name('home');

    Route::put('/proposals/review/{proposalId}', 'DekanController@review')->name('proposal.review.store');
    Route::get('/proposals/review/{proposalId}/user-history', 'DekanController@getUserHistory')->name('user.history.index');

    Route::post('/mails/{mail}/sign', 'MailController@sign')->name('mail.sign');

    Route::put('/dispositions', 'DispositionController@reDisposition')->name('reDisposition');
});

Route::group(['middleware' => ['role:Wadek II'], 'prefix' => 'wadek2', 'as' => 'wadek2.'], function () {
    Route::get('/home', 'Wadek2Controller@index')->name('home');
});

Route::group(['middleware' => ['role:Wadek III'], 'prefix' => 'wadek3', 'as' => 'wadek3.'], function () {
    Route::get('/home', 'Wadek3Controller@index')->name('home');
});

Route::group(['middleware' => ['role:Kabag'], 'prefix' => 'kabag', 'as' => 'kabag.'], function () {
    Route::get('/home', 'KabagController@index')->name('home');
});

Route::group(['middleware' => ['role:Kasubag|Keuangan'], 'prefix' => 'kasubag', 'as' => 'kasubag.'], function () {
    Route::get('/home', 'KasubagController@index')->name('home');

    Route::get('/lpj', 'LpjController@index')->name('lpj.index');
    Route::post('/lpj/{proposal}/confirm', 'LpjController@confirm')->name('lpj.confirm');
    Route::post('/lpj/{proposal}/revision', 'LpjController@revision')->name('lpj.revision');

    Route::put('/lpj/{proposal}/confirm-complete', 'LpjController@confirmComplete')->name('lpj.confirm-complete');
});

Route::group(['middleware' => ['role:Sekretaris Dekan'], 'prefix' => 'sekdek', 'as' => 'sekdek.'], function () {
    Route::get('/home', 'SekdekController@index')->name('home');

    Route::get('/mails/request', 'MailController@getMailRequest')->name('mail.request');
    Route::get('/mails/{mail}/create', 'MailController@create')->name('mail.create');

    Route::get('/budgets', 'BudgetController@create')->name('budget.create');
    Route::post('/budgets', 'BudgetController@store')->name('budget.store');
    Route::get('/budgets/{year}', 'BudgetController@edit')->name('budget.edit');
    Route::put('/budgets/{year}', 'BudgetController@update')->name('budget.update');
});

Route::group(['middleware' => ['role:Dekan|Wadek II|Kabag|Kasubag|Keuangan']], function () {
    Route::get('/dispositions', 'DispositionController@index')->name('disposition.index');
    Route::post('/dispositions', 'DispositionController@store')->name('disposition.store');
    Route::get('/dispositions/{proposal}', 'DispositionController@show')->name('disposition.show');
});

Route::group(['middleware' => ['role:Dekan|Sekretaris Dekan']], function () {
    Route::get('/mails', 'MailController@index')->name('mail.index');
    Route::post('/mails', 'MailController@store')->name('mail.store');
    Route::put('/mails/{mail}', 'MailController@update')->name('mail.update');
    Route::get('/mails/{mail}/revision', 'MailController@showRevision')->name('mail.show.revision');
    Route::put('/mails/{mail}/revision', 'MailController@revision')->name('mail.revision');
});

//Route::group(['middleware' => ['role:Dekan|Wadek III']], function () {
//    Route::get('/user-achievements', 'AchievementController@getAchievementsHistories')->name('user-achievements');
//});

Route::group(['middleware' => ['role:Dekan|Kabag|Kasubag|Keuangan|Sekretaris Dekan|Mahasiswa']], function () {
    Route::get('/documentation/{proposal}', 'DocumentationController@index')->name('documentation.index');
    Route::get('/achievement/{proposal}', 'AchievementController@index')->name('achievement.index');
});

Route::group(['middleware' => ['role:Dekan|Wadek II|Wadek III|Kabag|Kasubag|Sekretaris Dekan']], function () {
    Route::get('/disposition-histories', 'DispositionController@getHistory')->name('disposition.history.index');
    Route::get('/mail-histories', 'MailController@getHistory')->name('mail.history.index');

    Route::get('/chart-funding-statistic', 'ChartController@getFundingStatistic')->name('chart.funding-statistic');
    Route::get('/chart-funding-proposal-type', 'ChartController@getFundingProposalType')->name('chart.funding-proposal-type');
    Route::get('/chart-achievement-department', 'ChartController@getAchievementDepartment')->name('chart.achievement-department');

    Route::get('/user-achievements', 'AchievementController@getAchievementsHistories')->name('user-achievements');
});

Route::group(['middleware' => ['role:Dekan|Wadek II|Wadek III|Kabag|Kasubag|Sekretaris Dekan|Mahasiswa']], function () {
    Route::get('/mails/{mail}', 'MailController@show')->name('mail.show');

    Route::get('/files/{id}/download', 'FileController@download')->name('file.download');
    Route::get('/files/{type}/download/{name}', 'FileController@downloadByName')->name('file.download-by-name');

    Route::get('/proposal-histories', 'ProposalController@getHistory')->name('proposal.history.index');
});

