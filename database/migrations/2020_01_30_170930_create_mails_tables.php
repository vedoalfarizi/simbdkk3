<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_types', function (Blueprint $table) {
            $table->string('id', 10);
            $table->string('title', 20);
            $table->string('signed_code', 5);
            $table->string('type', 1);
            $table->timestamps();

            $table->primary('id');
        });

        Schema::create('mails', function (Blueprint $table) {
            $table->string('id', 36);
            $table->string('slug', 36)->unique();
            $table->string('type_id', 10);
            $table->string('proposal_id', 10);
            $table->text('content');
            $table->text('sign')->nullable();
            $table->boolean('is_manual_sign')->default(false);
            $table->string('signed_by', 24)->index();
            $table->string('revision')->nullable();
            $table->timestamps();

            $table->primary('id');
            $table->foreign('type_id')->references('id')->on('mail_types')->onDelete('cascade');
            $table->foreign('proposal_id')->references('id')->on('proposals')->onDelete('cascade');
            $table->foreign('signed_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mails');
        Schema::dropIfExists('mail_types');
    }
}
