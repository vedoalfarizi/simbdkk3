<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function (Blueprint $table) {
            $table->string('id', 10);
            $table->string('annual_budget_year', 4)->index();
            $table->unsignedTinyInteger('event_id')->index();
            $table->unsignedTinyInteger('organization_id')->nullable();
            $table->string('proposed_by', 24)->index();
            $table->string('title');
            $table->text('desc');
            $table->string('level', 15);
            $table->date('held_on');
            $table->date('end_on')->nullable();
            $table->string('location');
            $table->bigInteger('funds_needed');
            $table->bigInteger('funds_receive')->nullable();
            $table->bigInteger('funds_realisation')->nullable();
            $table->string('cover_letter_url');
            $table->string('proposal_url');
            $table->unsignedSmallInteger('status')->default(0);
            $table->string('committee_url')->nullable();
            $table->string('LPJ_url')->nullable();
            $table->string('receipt_url')->nullable();
            $table->string('revision')->nullable();
            $table->timestamps();

            $table->primary('id');
            $table->foreign('annual_budget_year')->references('year')->on('annual_budgets')->onDelete('cascade');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
            $table->foreign('proposed_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
