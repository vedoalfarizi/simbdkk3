<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = [
            [
                'name' => 'Kompetisi',
                'created_at' => '2020-01-31 14:20:36',
                'updated_at' => '2020-01-31 14:20:36',
            ],[
                'name' => 'Konferensi',
                'created_at' => '2020-01-31 14:20:36',
                'updated_at' => '2020-01-31 14:20:36',
            ],[
                'name' => 'Seminar',
                'created_at' => '2020-01-31 14:20:36',
                'updated_at' => '2020-01-31 14:20:36',
            ],[
                'name' => 'Pertukaran pelajar',
                'created_at' => '2020-01-31 14:20:36',
                'updated_at' => '2020-01-31 14:20:36',
            ],[
                'name' => 'Pengabdian masyarakat (Tanpa dosen)',
                'created_at' => '2020-01-31 14:20:36',
                'updated_at' => '2020-01-31 14:20:36',
            ]
        ];

        DB::table('events')->insert($events);
    }
}
