<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRoles = [
            [
                'role_id' => 1,
                'model_type' => 'App\User',
                'model_id' => '9999999999'
            ], [
                'role_id' => 2,
                'model_type' => 'App\User',
                'model_id' => '1511521016'
            ], [
                'role_id' => 2,
                'model_type' => 'App\User',
                'model_id' => '1511511022'
            ], [
                'role_id' => 2,
                'model_type' => 'App\User',
                'model_id' => '1511512022'
            ], [
                'role_id' => 2,
                'model_type' => 'App\User',
                'model_id' => '1511522004'
            ], [
                'role_id' => 2,
                'model_type' => 'App\User',
                'model_id' => '1511512028'
            ], [
                'role_id' => 2,
                'model_type' => 'App\User',
                'model_id' => '1511521004'
            ], [
                'role_id' => 3,
                'model_type' => 'App\User',
                'model_id' => '198910112013162001'
            ], [
                'role_id' => 4,
                'model_type' => 'App\User',
                'model_id' => '19640225198902100'
            ], [
                'role_id' => 5,
                'model_type' => 'App\User',
                'model_id' => '197202142000121001'
            ], [
                'role_id' => 6,
                'model_type' => 'App\User',
                'model_id' => '198327072008121003'
            ], [
                'role_id' => 7,
                'model_type' => 'App\User',
                'model_id' => '196307071991031000'
            ], [
                'role_id' => 9,
                'model_type' => 'App\User',
                'model_id' => '198410302008122002'
            ],
            [
                'role_id' => 10,
                'model_type' => 'App\User',
                'model_id' => '199482201602161001'
            ],
        ];

        DB::table('model_has_roles')->insert($userRoles);
    }
}
