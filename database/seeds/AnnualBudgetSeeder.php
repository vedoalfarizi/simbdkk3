<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnnualBudgetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $budget = [
            [
                'year' => Carbon::now()->year,
                'amount' => 75000000,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00',
            ],
        ];

        DB::table('departments')->insert($budget);
    }
}
