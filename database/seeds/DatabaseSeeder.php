<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(DepartmentsTableSeeder::class);
         $this->call(RolesTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(UserRolesTableSeeder::class);
         $this->call(EventsTableSeeder::class);
         $this->call(OrganizationsTableSeeder::class);
//         $this->call(AnnualBudgetSeeder::class);
//         $this->call(ProposalTableSeeder::class);
         $this->call(MailTypesTableSeeder::class);
    }
}
