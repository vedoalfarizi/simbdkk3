<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Helper\ShortId;

class ProposalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = ShortId::getId();
        $proposals = [
            [
                'id' => 'ODGYOEQER',
                'annual_budget_year' => Carbon::now()->year,
                'event_id' => 1,
                'proposed_by' => '1511521016',
                'title' => 'Indonesian Next 2021',
                'desc' => 'Indonesian Next 2021 merupakan salah satu program CSR Telkomsel yang memberikan fasilitas training hingga sertifikasi nasional dan internasional.',
                'level' => 'Nasional',
                'held_on' => '2021-06-14',
                'end_on' => null,
                'location' => 'Telkom Hub, Jakarta',
                'funds_needed' => 1000000,
                'cover_letter_url' => 'coverLetter/YcoCTp2IYAoy5V9uoDvczHFYGfN9waGlpMWX97Eg.pdf',
                'proposal_url' => 'proposal/5JseycSMm4ZKzZIugiStVFZWl6gYvMas8iGMJAFK.pdf',
                'status' => config('value.progress.reviewed-sekdek'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'id' => $id,
                'annual_budget_year' => Carbon::now()->year,
                'event_id' => 2,
                'proposed_by' => '1511512028',
                'title' => 'Youth Startup Forum Asean',
                'desc' => 'Delegasi Youth Startup Forum dipilih melalui seleksi online di seluruh wilayah Asia Tenggara dan
                            terpilih 100 delegasi terbaik dengan mengusung tema "The Least Entrepreneurial Generation"',
                'level' => 'Internasional',
                'held_on' => '2021-06-16',
                'end_on' => '2021-06-18',
                'location' => 'Malaysian Global Innovation & Creativity Center Block 3730, Persiaran Apec, 63000 Cyberjaya, Selangor, Malaysia',
                'funds_needed' => 15834550,
                'cover_letter_url' => 'coverLetter/YcoCTp2IYAoy5V9uoDvczHFYGfN9waGlpMWX97Eg.pdf',
                'proposal_url' => 'proposal/5JseycSMm4ZKzZIugiStVFZWl6gYvMas8iGMJAFK.pdf',
                'status' => config('value.progress.submitted'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];

        $teams = [
            [
                'user_id' => '1511521016',
                'proposal_id' => 'ODGYOEQER',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'user_id' => '1511512028',
                'proposal_id' => $id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'user_id' => '1511521016',
                'proposal_id' => $id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'user_id' => '1511521004',
                'proposal_id' => $id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ];

        $budgets = [
            [
                'proposal_id' => 'ODGYOEQER',
                'type' => 'accommodation',
                'amount' => 250000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'proposal_id' => 'ODGYOEQER',
                'type' => 'travel',
                'amount' => 750000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'proposal_id' => $id,
                'type' => 'registration',
                'amount' => 834550,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'proposal_id' => $id,
                'type' => 'travel',
                'amount' => 8000000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'proposal_id' => $id,
                'type' => 'accommodation',
                'amount' => 5000000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'proposal_id' => $id,
                'type' => 'daily',
                'amount' => 2000000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];

        DB::table('proposals')->insert($proposals);
        DB::table('teams')->insert($teams);
        DB::table('proposal_budgets')->insert($budgets);
    }
}
