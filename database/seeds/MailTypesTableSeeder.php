<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MailTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'id' => 'KM.05.03',
                'title' => 'SURAT KETERANGAN',
                'signed_code' => 'D',
                'type' => 'T',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00',
            ], [
                'id' => 'KM.05.02',
                'title' => 'SURAT TUGAS',
                'signed_code' => 'D',
                'type' => 'B',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00',
            ], [
                'id' => 'KM.05.01',
                'title' => 'SURAT IZIN',
                'signed_code' => 'D',
                'type' => 'B',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00',
            ]
        ];

        DB::table('mail_types')->insert($types);
    }
}
