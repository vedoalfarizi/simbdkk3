<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = [
          [
                'name' => 'Sistem Informasi',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00',
          ],[
                'name' => 'Teknik Komputer',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00',
            ]
        ];

        DB::table('departments')->insert($departments);
    }
}
