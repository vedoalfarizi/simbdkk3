<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'Super Admin',
                'guard_name' => 'web',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'name' => 'Mahasiswa',
                'guard_name' => 'web',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'name' => 'Sekretaris Dekan',
                'guard_name' => 'web',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'name' => 'Kasubag',
                'guard_name' => 'web',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'name' => 'Kabag',
                'guard_name' => 'web',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'name' => 'Wadek II',
                'guard_name' => 'web',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'name' => 'Dekan',
                'guard_name' => 'web',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'name' => 'Dosen',
                'guard_name' => 'web',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'name' => 'Wadek III',
                'guard_name' => 'web',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ],
            [
                'name' => 'Keuangan',
                'guard_name' => 'web',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ],
        ];

        DB::table('roles')->insert($roles);
    }
}
