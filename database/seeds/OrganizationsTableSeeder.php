<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrganizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $organization = [
            [
                'name' => 'BEM KM FTI',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00',
            ],[
                'name' => 'Al Fatih',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00',
            ],[
                'name' => 'Biner',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00',
            ],[
                'name' => 'HMSI',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00',
            ],[
                'name' => 'HMSK',
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00',
            ]
        ];

        DB::table('organizations')->insert($organization);
    }
}
