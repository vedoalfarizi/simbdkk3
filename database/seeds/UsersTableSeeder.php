    <?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = Hash::make('1sampai8');
        $users = [
            [
                'id' => '9999999999',
                'department_id' => NULL,
                'name' => 'Super Admin',
                'email' => 'super@admin.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'id' => '1511521016',
                'department_id' => 1,
                'name' => 'Vedo Alfarizi',
                'email' => 'vedoalfarizi@gmail.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'id' => '1511522004',
                'department_id' => 1,
                'name' => 'Ridho Darman',
                'email' => 'ridho@gmail.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'id' => '1511521004',
                'department_id' => 1,
                'name' => 'Dartika Anie Marian',
                'email' => 'dara@gmail.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'id' => '1511511022',
                'department_id' => 2,
                'name' => 'Tata Bayu Amarta',
                'email' => 'tata@gmail.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'id' => '1511512022',
                'department_id' => 2,
                'name' => 'Mahfuz Jailani Ibrahim',
                'email' => 'ibrahim@gmail.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'id' => '1511512028',
                'department_id' => 2,
                'name' => 'Marizka',
                'email' => 'marizka@gmail.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'id' => '198910112013162001',
                'department_id' => NULL,
                'name' => 'Sri Wahyuni Hasni, A.Md.',
                'email' => 'sekretaris@dekan.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'id' => '19640225198902100',
                'department_id' => NULL,
                'name' => 'Iswardi, S.Sos, M.M.',
                'email' => 'kasubag@udk.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'id' => '197202142000121001',
                'department_id' => NULL,
                'name' => 'Khairisman Fedra, S.Pt.',
                'email' => 'kabag@tu.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'id' => '198327072008121003',
                'department_id' => NULL,
                'name' => 'Hasdi Putra, M.T.',
                'email' => 'wadek@dua.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'id' => '196307071991031000',
                'department_id' => NULL,
                'name' => 'Dr. Ahmad Syafruddin Indrapriyatna, M.T.',
                'email' => 'dekan@fti.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ], [
                'id' => '198410302008122002',
                'department_id' => NULL,
                'name' => 'Ratna Aisuwarya, M.Eng.',
                'email' => 'wadek@tiga.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ],
            [
                'id' => '199482201602161001',
                'department_id' => NULL,
                'name' => 'Khalid Abdul Rauf, A.Md',
                'email' => 'keuangan@fti.com',
                'password' => $password,
                'created_at' => '2020-01-01 07:00:00',
                'updated_at' => '2020-01-01 07:00:00'
            ]
        ];

        DB::table('users')->insert($users);
    }
}
