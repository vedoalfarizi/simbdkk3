<?php

namespace App\Model;

use App\Traits\CompositeKey;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use CompositeKey;
    protected $primaryKey = ['user_id', 'proposal_id'];

    protected $fillable = ['user_id', 'proposal_id'];

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }

    public function proposal(){
        return $this->belongsTo(Proposal::class, 'proposal_id', 'id');
    }
}
