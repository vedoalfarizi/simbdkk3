<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Proposal extends Model
{
    public $incrementing = false;

    protected $dates = ['held_on', 'end_on'];

    public function annualBudget(){
        return $this->belongsTo(AnnualBudget::class, 'annual_budget_year', 'year');
    }

    public function event(){
        return $this->belongsTo(Event::class, 'event_id', 'id');
    }

    public function users(){
        return $this->belongsToMany(\App\User::class, 'teams', 'proposal_id', 'user_id');
    }

    public function proposer(){
        return $this->belongsTo(\App\User::class, 'proposed_by', 'id');
    }

    public function teams(){
        return $this->hasMany(Team::class, 'proposal_id', 'id');
    }

    public function dispositions(){
        return $this->hasMany(Disposition::class, 'proposal_id', 'id');
    }

    public function mails(){
        return $this->hasMany(Mail::class, 'proposal_id', 'id');
    }

    public function documentations(){
        return $this->hasMany(Documentation::class, 'proposal_id', 'id');
    }

    public function achievements(){
        return $this->hasMany(Achievement::class, 'proposal_id', 'id');
    }

    public function organization(){
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }

    public function budgets(){
        return $this->hasMany(ProposalBudget::class, 'proposal_id', 'id');
    }

    public function getFundsNeededRupiahAttribute(){
        return 'Rp'.number_format($this->funds_needed, 2, ',', '.');
    }

    public function getFundsReceiveRupiahAttribute(){
        return 'Rp'.number_format($this->funds_receive, 2, ',', '.');
    }

    public function getFundsRealisationRupiahAttribute(){
        return 'Rp'.number_format($this->funds_realisation, 2, ',', '.');
    }

    public function getTitleWithStatusAttribute(){
        if($this->organization_id){
            return '('.$this->organization->name.') Melaksanakan '.$this->event->name.' '.$this->title;
        }else{
            return 'Mengikuti '.$this->event->name.' '.$this->title;
        }
    }

    public function scopeCountByStatus($query){
        $query->select(
            DB::raw("count(if(status > 0, 1, null) and if(status < 11, 1, null)) as process"),
            DB::raw("count(if(status = 11, 1, null) or if(status = 13, 1, null)) as done"),
            DB::raw('count(*) as total')
        );
    }

    public function scopeCountByStatusForReview($query){
        $query->select(
            DB::raw("count(if(status = 0, 1, null)) as request"),
            DB::raw("count(if(status = 1, 1, null)) as revision"),
            DB::raw("count(if(status > 1, 1, null)) as done"),
            DB::raw('count(*) as total')
        );
    }

    public function scopeCountByEventAndFunding($query){
        $query->select(
            DB::raw("SUM(funds_receive) as funding"),
            DB::raw("count(if(event_id = 1, 1, null)) as competition"),
            DB::raw("count(if(event_id = 2, 1, null)) as conference"),
            DB::raw("count(if(event_id = 3, 1, null)) as seminar"),
            DB::raw("count(if(event_id = 4, 1, null)) as exchange"),
            DB::raw("count(if(event_id = 5, 1, null)) as dedication")
        );
    }

    public function scopeCountMailByType($query){
        $query->withCount([
            'mails as tugas' => function ($query){
                $query->where('type_id', 'KM.05.02');
            },
            'mails as izin' => function ($query){
                $query->where('type_id', 'KM.05.01');
            },
            'mails as keterangan' => function ($query){
                $query->where('type_id', 'KM.05.03');
            }
        ]);
    }

    public function scopeCountByStatusLPJ($query){
        $query->select(
            DB::raw("count(if(status = 8, 1, null)) as request"),
            DB::raw("count(if(status = 9, 1, null)) as revision"),
            DB::raw("count(if(status = 10, 1, null)) as waiting"),
            DB::raw("count(if(status = 11, 1, null)) as done"),
            DB::raw('count(*) as total')
        );
    }

    public function scopeCountByStatusDisposition($query){
        $query->select(
            DB::raw("count(if(status = 2, 1, null)) as request"),
            DB::raw("count(if(status = 3, 1, null)) as revised"),
            DB::raw("count(if(status >= 5, 1, null) and if(status <= 11, 1, null)) as disposed")
        );
    }

    public function scopeFundStatistic($query){
        $query->select(
            DB::raw('YEAR(created_at) as year'),
            DB::raw('SUM(funds_needed) as need'),
            DB::raw('SUM(funds_receive) as received'),
            DB::raw('SUM(funds_realisation) as used')
        )
        ->groupBy('year');
    }

    public function scopeFundsProposalType($query){
        $query->select(
            DB::raw('YEAR(created_at) as year'),
            DB::raw('count(if(organization_id IS null, 1, null)) as attend'),
            DB::raw('count(if(organization_id IS NOT null, 1, null)) as manage')
        )
        ->groupBy('year');
    }

    public function scopeEventReport($query){
        $query->select(
            DB::raw('YEAR(created_at) as year'),
            DB::raw("count(if(event_id = 1, 1, null)) as competition"),
            DB::raw("count(if(event_id = 2, 1, null)) as conference"),
            DB::raw("count(if(event_id = 3, 1, null)) as seminar"),
            DB::raw("count(if(event_id = 4, 1, null)) as exchange"),
            DB::raw("count(if(event_id = 5, 1, null)) as dedication")
        )
            ->groupBy('year');
    }

    public function scopeLastFiveYear($query){
        $query->whereYear('created_at', '<=', Carbon::now()->year)
            ->whereYear('created_at', '>=', Carbon::now()->subYear(4)->year);
    }

    public function scopeRealisation($query){
        $query->whereYear('created_at', '=', Carbon::now()->year)->select(
            DB::raw('SUM(funds_receive) as funding')
        );
    }
}
