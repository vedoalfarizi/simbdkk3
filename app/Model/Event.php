<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'name'
    ];

    public function proposals(){
        return $this->hasMany(Proposal::class, 'event_id', 'id');
    }
}
