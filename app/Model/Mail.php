<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Mail extends Model
{
    public $incrementing = false;

    public function type(){
        return $this->belongsTo(MailType::class, 'type_id', 'id');
    }

    public function signer(){
        return $this->belongsTo(\App\User::class, 'signed_by', 'id');
    }

    public function proposal(){
        return $this->belongsTo(Proposal::class, 'proposal_id', 'id');
    }

    public function scopeCountByStatus($query){
        $query->select(
            DB::raw("count(if(revision IS NOT null, 1, null)) as revision"),
            DB::raw("count(if(sign IS null, 1, null)) as waiting"),
            DB::raw("count(if(sign IS NOT null, 1, null)) as done"),
            DB::raw("count(if(is_manual_sign = 1, 1, null)) as manual"),
            DB::raw('count(*) as total')
        );
    }

    public function scopeCountByType($query){
        $query->select('type_id', DB::raw('count(*) as total'))->with('proposal')
        ->groupBy('type_id');
    }
}
