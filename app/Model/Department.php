<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'name'
    ];

    public function users(){
        return $this->hasMany(\App\User::class, 'department_id', 'id');
    }
}
