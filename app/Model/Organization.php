<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = [
        'name'
    ];

    public function proposals(){
        return $this->hasMany(Proposal::class, 'organization_id', 'id');
    }
}
