<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Documentation extends Model
{
    public function proposal(){
        return $this->belongsTo(Proposal::class, 'proposal_id', 'id');
    }
}
