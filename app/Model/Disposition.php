<?php

namespace App\Model;

use App\Traits\CompositeKey;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Disposition extends Model
{
    use CompositeKey;
    protected $primaryKey = ['proposal_id', 'from_id'];

    public function proposal(){
        return $this->belongsTo(Proposal::class, 'proposal_id', 'id');
    }

    public function dispositionBy(){
        return $this->belongsTo(\App\User::class, 'from_id', 'id');
    }

    public function dispositionTo(){
        return $this->belongsTo(\App\User::class, 'to_id', 'id');
    }

    public function scopeCountByStatus($query){
        $query->select(
            DB::raw("count(if(replied = 0, 1, null)) as request"),
            DB::raw("count(if(replied  = 2, 1, null)) as revised"),
            DB::raw("count(if(replied  = 1, 1, null)) as done"),
            DB::raw('count(*) as total')
        );
    }
}
