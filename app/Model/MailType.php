<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MailType extends Model
{
    public $incrementing = false;

    public function mails(){
        return $this->hasMany(Mail::class, 'type_id', 'id');
    }
}
