<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AnnualBudget extends Model
{
    protected $primaryKey = 'year';
}
