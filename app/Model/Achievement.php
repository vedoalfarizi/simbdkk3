<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    public function proposal(){
        return $this->belongsTo(Proposal::class, 'proposal_id', 'id');
    }

    public function user(){
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }

    public function scopeLastFiveYear($query){
        $query->whereYear('created_at', '<=', Carbon::now()->year)
            ->whereYear('created_at', '>=', Carbon::now()->subYear(4)->year);
    }

    public function scopeThisYear($query){
        $query->whereYear('created_at', '=', Carbon::now()->year);
    }
}
