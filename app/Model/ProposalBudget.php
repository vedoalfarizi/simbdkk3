<?php

namespace App\Model;

use App\Traits\CompositeKey;
use Illuminate\Database\Eloquent\Model;

class ProposalBudget extends Model
{
    use CompositeKey;

    protected $primaryKey = ['proposal_id', 'type'];

    public function proposal(){
        return $this->belongsTo(Proposal::class, 'proposal_id', 'id');
    }

    public function getAmountRupiahAttribute(){
        return 'Rp'.number_format($this->amount, 2, ',', '.');
    }
}
