<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'department_id', 'name', 'email', 'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function department(){
        return $this->belongsTo(Model\Department::class, 'department_id', 'id');
    }

    public function proposals(){
        return $this->belongsToMany(Model\Proposal::class, 'teams', 'user_id', 'proposal_id');
    }

    public function dispositionSenders(){
        return $this->hasMany(Model\Disposition::class, 'form_id', 'id');
    }

    public function dispositionReceivers(){
        return $this->hasMany(Model\Disposition::class, 'to_id', 'id');
    }

    public function achievements(){
        return $this->hasMany(Model\Achievement::class, 'user_id', 'id');
    }
}
