<?php

namespace App\Jobs;

use App\Notifications\Disposition;
use App\Notifications\MailStatus;
use App\Notifications\ProposalStatus;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;
    private $type;
    private $mail;
    private $proposal;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $type, $mail = null, $proposal = null)
    {
        $this->user = $user;
        $this->type = $type;
        $this->mail = $mail;
        $this->proposal = $proposal;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->type == 'mail'){
            $this->user->notify(new MailStatus($this->mail));
        }elseif ($this->type == 'proposal'){
            $this->user->notify(new ProposalStatus($this->proposal));
        }elseif ($this->type == 'disposition'){
            $this->user->notify(new Disposition($this->proposal));
        }
    }
}
