<?php

namespace App\Helper;

use Carbon\Carbon;
use Hashids\Hashids;

class ShortId{
    public static function getId($salt = null){
        $now = Carbon::now()->format('mdHis');
        if($salt === null){
            $hashid = new Hashids('', 6, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
        }else{
            $hashid = new Hashids($salt, 6, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
        }

        $id = $hashid->encode((int)$now);

        return $id;
    }
}