<?php

namespace App\Helper;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UploadFile
{
    public static function upload ($files){
        $paths = [];
        foreach ($files as $key => $value){
            $path = $files[$key]->store('public/'.Str::camel($key));
            $paths[$key] = substr($path, '7') ;
        }

        return $paths;
    }

    public static function deleteIfExists($path){
        if(Storage::exists($path)){
            Storage::delete($path);
        }

        return true;
    }
}