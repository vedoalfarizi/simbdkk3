<?php
/**
 * Created by PhpStorm.
 * User: vedoalfarizi
 * Date: 02/07/2020
 * Time: 13.43
 */

namespace App\Helper;


use Carbon\Carbon;

class Common
{
    public static function getDeadlineMessage($held_on){
        if(Carbon::parse($held_on) < Carbon::now()){
            return '<span class="badge badge-dark">'.$held_on->formatLocalized("%d %B %Y").'</span>';
        }else{
            $deadline = $held_on->diffInDays(new Carbon());
            if($deadline > 7){
                $badge = 'badge-success';
            }elseif($deadline > 0 && $deadline <= 7){
                $badge = 'badge-danger';
            }

            return '<span class="badge '.$badge.'">'.$deadline.' hari lagi</span>';
        }
    }

    public static function dateLocalize($held, $end = null){
        if($end){
            if($held->format('m') === $end->format('m') && $held->format('yyyy') === $end->format('yyyy')){
                $dateHeld = $held->formatLocalized("%d").'-'.$end->formatLocalized("%d %B %Y");
            }elseif ($held->format('m') !== $held->format('m') && $held->format('yyyy') === $end->format('yyyy')){
                $dateHeld = $held->formatLocalized("%d %B" ).' s.d. '.$end->formatLocalized("%d %B %Y");
            }else{
                $dateHeld = $held->formatLocalized("%d %B %Y ").' s.d. '.$end->formatLocalized("%d %B %Y");
            }
        }else{
            $dateHeld = $held->formatLocalized("%d %B %Y ");
        }

        return $dateHeld;
    }

    public static function generateYearLabel($start, $end){
        $years = [];
        for ($i = $start; $i <= $end; $i++){
            array_push($years, $i);
        }

        return $years;
    }

    public static function getPoint($level, $reward){
        return config('value.point.'.$level) * config('value.point.'.$reward);
    }

    public static function generateMailProposal($proposal){
        switch ($proposal->status){
            case 0:
                if($proposal->revision){
                    return [
                        'subject' => 'Revisi proposal',
                        'body' => 'Anda menerima revisi proposal bantuan dana dengan kode proposal '.$proposal->id.'.'
                    ];
                }

                return [
                    'subject' => 'Pengajuan proposal',
                    'body' => 'Anda menerima sebuah pengajuan proposal bantuan dana dengan kode proposal '.$proposal->id.'.',
                ];

            case 1:
            case 3:
                return [
                    'subject' => 'Revisi proposal',
                    'body' => 'Proposal Anda dengan kode '.$proposal->id.' telah direview. Mohon untuk diperbaiki sesuai dengan catatan revisi.'
                ];

            case 2:
                return [
                    'subject' => 'Pengajuan proposal',
                    'body' => 'Anda menerima sebuah pengajuan proposal bantuan dana dengan kode proposal '.$proposal->id.'.',
                ];

            case 5:
            case 6:
                return [
                    'subject' => 'Permintaan surat',
                    'body' => 'Anda menerima permintaan surat terkait proposal bantuan dana dengan kode proposal '.$proposal->id.'.',
                ];
            case 7:
                return [
                    'subject' => 'Pengajuan proposal selesai',
                    'body' => 'Proposal Anda dengan kode '.$proposal->id.' telah selesai diproses, berkas dapat diambil di dekanat. Harap mengirimkan LPJ sebelum batas waktu yang telah ditentukan.'
                ];
            case 8:
                return [
                    'subject' => 'Pengajuan LPJ',
                    'body' => 'Anda menerima sebuah pengajuan LPJ bantuan dana dengan kode proposal '.$proposal->id.'.',
                ];
            case 9:
                return [
                    'subject' => 'Revisi LPJ',
                    'body' => 'LPJ Anda dengan kode proposal '.$proposal->id.' telah direview. Mohon untuk diperbaiki sesuai dengan catatan revisi.'
                ];

            case 10:
                return [
                    'subject' => 'LPJ Diterima',
                    'body' => 'LPJ Anda dengan kode proposal '.$proposal->id.' telah diterima. Harap menyerahkan berkas asli ke dekanat.'
                ];

        }
         return [];
    }

    public static function generateMail($revision){
        if($revision){
            return [
                'subject' => 'Revisi surat',
                'body' => 'Anda memiliki sebuah permintaan revisi surat. Mohon diperbaiki sesuai dengan catatan revisi yang diberikan.',
            ];
        }

        return [
            'subject' => 'Permintaan tanda tangan surat',
            'body' => 'Anda memiliki sebuah permintaan tanda tangan surat.',
        ];
    }

    public static function getProposalStatus($status){
        switch ($status){
            case 0:
                return 'Proposal berhasil diupload. Menunggu review oleh Dekan.';
            case 3:
                return 'Harap memperbaiki proposal sesuai dengan catatan revisi yang diberikan.';
            case 5:
                return 'Proposal Dalam tahap disposisi.';
            case 6:
                return 'Proposal telah di disposisi. Menunggu proses berkas.';
            case 7:
                return 'Pengajuan berhasil. Berkas dapat diambil ke Dekanat.';
            case 8:
                return 'Menunggu review oleh Keuangan';
            case 9:
                return 'Harap memperbaiki LPJ sesuai dengan catatan revisi yang diberikan.';
            case 10:
                return 'Harap menyerahkan dokumen (SKPD & kwitansi) ke dekanat.';
            case 11:
                return 'Selesai';
            case 12:
                return "Dibatalkan";
            case 13:
                return 'Ditolak';
        }
    }

    public static function getNumberOfWorkingDays($start, $end){
        $rawHolidays = json_decode(
            file_get_contents('https://raw.githubusercontent.com/guangrei/Json-Indonesia-holidays/master/calendar.json'),
            true
        );

        $holidays = [];
        foreach ($rawHolidays as $key => $raw){
            if(isset($raw['status']) && $raw['status'] == 'CONFIRMED' && substr(strval($key), 0, 4) == $start->year){
                $holidays[] = strval($key);
            }
        }

        return $start->diffInDaysFiltered(function (Carbon $date) use ($holidays) {
            return $date->isWeekday() && !in_array($date->format('Ymd'), $holidays);
        }, $end);
    }
}
