<?php
namespace App\Helper;


class ApiResponse
{
    public static function success($data, $message = "Success", $code = 200){
        return response([
            "success" => true,
            "data" => $data,
            "code" => $code,
            "message" => $message
        ], $code);
    }

    public static function error($message = "Internal server error", $code = 500){
        return response([
            "success" => false,
            "data" => null,
            "code" => $code,
            "message" => $message
        ], $code);
    }
}