<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if(Auth::user()->hasRole('Super Admin')){
            return redirect(route('admin.home'));
        }elseif(Auth::user()->hasRole('Mahasiswa')){
            return redirect(route('mahasiswa.home'));
        }elseif(Auth::user()->hasRole('Dekan')){
            return redirect(route('dekan.home'));
        }elseif(Auth::user()->hasRole('Wadek II')){
            return redirect(route('wadek2.home'));
        }elseif(Auth::user()->hasRole('Kabag')){
            return redirect(route('kabag.home'));
        }elseif(Auth::user()->hasRole(['Kasubag', 'Keuangan'])){
            return redirect(route('kasubag.home'));
        }elseif (Auth::user()->hasRole('Sekretaris Dekan')){
            return redirect(route('sekdek.home'));
        }elseif (Auth::user()->hasRole('Wadek III')){
            return redirect(route('wadek3.home'));
        }

        $request->session()->invalidate();

        throw ValidationException::withMessages([
            'email' => 'Pengguna tidak memiliki akses',
        ]);

    }
}
