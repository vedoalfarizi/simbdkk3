<?php

namespace App\Http\Controllers;

use App\Helper\UploadFile;
use App\Http\Requests\LpjStoreRequest;
use App\Http\Requests\LpjUpdateRequest;
use App\Jobs\SendEmail;
use App\Model\Achievement;
use App\Model\Documentation;
use App\Model\Proposal;
use App\Model\Team;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LpjController extends Controller
{
    public function index(Request $request){
        $status = $request->query('status');

        $lpjs = null;
        if($status === 'revision'){
            $lpjs = Proposal::where('status', config('value.progress.lpj-revised'))->orderBy('created_at', 'ASC')->get();
        }else if($status === 'done'){
            $lpjs = Proposal::where('status', config('value.progress.completed'))->orderBy('created_at', 'ASC')->get();
        }else{
            $lpjs = Proposal::where(function ($query){
                return $query->where('status', config('value.progress.lpj-submitted'))
                    ->orWhere('status', config('value.progress.lpj-verified'));
            })->where('LPJ_url', '!=', null)
                ->orderBy('created_at', 'ASC')->get();
        }

        return view('lpj.index', compact('lpjs'));
    }

    public function create($id){
        $proposal = Proposal::find($id);
        if(!$proposal){
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Proposal tidak ditemukan');
        }

        return view('lpj.create', compact('proposal'));
    }

    public function store(LpjStoreRequest $request){
        $input = $request->all();

        $proposal = Proposal::find($input['proposal_id']);
        if(!$proposal){
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Proposal tidak ditemukan');
        }
        if(!$proposal->organization_id){
            if(!isset($input['achievement'])){
                return redirect()->back()->with('error', 'Penghargaan tidak boleh kosong.');
            }
        }else{
            $input['achievement'] = [];
        }

        DB::beginTransaction();
        try{
            $fileUrl = UploadFile::upload([
                'lpj' => $input['lpj'],
                'receipt' => $input['receipt'],
            ]);
            $proposal->funds_realisation = $input['realisation'];
            $proposal->LPJ_url = $fileUrl['lpj'];
            $proposal->receipt_url = $fileUrl['receipt'];
            $proposal->status = config('value.progress.lpj-submitted');
            $proposal->save();

            foreach ($input['photo'] as $key => $photo){
                $url = UploadFile::upload([
                    'photo' => $photo,
                ]);

                $documentation = new Documentation();
                $documentation->proposal_id = $input['proposal_id'];
                $documentation->photo_url = $url['photo'];
                $documentation->desc = $input['desc'][$key];
                $documentation->save();
            }

            $teams = Team::where('proposal_id', $input['proposal_id'])->get()->count();
            // Save All Achievement
            foreach ($input['achievement'] as $key => $file){
                $url = UploadFile::upload([
                    'achievement' => $file,
                ]);

                $achievement = new Achievement();
                $achievement->proposal_id = $input['proposal_id'];
                if($teams > 1){
                    $achievement->user_id = $input['nim'][$key];
                }else{
                    $achievement->user_id = Auth::user()->id;
                }
                $achievement->certificate_url = $url['achievement'];
                $achievement->reward = $input['reward'][$key];
                $achievement->save();
            }
        }catch (\Exception $exception){
            DB::rollBack();
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Gagal mengirim LPJ, Coba lagi');
        }

        DB::commit();

        $kasubag = User::role('Keuangan')->first();
        SendEmail::dispatch($kasubag, 'proposal', null, $proposal);

        return redirect(route('mahasiswa.proposal.index'))->with('success', 'Berhasil mengirim LPJ');
    }

    public function edit($proposalId){
        $proposal = Proposal::find($proposalId);

        if(!$proposal){
            return redirect()->back()->with('error', 'LPJ tidak ditemukan.');
        }

        return view('lpj.edit', compact('proposal'));
    }

    public function update($proposalId, LpjUpdateRequest $request){
        $proposal = Proposal::find($proposalId);

        if(!$proposal){
            return redirect()->back()->with('error', 'LPJ tidak ditemukan.');
        }

        $input = $request->all();

        if(isset($input['lpj'])){
            try{
                $fileUrl = UploadFile::upload([
                    'lpj' => $input['lpj'],
                ]);
                $proposal->LPJ_url = $fileUrl['lpj'];
            }catch (\Exception $exception){
                return redirect(route('mahasiswa.proposal.index'))->with('error', 'Terjadi kesalahan saat upload dokumen');
            }
        }

        if(isset($input['receipt'])){
            try{
                $fileUrl = UploadFile::upload([
                    'receipt' => $input['receipt'],
                ]);
                $proposal->LPJ_url = $fileUrl['receipt'];
            }catch (\Exception $exception){
                return redirect(route('mahasiswa.proposal.index'))->with('error', 'Terjadi kesalahan saat upload dokumen');
            }
        }

        $proposal->revision = null;
        $proposal->funds_realisation = $input['realisation'];
        $proposal->status = config('value.progress.lpj-submitted');
        $proposal->save();

        $kasubag = User::role('Kasubag')->first();
        SendEmail::dispatch($kasubag, 'proposal', null, $proposal);

        return redirect(route('mahasiswa.proposal.index'))->with('success', 'Berhasil mengirim revisi LPJ');
    }

    public function confirm($proposalId){
        $proposal = Proposal::whereId($proposalId)->with('users')->first();

        if(!$proposal){
            return redirect()->back()->with('error', 'LPJ tidak ditemukan.');
        }

        $proposal->status = config('value.progress.lpj-verified');
        $proposal->save();

        $user = User::whereId($proposal->proposed_by)->first();
        SendEmail::dispatch($user, 'proposal', null, $proposal);

        return redirect(route('kasubag.lpj.index'))->with('success', 'Konfirmasi LPJ berhasil');
    }

    public function confirmComplete($proposalId){
        $proposal = Proposal::find($proposalId);

        if(!$proposal){
            return redirect()->back()->with('error', 'LPJ tidak ditemukan.');
        }

        $proposal->status = config('value.progress.completed');
        $proposal->save();

        return redirect(route('kasubag.lpj.index'))->with('success', 'Konfirmasi LPJ berhasil');
    }

    public function revision($proposalId, Request $request){
        $proposal = Proposal::whereId($proposalId)->with('users')->first();

        if(!$proposal){
            return redirect()->back()->with('error', 'LPJ tidak ditemukan.');
        }

        $input = $request->all();

        $proposal->status = config('value.progress.lpj-revised');
        $proposal->revision = $input['message'];
        $proposal->save();

        $user = User::find($proposal->proposed_by);
        SendEmail::dispatch($user, 'proposal', null, $proposal);

        return redirect(route('kasubag.lpj.index'))->with('success', 'Berhasil mengirimkan permintaan revisi LPJ');
    }
}
