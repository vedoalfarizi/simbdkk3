<?php

namespace App\Http\Controllers;

use App\Helper\Common;
use App\Helper\ShortId;
use App\Helper\UploadFile;
use App\Http\Requests\ProposalStoreRequest;
use App\Http\Requests\ProposalUpdateRequest;
use App\Jobs\SendEmail;
use App\Model\Achievement;
use App\Model\AnnualBudget;
use App\Model\Disposition;
use App\Model\Documentation;
use App\Model\Event;
use App\Model\Mail;
use App\Model\MailType;
use App\Model\Organization;
use App\Model\Proposal;
use App\Model\ProposalBudget;
use App\Model\Team;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ProposalController extends Controller
{
    public function index()
    {
        $proposals = Proposal::where('status', '<', 11)
            ->whereHas('teams', function (Builder $team){
                $team->whereUserId(Auth::id());
            })
            ->with('teams.user', 'event', 'organization', 'budgets')->orderBy('created_at', 'ASC')->get();

        $proposals->map(function ($proposal){
           $proposal->status_detail = $this->getDetailStatus($proposal);
           $proposal->date = Common::dateLocalize($proposal->held_on, $proposal->end_on);
        });

        return view('proposal.index', compact('proposals'));
    }

    public function getOption(){
        return view('proposal.option');
    }

    public function create(Request $request)
    {
        $type = $request->input('option');
        $events = Event::pluck('name', 'id');
        $organizations = null;
        if($type === 'manage'){
            $organizations = Organization::pluck('name', 'id');
        }

        return view('proposal.create', compact('events', 'type', 'organizations'));
    }

    private function noDuplication(array $inputArray)
    {
        return count($inputArray) === count(array_flip($inputArray));
    }

    private function isValidBudgetDetail(array $budgets){
        foreach ($budgets as $bugdet){
            if (!is_numeric($bugdet)){
                return false;
            }
        }
        return true;
    }

    private function storeProposalBudget($proposalId, array $input){
        $budgets = collect($input)->filter(function($value, $key){
            if($key == 'travel' || $key == 'accommodation' || $key == 'daily' || $key == 'registration' ||
                $key == 'equipment'){
                return $key;
            }
        });

        try{
            foreach ($budgets as $type => $amount){
                $budget = new ProposalBudget();
                $budget->proposal_id = $proposalId;
                $budget->type = $type;
                $budget->amount = $amount;
                $budget->save();
            }
        }catch (\Exception $exception){
            throw $exception;
        }

        return null;
    }

    private function updateProposalBudget($proposalId, array $input){
        $budgets = collect($input)->filter(function($value, $key){
            if($key == 'travel' || $key == 'accommodation' || $key == 'daily' || $key == 'registration' ||
                $key == 'equipment'){
                return $key;
            }
        });

        try{
            foreach ($budgets as $type => $amount){
                $budget = ProposalBudget::where('proposal_id', $proposalId)->where('type', $type)->first();
                $budget->amount = $amount;
                $budget->save();
            }
        }catch (\Exception $exception){
            throw $exception;
        }

        return null;
    }

    private function isValidFund($input)
    {
        if ($input['type'] == 'manage') {
            if(!$input['fund'] || !is_numeric($input['fund'])){
                return false;
            }
        } elseif ($input['type'] == 'attend'){
            return $this->isValidBudgetDetail([$input['travel'], $input['accommodation'], $input['daily'],
                $input['registration'], $input['equipment']]);
        }else{
            return false;
        }

        return true;
    }

    public function store(ProposalStoreRequest $request)
    {
        $input = $request->all();
        $authId = (string)Auth::user()->id;

        if(Common::getNumberOfWorkingDays(Carbon::now(),
                Carbon::createFromFormat('Y-m-d', $input['start'])) < 5){
            return redirect()->back()->withInput()
                ->with('error', 'Batas maksimal pengajuan proposal adalah 5 hari kerja sebelum tanggal mulai kegiatan');
        }

        if(!$this->isValidFund($input)){
            return redirect()->back()->withInput()->with('error', 'Anggaran tidak valid');
        }

        if($input['type'] == 'manage'){
            if(!$input['organization_id'] || !$input['committee']){
                return redirect()->back()->withInput()->with('error', 'Mohon lengkapi data pelaksana');
            }
            $input['nim'][0] = $authId;
        }elseif($input['type'] == 'attend'){
            if(isset($input['nim'])){
                array_unshift($input['nim'], $authId);

                if (!$this->noDuplication($input['nim'])) {
                    return redirect()->back()->withInput()
                        ->with('error', 'Mohon periksa kembali data tim, terdapat NIM yang sama');
                }
            }else{
                $input['nim'][0] = $authId;
            }

            $input['fund'] = $input['travel'] + $input['accommodation'] + $input['daily'] + $input['registration'] +
                $input['equipment'];
        }

        $annualBudget = AnnualBudget::where('year', Carbon::now()->year)->first();
        if(!$annualBudget){
            return redirect(route('mahasiswa.proposal.index'))
                ->with('error', 'Pengajuan belum dapat dilakukan karena anggaran belum ada');
        }

        DB::beginTransaction();
        try {
            $proposal = new Proposal();

            if($input['type'] == 'manage'){
                $fileUrl = UploadFile::upload([
                    'committee' => $input['committee'],
                ]);
                $proposal->committee_url = $fileUrl['committee'];
                $proposal->organization_id = $input['organization_id'];
            }

            $fileUrl = UploadFile::upload([
                'cover_letter' => $input['cover_letter'],
                'proposal' => $input['proposal']
            ]);

            $proposal->id = ShortId::getId();
            $proposal->annual_budget_year = $annualBudget->year;
            $proposal->title = $input['title'];
            $proposal->desc = $input['desc'];
            $proposal->event_id = $input['event_id'];
            $proposal->level = $input['level'];
            $proposal->held_on = $input['start'];
            $proposal->end_on = $input['end'];
            $proposal->location = $input['location'];
            $proposal->funds_needed = $input['fund'];
            $proposal->cover_letter_url = $fileUrl['cover_letter'];
            $proposal->proposal_url = $fileUrl['proposal'];
            $proposal->proposed_by = $authId;
            $proposal->save();

            foreach ($input['nim'] as $nim) {
                $team = new Team();
                $team->user_id = $nim;
                $team->proposal_id = $proposal->id;
                $team->save();
            }

            if($input['type'] == 'attend'){
                $this->storeProposalBudget($proposal->id, $input);
            }
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Sistem error, silahkan coba kembali');
        }

        DB::commit();

        $dekan = User::role('Dekan')->first();
        SendEmail::dispatch($dekan, 'proposal', null, $proposal);

        return redirect(route('mahasiswa.proposal.index'))->with('success', 'Berhasil mengajukan proposal');
    }

    public function edit($id, Request $request){
        $proposal = Proposal::where('id', $id)->with('budgets')->first();
        if(!$proposal){
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Proposal tidak ditemukan');
        }
        $budgets = [];
        foreach ($proposal->budgets as $budget){
            $budgets[$budget->type] = $budget->amount;
        }

        $type = $request->input('option');
        $events = Event::pluck('name', 'id');
        $organizations = Organization::pluck('name', 'id');

        return view('proposal.edit', compact('proposal', 'events', 'type', 'organizations', 'budgets'));
    }

    public function update(ProposalUpdateRequest $request, $id){
        $proposal = Proposal::find($id);
        if(!$proposal){
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Proposal tidak ditemukan');
        }

        $input = $request->all();
        if(Common::getNumberOfWorkingDays(Carbon::now(),
                Carbon::createFromFormat('Y-m-d', $input['start'])) < 5){
            return redirect()->back()->withInput()
                ->with('error', 'Batas maksimal pengajuan proposal adalah 5 hari kerja sebelum tanggal mulai kegiatan');
        }

        if(!$this->isValidFund($input)){
            return redirect()->back()->withInput()->with('error', 'Anggaran tidak valid');
        }
        if($input['type'] == 'manage'){
            if(!$input['organization_id']){
                return redirect()->back()->withInput()->with('error', 'Mohon pilih organisasi');
            }
            $proposal->organization_id = $input['organization_id'];
        }elseif($input['type'] == 'attend'){
            $input['fund'] = $input['travel'] + $input['accommodation'] + $input['daily'] + $input['registration'] +
                $input['equipment'];
        }

        DB::beginTransaction();
        try{
            $proposal->title = $input['title'];
            $proposal->desc = $input['desc'];
            $proposal->event_id = $input['event_id'];
            $proposal->level = $input['level'];
            $proposal->held_on = $input['start'];
            $proposal->end_on = $input['end'];
            $proposal->location = $input['location'];
            $proposal->funds_needed = $input['fund'];

            if(isset($input['cover_letter']) && $input['cover_letter']){
                $path = 'public/'.$proposal->cover_letter_url;
                UploadFile::deleteIfExists($path);

                $fileUrl = UploadFile::upload([
                    'cover_letter' => $input['cover_letter'],
                ]);
                $proposal->cover_letter_url = $fileUrl['cover_letter'];
            }

            if(isset($input['proposal']) && $input['proposal']){
                $path = 'public/'.$proposal->proposal_url;
                UploadFile::deleteIfExists($path);

                $fileUrl = UploadFile::upload([
                    'proposal' => $input['proposal'],
                ]);
                $proposal->proposal_url = $fileUrl['proposal'];
            }

            if(isset($input['committee']) && $input['committee']){
                $path = 'public/'.$proposal->committee_url;
                UploadFile::deleteIfExists($path);

                $fileUrl = UploadFile::upload([
                    'committee' => $input['committee'],
                ]);
                $proposal->proposal_url = $fileUrl['committee'];
            }

            $proposal->save();

            $this->updateProposalBudget($id, $input);
        }catch (\Exception $exception){
            DB::rollback();
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Sistem error, silahkan coba kembali');
        }

        DB::commit();

        return redirect(route('mahasiswa.proposal.index'))->with('success', 'Berhasil mengubah data proposal');
    }

    public function editTeam($proposal_id){
        $proposal = Proposal::find($proposal_id);
        if(!$proposal){
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Proposal tidak ditemukan');
        }

        $team = Team::where('proposal_id', $proposal_id)->whereHas('user', function ($query){
            $query->where('id', '!=', Auth::user()->id);
        })->get();

        return view('team.edit', compact('team', 'proposal_id'));
    }

    public function updateTeam(Request $request, $proposal_id){
        $input = $request->all();
        $authId = Auth::user()->id;

        if(!isset($input['nim'])){
            $input['nim'][0] = $authId;
        }elseif(!in_array($authId, $input['nim'])){
            array_push($input['nim'], (string) $authId);
        }

        $input['nim'] = array_filter($input['nim'], function ($nim){
            return $nim != null;
        });

        if(count($input['nim']) > 1){ // more than one student
            if (!$this->noDuplication($input['nim'])) {
                return redirect()->back()->withInput()->with('error', 'Mohon periksa kembali data tim, terdapat data NIM yang sama');
            }
        }

        // Get existing member that not selected (be deleted)
        $deleteMember = DB::table('teams')->where('proposal_id', $proposal_id)
            ->whereNotIn('user_id', $input['nim'])
            ->get();

        DB::beginTransaction();

        $proposal = Proposal::find($proposal_id);
        if(!in_array($proposal->proposed_by, $input['nim'])){
            $proposal->proposed_by = $authId;

            $proposal->save();
        }

        try{
            foreach ($deleteMember as $member){
                Team::where('user_id', $member->user_id)->where('proposal_id', $proposal_id)->delete();
            }

            // create if not exist
            foreach ($input['nim'] as $member){
                Team::where('user_id', $member)->where('proposal_id', $proposal_id)->firstOrCreate([
                    'user_id' => $member,
                    'proposal_id' => $proposal_id,
                ]);
            }
        }catch (\Exception $exception){
            DB::rollBack();
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Gagal mengubah data tim, silahkan coba kembali');
        }

        DB::commit();

        return redirect(route('mahasiswa.proposal.index'))->with('success', 'Berhasil mengubah data tim');
    }

    public function destroy($id){
        $proposal = Proposal::find($id);
        if(!$proposal){
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Proposal tidak ditemukan');
        }

        if($proposal->status > config('value.progress.revised-dekan')){
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Mohon maaf tidak dapat menghapus proposal yang telah diproses');
        }

        $proposal->delete();

        return redirect(route('mahasiswa.proposal.index'))->with('success', 'Berhasil membatalkan proposal');
    }

    public function getHistory(Request $request){
        if($request->ajax()) {
            if(Auth::user()->hasRole('Mahasiswa')){
                $proposals = User::find(Auth::user()->id)
                    ->proposals()->where('status', '>', 10)->orderBy('created_at', 'DESC')
                    ->get();
            }else{
                $proposals = Proposal::where('status', '>', 10)->orderBy('created_at', 'DESC')->get();
            }

            if(count($proposals) > 0){
                $proposals->map(function ($proposal){
                    $team = Team::where('proposal_id', $proposal->id)->with(array('user' => function($query){
                        $query->orderBy('name', 'ASC')->select('id', 'name');
                    }))->get(['user_id']);
                    $proposal->team = $team;

                    $mails = Mail::where('proposal_id', $proposal->id)
                        ->with('type')->get();
                    $proposal->mails = $mails;

                    $documentations = Documentation::where('proposal_id', $proposal->id)->get();
                    $proposal->documentations = $documentations;

                    $achievements = Achievement::where('proposal_id', $proposal->id)->get();
                    $proposal->achievements = $achievements;

                    $proposal->result = $this->getFundingStatus($proposal);
                });
            }

            $start = intval($request->get('startRp'));
            $end = intval($request->get('endRp'));
            if($start != 0 || $end != 0){
                if($end == 0 || $start > $end) $end = $start;

                $proposals = $proposals->filter(function($proposal) use ($start, $end){
                    return $proposal->funds_receive >= $start && $proposal->funds_receive <= $end;
                });
            }

            return DataTables::of($proposals)
                ->addIndexColumn()
                ->editColumn('id', function ($row){
                    return view('proposal.datatables.declined-info', compact('row'))->render();
                })
                ->editColumn('event', function ($row){
                    return view('proposal.datatables.event', compact('row'))->render();
                })
                ->addColumn('fundingRupiah', function ($row){
                    return $row->fundsReceiveRupiah;
                })
                ->addColumn('funding', function ($row){
                    return $row->funds_receive;
                })
                ->addColumn('files', function ($row){
                    return view('proposal.datatables.files', compact('row'))->render();
                })
                ->rawColumns(['id', 'event', 'funding', 'files', 'action'])
                ->make(true);
        }

        $years = [];
        $startYear = Carbon::now()->subYear(4)->year;
        $endYear = Carbon::now()->year;
        for ($i = $startYear; $i <= $endYear; $i++){
            array_push($years, $i);
        }

        $levels = config('value.level');

        return view('proposal.history',  compact('years', 'levels'));
    }

    public function confirmRevision($proposalId){
        $proposal = Proposal::find($proposalId);
        if(!$proposal){
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Proposal tidak ditemukan');
        }

        $proposal->status = config('value.progress.submitted');
        $dekan = User::role('Dekan')->first();

        $proposal->save();
        SendEmail::dispatch($dekan, 'proposal', null, $proposal);

        return redirect(route('mahasiswa.proposal.index'))->with('success', 'Berhasil mengirimkan revisi proposal');
    }

    private function getDetailStatus($proposal){
        $deadline = $proposal->end_on ?
            $proposal->end_on->modify('2 week')->formatLocalized("%d %B %Y") :
            $proposal->held_on->modify('2 week')->formatLocalized("%d %B %Y");

        $detail = [
            'label' => '',
            'message' => '',
            'img' => ''
        ];

        $status = $proposal->status;
        switch ($status){
            case 0:
                $detail['label'] = 'Menunggu review';
                $detail['message'] = 'Proposal berhasil diupload. Menunggu review oleh Dekan';
                $detail['img'] = '/img/first.png';
                break;
            case 3:
                $detail['label'] = 'Revisi proposal';
                $detail['message'] = '<p>Harap memperbaiki proposal sesuai dengan catatan revisi yang diberikan.</p>
                                            <div style="text-align: left">
                                                <b>Revisi:</b><p>'.$proposal->revision.'</p>
                                            </div>';
                $detail['img'] = '/img/first.png';
                break;
            case 5:
                $dispositionDetail = $this->getDispositionDetail($proposal->id);

                $detail['label'] = 'Disposisi';
                $detail['message'] = '<p>Dalam tahap disposisi</p>
                                        <ol style="text-align: left">'.$dispositionDetail.'</ol>';
                $detail['img'] = '/img/second.png';

                break;
            case 6:
                $processDetail = $this->getProcessDetail($proposal->id);

                $detail['label'] = 'Diproses';
                $detail['message'] = '<p>Proposal telah di disposisi. Menunggu proses berkas.</p>
                                        <ol style="text-align: left">'.$processDetail.'</ol>';
                $detail['img'] = '/img/second.png';

                break;
            case 7:
                $detail['label'] = 'Menunggu LPJ';
                $detail['message'] = 'Pengajuan berhasil. Berkas dapat diambil ke Dekanat. <br> Harap mengirimkan LPJ sebelum '.$deadline;
                $detail['img'] = '/img/third.png';

                break;
            case 8:
                $detail['label'] = 'Menunggu konfirmasi LPJ';
                $detail['message'] = 'LPJ berhasil dikirim. Menunggu review oleh Keuangan';
                $detail['img'] = '/img/third.png';

                break;
            case 9:
                $detail['label'] = 'Revisi LPJ';
                $detail['message'] = '<p>Harap memperbaiki LPJ sesuai dengan catatan revisi yang diberikan.</p>';
                $detail['img'] = '/img/third.png';

                break;
            case 10:
                $detail['label'] = 'Lengkapi berkas';
                if($proposal->organization_id){
                    $detail['message'] = '<p>Harap menyerahkan dokumen berupa kuitansi asli ke dekanat.</p>';
                }else{
                    $detail['message'] = '<p>Harap menyerahkan dokumen (SKPD & kwitansi) ke dekanat.</p>';
                }
                $detail['img'] = '/img/fourth.png';

                break;
        }

        return $detail;
    }

    private function getFundingStatus($proposal){
        if($proposal->status === config('value.progress.completed')){
            return '<span class="badge badge-success">Didanai</span>';
        }elseif ($proposal->status === config('value.progress.canceled')){
            return '<span class="badge badge-info">Dibatalkan</span>';
        }elseif($proposal->status === config('value.progress.declined-dekan')){
            return '<button type="button" class="btn badge badge-danger" data-toggle="modal" data-target="#declined-'.$proposal->id.'">
                        Ditolak
                    </button>';

        }

        return "";
    }

    private function getProcessDetail($id){
        $base = MailType::all();
        $mail = [];

        $mails = Mail::where('proposal_id', $id)->with('type')->get();

        foreach ($base as $item) {
            $mail[$item->id] = "<li><span class='badge badge-warning'>Menunggu </span>".strtolower($item->title).' diproses</li>';
        }

        foreach ($mails as $item){
            if(array_key_exists($item->type_id, $mail)){
                if($item->sign == null){
                    $mail[$item->type_id] = "<li><span class='badge badge-primary'>Menunggu tandatangan </span>".strtolower($item->type->title).'</li>';
                }else{
                    $mail[$item->type_id] = '<li>'.strtolower($item->type->title) ."<span class='badge badge-success'> ditandatangani</span></li>";
                }
            }
        }

        return implode('', $mail);
    }

    private function getDispositionDetail($id){
        $result = [];

        $dispositions = Disposition::where('proposal_id', $id)->with('dispositionTo.roles')->orderBy('created_at', 'asc')->get();

        foreach ($dispositions as $disposition){
            if($disposition->replied === config('value.dispositionStatus.disposed')){
                $result[] = "<li><span class='badge badge-success'>Telah di disposisi oleh </span> (".$disposition->dispositionTo->roles[0]->name.") ".$disposition->dispositionTo->name.'</li>';
            }else{
                $result[] = "<li><span class='badge badge-primary'>Menunggu disposisi oleh</span> (".$disposition->dispositionTo->roles[0]->name.") ".$disposition->dispositionTo->name.'</li>';
            }
        }

        return implode('', $result);
    }

    public function getStudentsJson(Request $request){
        if($request->has('q')){
            $query = $request->q;
            $students = User::role('Mahasiswa')->where('name','LIKE',"%$query%")
                ->orWhere('id', 'LIKE', "%$query%")->get(['id', 'name']);
            return response()->json($students);
        }

        return null;
    }
}
