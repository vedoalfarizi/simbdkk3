<?php

namespace App\Http\Controllers;

use App\Model\Disposition;
use App\Model\Proposal;
use App\User;
use Illuminate\Support\Facades\Auth;

class KasubagController extends Controller
{
    public function index(){
        if(Auth::user()->hasRole('Kasubag')){
            $dispositions = Disposition::where('to_id', Auth::user()->id)->countByStatus()->first();
        }else{
            $kasubag = User::role('Kasubag')->first();
            $dispositions = Disposition::where('to_id', $kasubag->id)->countByStatus()->first();
        }

        $lpj = Proposal::where('status', '>=', config('value.progress.lpj-submitted'))
            ->where('status', '<=', config('value.progress.completed'))
            ->countByStatusLPJ()->first();

        return view('kasubag.home', compact( 'lpj', 'dispositions'));
    }
}
