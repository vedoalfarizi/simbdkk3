<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use App\Model\Achievement;
use App\Model\AnnualBudget;
use App\Model\Mail;
use App\Model\Proposal;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SekdekController extends Controller
{
    public function index(){
        $proposal = Proposal::countByStatusForReview()->first();

        $mail = Mail::countByStatus()->first();
        $mail->waiting -= ($mail->manual + $mail->revision);
        $lastMailNo = Mail::latest()->first(['id']);
        $mailRequest = Proposal::where('status', config('value.progress.disposed'))->orWhere('status', config('value.progress.processed'))
            ->countMailByType()->get();
        $request = $mailRequest->map(function ($mail){
           $count = 0;

           if ($mail->tugas === 0){
              $count++;
           }
           if ($mail->izin === 0){
               $count++;
           }
           if ($mail->keterangan === 0){
               $count++;
           }

           return $count;
        });

        $countRequest = 0;
        foreach ($request as $count){
            $countRequest += $count;
        }
        $mail->request = $countRequest;

        $archive["proposal"] = Proposal::where('status', '>', 10)->orderBy('created_at', 'DESC')->count();
        $archive["mail"] = Mail::count();
        $archive["achievement"] = Achievement::count();

        $nowYear = Carbon::now()->year;
        $nowBudget = 0;
        $budget = AnnualBudget::find($nowYear);
        if($budget){
            $nowBudget = $budget->amount;
        }

        return view('sekdek.home', compact('proposal', 'mail', 'lastMailNo', 'archive', 'nowBudget', 'nowYear'));
    }

    public function getReviewRequest(Request $request){
        if($request->ajax()){
            $proposals = Proposal::whereStatus(config('value.progress.submitted'))->orderBy('held_on', 'asc')->get();

            return DataTables::of($proposals)
                ->addIndexColumn()
                ->editColumn('title', function ($row){
                    return $row->titleWithStatus;
                })
                ->addColumn('action', function ($row){
                    return '<a href="'.route('sekdek.proposal.review.detail', $row->id).'" class="btn btn-primary"><small>Review</small></a>';
                })
                ->editColumn('created_at', function ($row){
                    return $row->created_at->diffForHumans();
                })->rawColumns(['action'])
                ->make(true);
        }
        return view('proposal.review.index');
    }

    public function getOnReview(Request $request){
        if($request->ajax()){
            $proposals = Proposal::whereStatus(config('value.progress.revised-sekdek'))->orderBy('created_at', 'asc')->get();

            return DataTables::of($proposals)
                ->addIndexColumn()
                ->editColumn('title', function ($row){
                    return $row->titleWithStatus;
                })
                ->editColumn('created_at', function ($row){
                    return $row->created_at->formatLocalized("%d %B %Y %H:%M");
                })->rawColumns(['revision'])
                ->make(true);
        }
        return view('proposal.review.on-review');
    }

    public function getProposalDetail($proposalId){
        try{
            $proposal = Proposal::with(['event', 'organization', 'budgets'])->findOrFail($proposalId);

            return view('proposal.review.detail', compact('proposal'));
        }catch(ModelNotFoundException $e){
            return redirect(route('sekdek.proposal.review'))->with('error', 'Proposal tidak ditemukan');
        }
    }

    public function review(Request $request, $proposalId){
        $proposal = Proposal::whereId($proposalId)->with('users')->first();
        if(!$proposal){
            return redirect(route('sekdek.proposal.review'))->with('error', 'Proposal tidak ditemukan');
        }

        $input = $request->all();
        if($input['reviewBtn'] === 'verify'){
            $proposal->status = config('value.progress.reviewed-sekdek');

            $user = User::role('Dekan')->first();
            $message = 'Berhasil verifikasi proposal';
        }elseif ($input['reviewBtn'] === 'revision'){
            if(!$input['message']){
                return redirect()->back()->with('error', 'Mohon isi catatan revisi');
            }
            $proposal->status = config('value.progress.revised-sekdek');
            $proposal->revision = $input['message'];

            $user = User::find($proposal->proposed_by);
            $message = 'Berhasil mengirimkan revisi proposal';
        }else{
            return redirect()->back()->with('error', 'Aksi tidak dikenal');
        }

        $proposal->save();
        SendEmail::dispatch($user, 'proposal', null, $proposal);


        return redirect(route('sekdek.proposal.review'))->with('success', $message);
    }
}
