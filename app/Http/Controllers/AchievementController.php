<?php

namespace App\Http\Controllers;

use App\Helper\UploadFile;
use App\Model\Achievement;
use App\Model\Team;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class AchievementController extends Controller
{
    private $startYear, $endYear;

    public function __construct()
    {
        $this->startYear = Carbon::now()->subYear(4)->year;
        $this->endYear = Carbon::now()->year;
    }

    public function index($proposalId){
        $achievements = Achievement::where('proposal_id', $proposalId)->get();

        if(Auth::user()->hasRole('Kasubag|Keuangan')){
            return view('achievement.kasubag.index', compact('proposalId', 'achievements'));
        }

        return view('achievement.index', compact('proposalId', 'achievements'));
    }

    public function create($proposalId){
        return view('achievement.create', compact('proposalId'));
    }

    public function store(Request $request, $proposalId){
        $input = $request->all();

        DB::beginTransaction();
        try{
            foreach ($input['achievement'] as $key => $certificate){
                // Upload each certificate
                $url = UploadFile::upload([
                    'certificate' => $certificate,
                ]);

                $achievement = new Achievement();
                $achievement->proposal_id = $proposalId;
                $achievement->user_id = $input['nim'][$key];
                $achievement->certificate_url = $url['certificate'];
                $achievement->reward = $input['reward'][$key];
                $achievement->save();
            }
        }catch (\Exception $exception){
            DB::rollBack();
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Gagal menambah penghargaan, Coba lagi');
        }

        DB::commit();
        return redirect(route('achievement.index', $proposalId))->with('success', 'Berhasil menambahkan penghargaan');
    }

    public function destroy($proposalId, $certificateId){
        $certificate = Achievement::where('id', $certificateId)->where('proposal_id', $proposalId)->first();

        if(!$certificate){
            return redirect()->back()->with('error', 'Terjadi kesalahan ketika menghapus penghargaan.');
        }

        $certificate->delete();

        return redirect(route('achievement.index', $proposalId))->with('success', 'Berhasil menghapus penghargaan');
    }

    public function getAchievementsHistories(Request $request){
        if($request->ajax()){

            $achievements = DB::table('achievements')
                ->join('proposals', 'achievements.proposal_id', '=', 'proposals.id')
                ->join('users', 'achievements.user_id', '=', 'users.id')
                ->select(
                    'level', 'user_id', 'name', 'title', 'reward', 'certificate_url',
                    DB::raw('YEAR(achievements.created_at) as year')
                )
                ->where('achievements.user_id', '!=', null)
                ->whereYear('achievements.created_at', '<=', $this->endYear)
                ->whereYear('achievements.created_at', '>=', $this->startYear)
                ->orderBy('year', 'desc')
                ->get();

            $anotherAchievements = DB::table('achievements')
                ->join('proposals', 'achievements.proposal_id', 'proposals.id')
                ->select(
                    'proposals.id', 'level', 'title', 'reward', 'certificate_url',
                    DB::raw('YEAR(achievements.created_at) as year')
                )
                ->where('achievements.user_id', '=', null)
                ->whereYear('achievements.created_at', '<=', $this->endYear)
                ->whereYear('achievements.created_at', '>=', $this->startYear)
                ->orderBy('year', 'desc')
                ->groupBy('proposals.id')
                ->get();

            $anotherAchievements = $anotherAchievements->map(function ($achievement){
               $achievement->users = Team::where('proposal_id', $achievement->id)->with(['user' => function($q){
                   $q->select('id', 'name');
               }])->get();
               return $achievement;
            });

            $achievements = $achievements->merge($anotherAchievements);

            return DataTables::of($achievements)
                ->addIndexColumn()
                ->addColumn('certificate', function ($row){
                    if(substr($row->certificate_url, -4) == '.pdf'){
                        $path = explode('/', $row->certificate_url);
                        $url = route('file.download-by-name', [$path[0], $path[1]]);
                        return '<a class="btn btn-sm btn-primary" href="'.$url.'">
                                    <i class="fa fa-download"></i>
                                </a>';
                    }else{
                        $url = url('storage/'.$row->certificate_url);
                        return '<a download="'.$row->title.'" href="'.$url.'">
                                <i class="fa fa-download"></i>
                            </a>';
                    }
                })
                ->addColumn('users', function ($row){
                    $users = null;
                    if(isset($row->users)){
                        $users = '<ol>';
                        foreach ($row->users as $user){
                            $users .= '<li>('.$user->user_id.') '.$user->user->name.'</li>';
                        }
                        $users .= '</ol>';
                    }else{
                        $users .= "($row->user_id) $row->name</li>";
                    }

                    return $users;
                })
                ->rawColumns(['certificate', 'users'])
                ->make(true);
        }

        $years = [];
        for ($i = $this->startYear; $i <= $this->endYear; $i++){
            array_push($years, $i);
        }

        $levels = config('value.level');

        return view('achievement.histories.index', compact('years', 'levels'));
    }

    public function createRequest(){
        return view('achievement.request.create');
    }

    public function storeRequest(Request $request){
        dd($request);
    }
}
