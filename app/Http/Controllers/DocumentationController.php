<?php

namespace App\Http\Controllers;

use App\Helper\UploadFile;
use App\Model\Documentation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DocumentationController extends Controller
{
    public function index($proposalId){
        $documentations = Documentation::where('proposal_id', $proposalId)->get();

        if(Auth::user()->hasRole('Kasubag|Keuangan')){
            return view('documentation.kasubag.index', compact('proposalId', 'documentations'));
        }

        return view('documentation.index', compact('proposalId', 'documentations'));
    }

    public function create($proposalId){
        return view('documentation.create', compact('proposalId'));
    }

    public function store(Request $request, $proposalId){
        $input = $request->all();

        DB::beginTransaction();
        try{
            foreach ($input['photo'] as $key => $photo){
                // Upload each photo
                $url = UploadFile::upload([
                    'photo' => $photo,
                ]);

                $documentation = new Documentation();
                $documentation->proposal_id = $proposalId;
                $documentation->photo_url = $url['photo'];
                $documentation->desc = $input['desc'][$key];
                $documentation->save();
            }
        }catch (\Exception $exception){
            DB::rollBack();
            return redirect(route('mahasiswa.proposal.index'))->with('error', 'Gagal menambah dokumentasi, Coba lagi');
        }

        DB::commit();
        return redirect(route('documentation.index', $proposalId))->with('success', 'Berhasil menambah dokumentasi');
    }

    public function destroy($proposalId, $photoId){
        $photo = Documentation::where('id', $photoId)->where('proposal_id', $proposalId)->first();

        if(!$photo){
            return redirect()->back()->with('error', 'Terjadi kesalahan ketika menghapus dokumentasi.');
        }

        $photo->delete();

        return redirect(route('documentation.index', $proposalId))->with('success', 'Berhasil menghapus dokumentasi');
    }
}
