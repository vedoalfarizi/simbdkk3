<?php

namespace App\Http\Controllers;

use App\Model\AnnualBudget;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BudgetController extends Controller
{
    public function create(){
        $year = Carbon::now()->year;
        $currentBudget = AnnualBudget::find($year);
        if($currentBudget){
            return redirect(route('sekdek.home'))->with('error', 'Pagu tahun ini sudah diatur');
        }

        return view('budget.create', compact('year'));
    }

    public function store(Request $request){
        $input = $request->all();
        $year = Carbon::now()->year;

        $budget = new AnnualBudget();
        $budget->year = $year;
        $budget->amount = $input['amount'];
        $budget->save();

        return redirect(route('sekdek.home'))->with('success', 'Berhasil menyimpan pagu');
    }

    public function edit($year){
        $budget = AnnualBudget::find($year);
        if(!$budget){
            return redirect(route('sekdek.home'))->with('error', 'Pagu tahun ini belum diatur');
        }

        $nowYear = Carbon::now()->year;
        if($budget->year != $nowYear){
            return redirect(route('sekdek.home'))->with('error', 'Permintaan tidak valid');
        }

        return view('budget.edit', compact('budget'));
    }

    public function update(Request $request, $year){
        $budget = AnnualBudget::find($year);
        if(!$budget){
            return redirect(route('sekdek.home'))->with('error', 'Pagu tahun ini belum diatur');
        }

        $nowYear = Carbon::now()->year;
        if($budget->year != $nowYear){
            return redirect(route('sekdek.home'))->with('error', 'Tidak dapat mengubah pagu yang telah lalu');
        }

        $input = $request->all();
        $budget->amount = $input['amount'];
        $budget->save();

        return redirect(route('sekdek.home'))->with('success', 'Berhasil memperbarui pagu');
    }
}
