<?php

namespace App\Http\Controllers;

use App\Helper\ApiResponse;
use App\Helper\Common;
use App\Model\Mail;
use App\Model\Proposal;
use App\Model\Team;
use App\User;
use Illuminate\Http\Request;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\ValidationData;

class ApiController extends Controller
{

    public function trackProposal(Request $request){
        $input = $request->all();

        if(!isset($input['proposalId'])){
            return ApiResponse::error("ID proposal tidak boleh kosong", 400);
        }

        $proposal = Proposal::find($input['proposalId']);
        if(!$proposal){
            return ApiResponse::error("Proposal tidak ditemukan", 404);
        }

        $result = [
          "id" => $proposal->id,
          "title" => $proposal->title,
          "status" => Common::getProposalStatus($proposal->status),
          "updatedAt" => $proposal->updated_at->format('d-m-Y H:i:s'),
        ];

        return ApiResponse::success($result);
    }

    private function getDelegation($proposal_id, $type){
        if($type === 'KM.05.03'){
            return Proposal::where('id', $proposal_id)->with('proposer.department')->first(['proposed_by']);
        }else{
            return Team::where('proposal_id', $proposal_id)->with(array('user' => function($query){
                $query->with('department')->select('id', 'name', 'department_id');
            }))->get();
        }
    }

    private function verify($sign, $mailId, $signerId){
        $signer = new Sha256();
        $publicKey = new Key('file://'.storage_path('jwt-sign/public.pem'));

        $parsing = (new Parser())->parse((string) $sign);

        if($parsing->verify($signer, $publicKey)){
            $data = new ValidationData();
            $data->setIssuer(env('APP_URL'));
            $data->setAudience($mailId);
            $data->setSubject($signerId);

            $validation = $parsing->validate($data);
            if($validation) {
                $result = [
                    'data' => $parsing->getClaim('data'),
                    'signedAt' => date("d/m/Y", $parsing->getClaim('iat'))
                ];
                return $result;
            }else{
                return false;
            }

        }else{
            return false;
        }
    }

    private function generateMailInfo($mailType, $funding){
        if($mailType == "KM.05.01"){
            return "izin diberikan kepada ";
        }

        if ($mailType == "KM.05.02"){
            return "menugaskan mahasiswa berikut ";
        }

        if ($mailType == "KM.05.03"){
            return 'bantuan sebesar Rp'.number_format($funding, 2, ',', '.').' diberikan kepada ';
        }

        return "Jenis surat tidak valid";
    }

    public function verifiedMail(Request $request){
        $input = $request->all();

        if(!isset($input['mailId'])){
            return ApiResponse::error("ID surat tidak boleh kosong", 400);
        }

        $mail = Mail::where('slug', $input['mailId'])->whereNotNull('sign')->with('type')->first();
        if(!$mail){
            return ApiResponse::error("Data surat tidak ditemukan", 404);
        }

        $signer = User::find($mail->signed_by);
        $delegation = $this->getDelegation($mail->proposal_id, $mail->type_id);

        $verified = $this->verify($mail->sign, $mail->slug, $signer->id);
        if(!$verified || $mail->proposal_id !== $verified['data']->proposal || $mail->proposal->funds_receive !== $verified['data']->funding){
            return ApiResponse::error("Tanda tangan digital tidak valid", 401);
        }

        $validMessage = $mail->type->title.' dengan nomor <br><b>'.$mail->id.'</b> <br> <b>TERBUKTI VALID</b>. <br> Ditandatangani secara digital oleh '.$signer->name.' pada '.$verified['signedAt'];
        $info = $this->generateMailInfo($mail->type_id, $verified['data']->funding);

        if(is_countable($delegation)){
            $delegation = $delegation->map(function ($delegate){
                return $delegate->user_id.' - '.$delegate->user->name;
            });
        }else{
            $delegation = [$delegation->proposer->id.' - '.$delegation->proposer->name];
        }

        $result = [
            "validMessage" => $validMessage,
            "info" => $info,
            "delegation" => $delegation
        ];

        return ApiResponse::success($result);
    }
}
