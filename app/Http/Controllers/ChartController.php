<?php

namespace App\Http\Controllers;

use App\Model\AnnualBudget;
use App\Model\Proposal;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ChartController extends Controller
{
    public function getFundingStatistic(){
        $funds = Proposal::fundStatistic()->lastFiveYear()->get();
        $year = Carbon::now()->year;
        $budget = AnnualBudget::find($year);
        if($budget){
            $budget = $budget->amount;
        }else{
            $budget = 0;
        }

        $result = [
            "funds" => $funds,
            "budget" => $budget,
        ];

        return response()->json($result);
    }

    public function getFundingProposalType(){
        $funds = Proposal::fundsProposalType()->lastFiveYear()->get();

        return response()->json($funds);
    }

    public function getAchievementDepartment(){
        $startYear = Carbon::now()->subYear(4)->year;
        $endYear = Carbon::now()->year;

        $achievementDepartment = DB::table('achievements')
            ->join('users', 'achievements.user_id', 'users.id')
            ->join('departments', 'users.department_id', 'departments.id')
            ->select(
                'departments.name',
                DB::raw('YEAR(achievements.created_at) as year'),
                DB::raw('count(department_id) as total')
            )
            ->whereYear('achievements.created_at', '<=', $endYear)
            ->whereYear('achievements.created_at', '>=', $startYear)
            ->groupBy('department_id', 'year')
            ->get();

        $anotherAchievementDepartment = DB::table('achievements')
            ->leftJoin('teams', 'achievements.proposal_id', 'teams.proposal_id')
            ->join('users', 'teams.user_id', 'users.id')
            ->join('departments', 'users.department_id', 'departments.id')
            ->select(
                'departments.name',
                DB::raw('YEAR(achievements.created_at) as year'),
//                DB::raw('count(department_id) as total')
                DB::raw('count(department_id) as total')
            )
            ->where('achievements.user_id', '=', null)
            ->whereYear('achievements.created_at', '<=', $endYear)
            ->whereYear('achievements.created_at', '>=', $startYear)
            ->groupBy('department_id', 'year')
            ->distinct('teams.proposal_id')
            ->get();

        $achievementDepartment = $achievementDepartment->merge($anotherAchievementDepartment)->sortBy('year');

        $achievementDepartment = $achievementDepartment->reduce(function ($carry, $item){
            $key = $item->year;
            $subKey = str_replace(' ','', $item->name) ;

            if(empty($carry[$key])){
                $carry[$key]["year"] = $key;
                $carry[$key][$subKey] = $item->total;
            }else{
                if(empty($carry[$key][$subKey])){
                    $carry[$key][$subKey] = $item->total;
                }else{
                    $carry[$key][$subKey] += $item->total;
                }
            }

            return $carry;
        }, []);

        $achievementDepartment = array_map(function($items){
            if(count($items) != 3){
                if (!array_key_exists('SistemInformasi', $items)) {
                    $items['SistemInformasi'] = 0;
                }else{
                    $items['TeknikKomputer'] = 0;
                }
            }

            return $items;
        }, $achievementDepartment);

        $result = array_values($achievementDepartment);

        return response()->json($result);
    }
}
