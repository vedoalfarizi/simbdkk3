<?php

namespace App\Http\Controllers;

use App\Model\Disposition;
use App\Model\Mail;
use Illuminate\Support\Facades\Auth;

class KabagController extends Controller
{
    public function index(){
        $mails = Mail::countByType()->get();
        $lastMailNo = Mail::latest()->first(['id']);
        $dispositions = Disposition::where('to_id', Auth::user()->id)->countByStatus()->first();

        $total = 0;
        foreach ($mails as $mail){
            $total += $mail->total;
        }

        return view('kabag.home', compact('mails', 'total', 'lastMailNo', 'dispositions'));
    }
}
