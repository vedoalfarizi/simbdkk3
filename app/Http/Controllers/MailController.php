<?php

namespace App\Http\Controllers;

use App\Helper\Common;
use App\Jobs\SendEmail;
use App\Model\Mail;
use App\Model\MailType;
use App\Model\Proposal;
use App\Model\Team;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\ValidationData;
use Yajra\DataTables\DataTables;
use Lcobucci\JWT\Builder as JWTBuilder;

class MailController extends Controller
{
    private $mail_type = null;

    public function index(Request $request){
        if($request->ajax()){
            if (Auth::user()->hasRole('Dekan')){
                $mails = Mail::where('revision', null)->where('sign', null)->where('is_manual_sign', false)->whereHas('type', function (Builder $query){
                    $query->where('signed_code', 'D');
                })->with(['type', 'proposal'])->get();

                return DataTables::of($mails)
                    ->addColumn('jenis', function ($row){
                        return strtolower($row->type->title);
                    })
                    ->addColumn('deadline', function ($row){
                        $message = Common::getDeadlineMessage($row->proposal->held_on);

                        return $message;
                    })
                    ->addColumn('action', function ($row){
                        return view('mail.datatables.dekan-action', compact('row'))->render();
                    })
                    ->rawColumns(['jenis', 'deadline', 'action'])
                    ->make(true);

            }elseif ((Auth::user()->hasRole('Sekretaris Dekan'))){
                $status = $request->query('status');
                if(!$status || $status!= 'revision'){
                    $mails = Mail::where('sign', '!=', null);
                }else{
                    $mails = Mail::where('revision', '!=', null);
                }

                $mails = $mails->orderBy('proposal_id', 'asc')->orderBy('created_at', 'asc')
                    ->with(['type', 'proposal'])->get();

                return DataTables::of($mails)
                    ->addColumn('type', function ($row){
                        return strtolower($row->type->title);
                    })
                    ->addColumn('status', function ($row){
                        $status = null;
                        if($row->revision){
                            $status = '<span class="badge badge-danger">Revisi</span>';
                        }else{
                            if($row->sign){
                                $status = '<span class="badge badge-success">Ditandatangani</span>';
                            }else{
                                $status = '<span class="badge badge-info">Menunggu tandatangan</span>';
                            }
                        }

                        return $status;
                    })
                    ->addColumn('deadline', function ($row){
                        $message = Common::getDeadlineMessage($row->proposal->held_on);

                        return $message;
                    })
                    ->addColumn('action', function ($row){
                        return view('mail.datatables.kasubag-action', compact('row'))->render();
                    })
                    ->rawColumns(['type', 'status', 'deadline', 'action'])
                    ->make(true);
            }
        }

        return view('mail.index');
    }

    private function getSigner($type){
        $this->mail_type = MailType::find($type);
        if($this->mail_type->signed_code === 'D'){
            return User::orderBy('created_at', 'DESC')->role('Dekan')->first(['id', 'name']);
        }elseif ($this->mail_type->signed_code === 'WDII'){
            return User::orderBy('created_at', 'DESC')->role('Wadek II')->first(['id', 'name']);
        }

        return null;
    }

    private function getDelegation($proposal, $type){
        if($type === 'KM.05.03'){
            return Proposal::where('id', $proposal)->with('proposer.department')->first(['proposed_by']);
        }else{
            return Team::where('proposal_id', $proposal)->with(array('user' => function($query){
                $query->with('department')->select('id', 'name', 'department_id');
            }))->get();
        }
    }

    private function countMail($proposal_id){
        return Mail::where('proposal_id', $proposal_id)->where('sign', '!=', null)->count();
    }

    public function store(Request $request){
        $input = $request->all();

        $mail_no = $input['type_type'].'-'.$input['mail_no'].'/UN16.15/'.$input['type_signed_code'].'/'.$input['type_id'].'/'.$input['now'];
        $content = $input['content'];
        $date = null;

        $signer = $this->getSigner($input['type_id']);
        $delegation = $this->getDelegation($input['proposal_id'], $input['type_id']);
        $mail_type = $this->mail_type;

        if($input['submit'] === 'preview'){
            $date = new Carbon();
            if($input['type_id'] === 'KM.05.03'){
                return view('mail.surat-keterangan', compact('mail_type', 'mail_no', 'signer', 'delegation', 'content', 'date'));
            }else{
                return view('mail.surat-izintugas', compact('mail_type', 'mail_no', 'signer', 'delegation', 'content', 'date'));
            }
        }

        if($input['submit'] === 'submit' || $input['submit'] === 'submitManual'){
            DB::beginTransaction();

            try{
                $mail = new Mail();
                $mail->id = $mail_no;
                $mail->slug = Str::slug($mail_no);
                $mail->type_id = $input['type_id'];
                $mail->proposal_id = $input['proposal_id'];
                $mail->content = $input['content'];
                $mail->is_manual_sign = $input['submit'] === 'submitManual' ? true : false;
                $mail->signed_by = $signer->id;
                $mail->save();

                $updateProposal = Proposal::find($input['proposal_id']);
                if($updateProposal->status != config('value.progress.processed')){
                    $updateProposal->status = config('value.progress.processed');
                    $updateProposal->save();
                }

                $manualMail = Mail::where('proposal_id', $input['proposal_id'])->where('is_manual_sign', true)->get();
                if($manualMail->count() >= 3){
                    $updateProposal = Proposal::whereId($input['proposal_id'])->with('users')->first();
                    $updateProposal->status = config('value.progress.mail-done');
                    $updateProposal->save();

                    $user = User::find($updateProposal->proposed_by);
                    SendEmail::dispatch($user, 'proposal', null, $updateProposal);
                }

                DB::commit();
                $message = 'Berhasil mengirim permintaan tanda tangan surat';
                if($input['submit'] === 'submitManual'){
                    $message = 'BErhasil menyimpan surat';
                }else{
                    $dekan = User::role('Dekan')->first();
                    SendEmail::dispatch($dekan, 'mail', $mail, null);
                }

                return redirect(route('sekdek.mail.request'))->with('success', $message);
            }catch (\Exception $exception){
                DB::rollBack();
                return back()->with('error', 'Sistem error, harap coba kembali');
            }
        }

        return back()->with('error', 'Tindakan tidak valid');
    }

    public function show(Request $request, $id){
        $print = $request->query('print');
        if(!$print || $print != 'true'){
            $print = false;
        }

        $mail = Mail::where('slug', $id)->with('type')->first();
        $signer = User::withTrashed()->find($mail->signed_by);
        $delegation = $this->getDelegation($mail->proposal_id, $mail->type_id);
        $verified = null;
        if($mail->sign){
            $verified = $this->verify($mail->sign, $mail->slug, $signer->id);
            if(!$verified || $mail->proposal_id !== $verified['data']->proposal || $mail->proposal->funds_receive !== $verified['data']->funding){
                $verified = false;
            }
        }

        $param = compact('mail', 'signer', 'delegation', 'verified');

        if($print){
            if($mail->type_id === 'KM.05.03'){
                return view('mail.print.surat-keterangan', $param);
            }else{
                return view('mail.print.surat-izintugas', $param);
            }
        }else{
            if($mail->type_id === 'KM.05.03'){
                return view('mail.surat-keterangan', $param);
            }else{
                return view('mail.surat-izintugas', $param);
            }
        }
    }

    public function update(Request $request, $id){
        $mail = Mail::where('slug', $id)->with('type')->first();

        if(!$mail){
            return redirect(route('mail.index', ['status' => 'revision']))->with('error', 'Surat tidak ditemukan');
        }
        $input = $request->all();

        $mail_no = $mail->id;
        $content = $input['content'];

        $signer = $this->getSigner($mail->type_id);
        $delegation = $this->getDelegation($mail->proposal_id, $mail->type_id);
        $mail_type = $this->mail_type;

        if($input['submit'] === 'preview'){
            $date = new Carbon();
            if($mail->type_id === 'KM.05.03'){
                return view('mail.surat-keterangan', compact('mail_type', 'mail_no', 'signer', 'delegation', 'content', 'date'));
            }else{
                return view('mail.surat-izintugas', compact('mail_type', 'mail_no', 'signer', 'delegation', 'content', 'date'));
            }
        }

        $dekan = User::role('Dekan')->first();

        $mail->content = $input['content'];
        $mail->revision = null;
        $mail->signed_by = $dekan->id;
        $mail->save();

        SendEmail::dispatch($dekan, 'mail', $mail, null);
        if($mail->is_manual_sign == true){
            return redirect(route('mail.history.index'))->with('success', 'Perubahan surat berhasil disimpan');
        }

        return redirect(route('mail.index', ['status' => 'revision']))->with('success', 'Perubahan surat berhasil disimpan');
    }

    public function revision(Request $request, $id){
        $mail = Mail::where('slug', $id)->first();

        if(!$mail){
            return redirect(route('mail.index'))->with('error', 'Surat tidak ditemukan');
        }

        if(!$request->revision){
            return redirect(route('mail.index'))->with('error', 'Catatan revisi tidak boleh kosong');
        }

        $mail->revision = $request->revision;
        $mail->save();

        $user = User::role('Sekretaris Dekan')->first();
        SendEmail::dispatch($user, 'mail', $mail, null);

        return redirect(route('mail.index'))->with('success', 'Berhasil mengirim revisi surat');
    }

    public function showRevision($id){
        $mail = Mail::where('slug', $id)->with('type')->first();

        return view('mail.show-revision', compact('mail'));
    }

    public function getMailRequest(Request $request){
        if($request->ajax()){
            $mails = Proposal::where('status', config('value.progress.disposed'))
                ->orWhere('status', config('value.progress.processed'))
                ->with('event')
                ->withCount([
                    'mails as tugas' => function ($query){
                        $query->where('type_id', 'KM.05.02');
                    },
                    'mails as izin' => function ($query){
                        $query->where('type_id', 'KM.05.01');
                    },
                    'mails as keterangan' => function ($query){
                        $query->where('type_id', 'KM.05.03');
                    }
                ])->get();

            $mails = $mails->filter(function ($item){
                return $item->tugas === 0 || $item->izin === 0 || $item->keterangan === 0;
            });

            return DataTables::of($mails)
                ->addColumn('event', function ($row){
                    return $row->event->name.' - '.$row->title;
                })
                ->addColumn('deadline', function ($row){
                    $message = Common::getDeadlineMessage($row->held_on);

                    return $message;
                })
                ->addColumn('action', function ($row){
                    return view('mail.datatables.request-action', compact('row'))->render();
                })
                ->rawColumns(['event', 'deadline', 'action'])
                ->make(true);
        }

        return view('mail.mail-request');
    }

    public function create(Request $request, $proposal_id){
        $type = $request->input('type');
        if(!$type){
            return redirect(route('sekdek.mail.request'))->with('error', 'Jenis surat tidak valid');
        }

        $proposal = Proposal::find($proposal_id);
        $lastMail = Mail::orderBy('created_at', 'DESC')->first(['id']);
        if($lastMail){
            $number = substr(explode('/', $lastMail->id)[0], '2');
            $lastMailNumber = $number + 1;
        }else{
            $lastMailNumber = 1;
        }

        $now = new Carbon();
        $dateHeld = Common::dateLocalize($proposal->held_on, $proposal->end_on);

        if($type === 'izin'){
            $type = MailType::find('KM.05.01');
            $content = 'Untuk mengikuti acara '.$proposal->title.' yang akan dilaksanakan tanggal '.$dateHeld.' di '.$proposal->location;
        }elseif($type === 'tugas'){
            $type = MailType::find('KM.05.02');
            $content = 'Untuk mengikuti acara '.$proposal->title.' yang akan dilaksanakan tanggal '.$dateHeld.' di '.$proposal->location;
        }elseif($type === 'keterangan'){
            $type = MailType::find('KM.05.03');
            $content = 'mendapatkan bantuan dari Fakultas Teknologi Informasi Universitas Andalas berupa uang sejumlah '.
                $proposal->funds_receive_rupiah.' untuk mengikuti acara "<b>'.$proposal->title.'</b>" yang diadakan di '.
                $proposal->location.'.';
        }else{
            return redirect(route('sekdek.mail.request'))->with('error', 'Jenis surat tidak valid');
        }

        return view('mail.create', compact('proposal', 'content', 'type', 'now', 'lastMailNumber'));
    }

    public function sign($slug){
        $signedId = User::role('Dekan')->first(['id']);
        $mail = Mail::where('slug', $slug)->where('signed_by', $signedId->id)
            ->with(['type', 'proposal'])->first();

        if(!$mail){
            return redirect(route('mail.index'))->with('error', 'Surat tidak ditemukan');
        }

        $signer = new Sha256();
        $privateKey = new Key('file://'.storage_path('jwt-sign/private.pem'));
        $time = time();

        $token = (new JWTBuilder())->issuedBy(env('APP_URL'))
            ->permittedFor($mail->slug)
            ->relatedTo($signedId->id)
            ->issuedAt($time)
            ->withClaim('data', [
                'proposal' => $mail->proposal_id,
                'funding' => $mail->proposal->funds_receive,
            ])
            ->getToken($signer, $privateKey);

        $mail->sign = $token;
        $mail->save();

        $nMail = $this->countMail($mail->proposal_id);
        if($nMail >= 3){
            $updateProposal = Proposal::whereId($mail->proposal_id)->with('users')->first();
            $updateProposal->status = config('value.progress.mail-done');
            $updateProposal->save();

            $user = User::find($updateProposal->proposed_by);
            SendEmail::dispatch($user, 'proposal', null, $updateProposal);
        }

        return redirect(route('mail.index'))->with('success', 'Berhasil menandatangani surat');
    }

    private function verify($sign, $mailId, $signerId){
        $signer = new Sha256();
        $publicKey = new Key('file://'.storage_path('jwt-sign/public.pem'));

        $parsing = (new Parser())->parse((string) $sign);
//        $jti = $parsing->getHeader('jti');

        if($parsing->verify($signer, $publicKey)){
            $data = new ValidationData();
            $data->setIssuer(env('APP_URL'));
            $data->setAudience($mailId);
            $data->setSubject($signerId);
//        $data->setId($jti);

            $validation = $parsing->validate($data);
            if($validation) {
                $result = [
                  'data' => $parsing->getClaim('data')
                ];
                return $result;
            }else{
                return false;
            }

        }else{
            return false;
        }

    }

    public function getHistory(Request $request){
        if($request->ajax()){
            $mails = null;
            if (Auth::user()->hasRole('Dekan')){
                $mails = Mail::where('sign', '!=', null)->whereHas('type', function (Builder $query){
                    $query->where('signed_code', 'D');
                })->with('type')->orderBy('created_at', 'DESC')->get();

                return DataTables::of($mails)
                    ->addIndexColumn()
                    ->addColumn('jenis', function ($row){
                        return strtolower($row->type->title);
                    })
                    ->addColumn('action', function ($row){
                        return view('mail.history.datatables', compact('row'))->render();
                    })
                    ->rawColumns(['jenis', 'action'])
                    ->make(true);
            }elseif (Auth::user()->hasRole(['Sekretaris Dekan', 'Kabag'])){
                $mails = Mail::orderBy('created_at', 'desc')->with('proposal')->get();

                return DataTables::of($mails)
                    ->addIndexColumn()
                    ->addColumn('jenis', function ($row){
                        return strtolower($row->type->title);
                    })
                    ->addColumn('action', function ($row){
                        return view('mail.history.datatables', compact('row'))->render();
                    })
                    ->rawColumns(['jenis', 'action'])
                    ->make(true);
            }else{
                return redirect()->back()->with('error', 'Halaman tidak dapat diakses');
            }
        }

        return view('mail.history.index');
    }
}
