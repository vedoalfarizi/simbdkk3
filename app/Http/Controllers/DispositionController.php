<?php

namespace App\Http\Controllers;

use App\Helper\Common;
use App\Jobs\SendEmail;
use App\Model\Disposition;
use App\Model\Mail;
use App\Model\Proposal;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class DispositionController extends Controller
{
    public function index(Request $request){
        if($request->ajax()){
            if(Auth::user()->hasRole('Dekan')){
                $proposals = Proposal::where('status', config('value.progress.submitted'))
                    ->orWhereHas('dispositions', function ($q){
                        $q->where('from_id', Auth::user()->id)
                            ->where('replied', config('value.dispositionStatus.revision'));
                    })->orderBy('held_on', 'ASC')
                    ->with('teams', 'event', 'budgets', 'dispositions')
                    ->get();

                return DataTables::of($proposals)
                    ->removeColumn('location', 'status', 'created_at', 'updated_at',
                        'teams.proposal_id', 'created_at', 'updated_at')
                    ->addIndexColumn()
                    ->addColumn('event', function ($row){
                        if($row->organization) {
                            return 'Melaksanakan '.$row->event->name;
                        }else{
                            return 'Mengikuti '.$row->event->name;
                        }
                    })
                    ->addColumn('info', function ($row){
                        return view('disposition.datatables.info', compact('row'))->render();
                    })
                    ->addColumn('duration', function ($row){
                        if($row->end_on){
                            $duration = $row->held_on->diffInDays($row->end_on) + 1;
                            return $duration.' hari';
                        }
                        return '1 hari';
                    })
                    ->editColumn('funds_needed', function ($row){
                        return $row->fundsNeededRupiah;
                    })
                    ->addColumn('deadline', function ($row){
                        $message = Common::getDeadlineMessage($row->held_on);

                        return $message;
                    })
                    ->addColumn('action', function ($row){
                        return view('disposition.datatables.action', compact('row'))->render();
                    })
                    ->rawColumns(['info', 'duration', 'action', 'deadline'])
                    ->make(true);

            }elseif(Auth::user()->hasRole('Wadek II|Kabag|Kasubag|Keuangan')){
                $authId = Auth::user()->id;
                if(Auth::user()->hasRole('Keuangan')){
                    $kasubag = User::role('Kasubag')->first();
                    $authId = $kasubag->id;
                }
                $dispositions = Disposition::where('to_id', $authId)
                    ->where('replied', config('value.dispositionStatus.waiting'))->orderBy('created_at', 'ASC')
                    ->with(array('proposal' => function($query){
                        $query->with('event')->select('id', 'title', 'proposal_url', 'event_id');
                    }))
                    ->get();

                return DataTables::of($dispositions)
                    ->removeColumn('to_id', 'updated_at')
                    ->addIndexColumn()
                    ->addColumn('action', function ($row){
                        return '<a target="_blank" href="'.url('storage/'.$row->proposal->proposal_url).'" class="btn btn-info"><small>Proposal</small></a>
                                <a href="'.route('disposition.show', $row->proposal_id).'" class="btn btn-primary"><small>Proses</small></a>';
                    })
                    ->addColumn('title', function ($row){
                        return '<b>'.$row->proposal->event->name.'</b> '.$row->proposal->title;
                    })
                    ->editColumn('created_at', function ($row){
                        return $row->created_at->diffForHumans();
                    })
                    ->rawColumns(['title', 'action'])
                    ->make(true);
            }
        }

        return view('disposition.index');
    }

    public function store(Request $request){
        $input = $request->all();

        $message = null;
        if(!Auth::user()->hasRole('Kasubag|Keuangan') && (!isset($input['message']) || !$input['message'])){
            if($input['dispositionBtn'] === 'disposition'){
                if(config('value.dispositionMessageTemplate')[$input['templateMsg']] ===
                    config('value.dispositionMessageTemplate')[0]){
                    $message = 'Isi pesan disposisi tidak boleh kosong';

                    return redirect(route('disposition.index'))->with('error', $message);
                }
                $input['message']= '';
            }else{
                if($input['dispositionBtn'] === 'revision'){
                    $message = 'Catatan revisi tidak boleh kosong';
                }else if($input['dispositionBtn'] === 'declined'){
                    $message = 'Alasan penolakan tidak boleh kosong';
                }
                return redirect(route('disposition.index'))->with('error', $message);
            }
        }

        $proposal = Proposal::whereId($input['proposal_id'])->with('users')->first();
        if($input['dispositionBtn'] === 'disposition'){
            $sendTo = null;
            $authId = Auth::user()->id;
            DB::beginTransaction();
            try{
                if(Auth::user()->hasRole('Keuangan')) {
                    $message = 'Berhasil konfirmasi berkas';
                    if($proposal->organization_id) {
                        $proposal->status = config('value.progress.mail-done');
                    }else{
                        $mailDone = Mail::where('proposal_id', $proposal->id)->where(function($q){
                            $q->where('is_manual_sign', true)->orWhere(function ($query){
                                $query->where('is_manual_sign', false)->where('sign', '!=', null);
                            });
                        })->count();
                        if($mailDone < 3){
                            $proposal->status = config('value.progress.processed');
                        }
                    }
                    $proposal->save();

                    // karena tidak ada disposisi yang ditujukan kepada keuangan, diwakili oleh kasubag
                    $kasubag = User::role('Kasubag')->first();
                    $authId = $kasubag->id;
                }else{
                    $message = 'Berhasil disposisi proposal';
                    if(Auth::user()->hasRole('Dekan')){
                        $sendTo = User::role($input['dispositionTo'])->first();

                        $proposal->status = config('value.progress.disposed');
                        $proposal->funds_receive = $input['funds'];
                        $proposal->save();
                    }elseif (Auth::user()->hasRole('Wadek II')){
                        $sendTo = User::role('Kabag')->first();
                    }elseif (Auth::user()->hasRole('Kabag')){
                        $sendTo = User::role('Kasubag')->first();
                    }

                    if(!$input['message']){
                        $dispositionMsg = config('value.dispositionMessageTemplate')[$input['templateMsg']];
                    }else{
                        $dispositionMsg = '<br>'.$input['message'].'<br>'.config('value.dispositionMessageTemplate')[$input['templateMsg']];
                    }

                    $disposition = new Disposition();
                    $disposition->proposal_id = $input['proposal_id'];
                    $disposition->from_id = Auth::user()->id;
                    $disposition->to_id = $sendTo->id;
                    $disposition->message = $dispositionMsg;
                    $disposition->save();
                }

                if(!Auth::user()->hasRole('Dekan')){
                    $updateDisposition = Disposition::where('proposal_id', $input['proposal_id'])
                        ->where('to_id', $authId)->first();
                    $updateDisposition->replied = config('value.dispositionStatus.disposed');
                    $updateDisposition->save();
                }
            }catch (\Exception $exception){
                DB::rollback();
                return redirect(route('disposition.index'))->with('error', 'Sistem error, silahkan coba kembali');
            }
            DB::commit();

            if(Auth::user()->hasRole('Keuangan')){
                if($proposal->organization_id) {
                    $user = User::find($proposal->proposed_by);
                    SendEmail::dispatch($user, 'proposal', null, $proposal);
                }else{
                    $sekdek = User::role('Sekretaris Dekan')->first();
                    SendEmail::dispatch($sekdek, 'proposal', null, $proposal);
                }
            }else{
                SendEmail::dispatch($sendTo, 'disposition', null, $proposal);

                if($sendTo->hasRole('Kasubag')){
                    $keuangan = User::role('Keuangan')->first();
                    SendEmail::dispatch($keuangan, 'disposition', null, $proposal);
                }
            }
        }else if($input['dispositionBtn'] === 'revision'){
            $message = 'Berhasil mengirimkan revisi';
            if(Auth::user()->hasRole('Dekan')){
                $proposal->status = config('value.progress.revised-dekan');
                $proposal->revision = $input['message'];
                $proposal->save();

                $user = User::find($proposal->proposed_by);
                SendEmail::dispatch($user, 'proposal', null, $proposal);
            }else if(Auth::user()->hasRole('Wadek II')){
                $updateDisposition = Disposition::where('proposal_id', $input['proposal_id'])
                    ->where('to_id', Auth::user()->id)->first();
                $updateDisposition->replied = config('value.dispositionStatus.revision');
                $updateDisposition->message = $input['message'];
                $updateDisposition->save();

                $user = User::role('Dekan')->first();
                SendEmail::dispatch($user, 'disposition', null, $proposal);
            }
        }else if($input['dispositionBtn'] === 'declined'){
            $proposal->status = config('value.progress.declined-dekan');
            $proposal->revision = $input['message'];
            $message = 'Berhasil menolak proposal';
            $proposal->save();

            $user = User::find($proposal->proposed_by);
            SendEmail::dispatch($user, 'proposal', null, $proposal);
        }

        return redirect(route('disposition.index'))->with('success', $message);
    }

    public function show($proposal_id){
        $dispositions = Disposition::where('proposal_id', $proposal_id)->orderBy('created_at', 'ASC')
            ->with(array('dispositionBy' => function($query){
                $query->select('id', 'name');
            }))
            ->with(array('dispositionTo' => function($query){
                $query->select('id', 'name');
            }))
            ->with('proposal')
            ->get();

        return view('disposition.show', compact('dispositions', 'proposal_id'));
    }

    public function getHistory(Request $request){
        if($request->ajax()){
            $userId = null;
            if(Auth::user()->hasRole('Dekan|Wadek II|Kabag')){
                $userId = Auth::user()->id;
            }

            $dispositions = Disposition::where('from_id', $userId)->orderBy('created_at', 'DESC')
                ->with(['proposal' => function($query){
                    $query->with(['event', 'users']);
                }])->get();

            return DataTables::of($dispositions)
                ->removeColumn('replied', 'created_at', 'updated_at')
                ->addIndexColumn()
                ->addColumn('name', function ($row){
                    return $row->proposal->titleWithStatus.'<br><small><b>'.$row->proposal->level.'</b></small>';
                })
                ->addColumn('teams', function ($row){
                    if(count($row->proposal->users) <= 1){
                        $result = $row->proposal->users[0]->name;
                    }else{
                        $result = '<ol style="padding: 1rem">';
                        foreach ($row->proposal->users as $user){
                            $result .= '<li>'.$user->name.'</li>';
                        }
                        $result .= '</ol>';
                    }
                    return $result;
                })
                ->addColumn('funded', function ($row){
                    return '<small>'.$row->proposal->funds_receive_rupiah.'</small>';
                })
                ->addColumn('disposition_at', function ($row){
                    return $row->created_at->format('d M Y');
                })
                ->addColumn('action', function ($row){
                    return view('disposition.datatables.history-action', compact('row'))->render();
                })
                ->rawColumns(['name', 'teams', 'message', 'funded', 'disposition_at', 'action'])
                ->make(true);
        }

        return view('disposition.history');
    }

    public function reDisposition(Request $request){
        $input = $request->all();

        if($input['templateMsg'] == 0 && !$input['message']){
            return redirect(route('disposition.index'))->with('error', 'Pesan disposisi tidak boleh kosong');
        }

        $proposal = Proposal::find($input['proposal_id']);
        if(!$proposal){
            return redirect(route('disposition.index'))->with('error', 'Proposal tidak ditemukan');
        }

        DB::beginTransaction();
        try{
            $proposal->funds_receive = $input['funds'];
            $proposal->save();

            $updateDisposition = Disposition::where('proposal_id', $input['proposal_id'])
                ->where('from_id', Auth::user()->id)->first();
            $updateDisposition->replied = config('value.dispositionStatus.waiting');
            $updateDisposition->message = $input['message'].'<br>'.config('value.dispositionMessageTemplate')[$input['templateMsg']];
            $updateDisposition->save();
        }catch(\Exception $e){
            DB::rollBack();
            return redirect(route('disposition.index'))->with('error', 'Terjadi kesalahan sistem, coba kembali.');
        }

        DB::commit();

        $user = User::role('Wadek II')->first();
        SendEmail::dispatch($user, 'disposition', null, $proposal);
        return redirect(route('disposition.index'))->with('success', 'Berhasil mendisposisikan kembali');
    }
}
