<?php

namespace App\Http\Controllers;

use App\Model\Achievement;
use App\Model\Proposal;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class MahasiswaController extends Controller
{
    public function index(){
        $userId = Auth::id();

        $reportProposal = Proposal::whereHas('teams', function (Builder $team) use ($userId){
            $team->whereUserId($userId);
        })->countByStatus()->first()->toArray();

        $reportOther = Proposal::whereOrganizationId(null)->where('status', '<=', config('value.progress.completed'))
            ->whereHas('teams', function (Builder $team) use ($userId){
                $team->whereUserId($userId);
            })->countByEventAndFunding()->first()->toArray();

        $reportProposal = array_merge($reportProposal, $reportOther);
        $achievements = Achievement::whereHas('proposal', function(Builder $proposal) use ($userId){
            $proposal->whereHas('teams', function (Builder $team) use ($userId){
                $team->whereUserId($userId);
            });
        })->where('user_id', null)->orWhere('user_id', $userId)
            ->with('proposal.event')->orderBy('created_at', 'desc')->get();

        return view('mahasiswa.home', compact('reportProposal', 'achievements'));
    }
}
