<?php

namespace App\Http\Controllers;

use App\Helper\Common;
use App\Model\Achievement;
use App\Model\AnnualBudget;
use App\Model\Disposition;
use App\Model\Mail;
use App\Model\Proposal;
use App\Model\Team;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DekanController extends Controller
{
    public function index(){
        $mail = Mail::countByStatus()->first();
        $mail->waiting -= ($mail->manual + $mail->revision);
        $mail->total -= $mail->manual;

        $proposal = Proposal::countByStatusDisposition()->first();
        $revisedDispositions = Disposition::where('from_id', Auth::user()->id)->where('replied', 2)->count();
        $proposal->request += $revisedDispositions;
        $proposal->disposed -= $revisedDispositions;

        $startYear = Carbon::now()->subYear(4)->year;
        $nowYear = Carbon::now()->year;
        $budget = AnnualBudget::find($nowYear);
        if($budget){
            $funding = Proposal::realisation()->first();
            $budget = $budget->amount - $funding->funding;
        }else{
            $budget = 0;
        }

        $bestStudents = Achievement::select('proposal_id', 'user_id', 'reward')->orderBy('user_id')
            ->with(
                ['user' => function($query){
                    $query->select('id', 'department_id', 'name');
                }]
            )
            ->with(
                ['proposal' => function($query){
                    $query->select('id', 'level');
                }]
            )
            ->thisYear()->get();

        $bestStudents = $bestStudents->reduce(function ($carry, $item){
            $nim = $item->user_id;
            if($nim == null){
                $nims = Team::where('proposal_id', $item->proposal_id)->with('user')->get(['user_id']);
                foreach ($nims as $nim){
                    if(empty($carry[$nim->user_id])){
                        $carry[$nim->user_id]['name'] = $nim->user->name;
                        $carry[$nim->user_id]['nim'] = $nim->user_id;
                        $carry[$nim->user_id]['point'] = Common::getPoint($item->proposal->level, $item->reward);
                    }else{
                        $carry[$nim->user_id]['point'] += Common::getPoint($item->proposal->level, $item->reward);
                    }
                }

                return $carry;
            }

            if(empty($carry[$nim])){
                $carry[$nim]['name'] = $item->user->name;
                $carry[$nim]['nim'] = $nim;
                $carry[$nim]['point'] = Common::getPoint($item->proposal->level, $item->reward);
            }else{
                $carry[$nim]['point'] += Common::getPoint($item->proposal->level, $item->reward);
            }

            return $carry;
        }, []);

        usort($bestStudents, function ($a, $b){
            return ($a['point'] > $b['point']) ? -1 : 1;
        });

        $proposalEvents = Proposal::eventReport()->lastFiveYear()->get();

        $achievementLevels = DB::table('achievements')
            ->join('proposals', 'achievements.proposal_id', '=', 'proposals.id')
            ->select(
                'level',
                DB::raw('YEAR(achievements.created_at) as year'),
                DB::raw('count(level) as total')
            )
            ->where('achievements.user_id', '!=', null)
            ->whereYear('achievements.created_at', '<=', $nowYear)
            ->whereYear('achievements.created_at', '>=', $startYear)
            ->groupBy('level', 'year')
            ->get();

        $anotherAchievementLevels = DB::table('achievements')
            ->join('proposals', 'achievements.proposal_id', 'proposals.id')
            ->select(
                'level',
                DB::raw('YEAR(achievements.created_at) as year'),
                DB::raw('count(level) as total')
            )
            ->where('achievements.user_id', '=', null)
            ->whereYear('achievements.created_at', '<=', $nowYear)
            ->whereYear('achievements.created_at', '>=', $startYear)
            ->groupBy('proposals.id', 'level', 'year')
            ->get();

        $achievementLevels = $achievementLevels->merge($anotherAchievementLevels);

        $achievementLevels = $achievementLevels->reduce(function ($carry, $item){
            $key = $item->year;
            $subKey = $item->level;

            if(empty($carry[$key])){
                $carry[$key][$subKey] = $item->total;
            }else{
                if(empty($carry[$key][$subKey])){
                    $carry[$key][$subKey] = $item->total;
                }else{
                    $carry[$key][$subKey] += $item->total;
                }
            }

            return $carry;
        }, []);
        ksort($achievementLevels, 1);

        return view('dekan.home', compact('mail', 'proposal', 'budget', 'bestStudents', 'proposalEvents', 'achievementLevels'));
    }

    public function getUserHistory($proposal_id){
        $users = Team::where('proposal_id', $proposal_id)->get(['user_id']);

        $proposals = [];
        $proposalCheck = [];
        foreach ($users as $user){
            $proposalUser = Proposal::where('id', '!=', $proposal_id)->where('status', '<', config('value.progress.canceled'))->whereHas('teams', function (Builder $team) use ($user) {
                $team->where('user_id', $user->user_id);
            })->with(['event', 'organization', 'achievements.user'])->get(['id', 'title', 'event_id', 'level', 'funds_receive', 'status', 'organization_id']);

            foreach ($proposalUser as $proposal){
                if(!in_array($proposal->id, $proposalCheck)){
                    array_push($proposalCheck, $proposal->id);
                    array_push($proposals, $proposal);
                }

            }
        }

        return view('dekan.user-history', compact('proposals', 'proposal_id'));
    }
}
