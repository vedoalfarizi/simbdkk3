<?php

namespace App\Http\Controllers;

use App\Model\Achievement;
use App\Model\Documentation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function download(Request $request, $id){
        $type = $request->input('type');

        $file = null;
        switch ($type){
            case 'documentation':
                $documentation = Documentation::find($id);
                $file = $documentation->photo_url;
                break;
            case 'achievement':
                $achievement = Achievement::find($id);
                $file = $achievement->certificate_url;
                break;
            default:
                return redirect()->back()->with('error', 'Gagal download');

        }

        return Storage::disk('public')->download($file);
    }

    public function downloadByName($type, $name){
        if(Storage::disk('public')->exists($type.'/'.$name)){
            return Storage::disk('public')->download($type.'/'.$name);
        }

        return redirect()->back()->with('error', 'Gagal download');
    }
}
