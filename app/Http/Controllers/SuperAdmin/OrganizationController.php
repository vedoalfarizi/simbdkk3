<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrganizationStoreRequest;
use App\Model\Organization;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class OrganizationController extends Controller
{
    public function index(Request $request){
        if($request->ajax()){
            Carbon::setLocale('id');

            $organizations = Organization::orderBy('created_at', 'DESC')->get();

            return DataTables::of($organizations)
                ->removeColumn('updated_at')
                ->addIndexColumn()
                ->addColumn('action', function ($row){
                    return view('superadmin.organization.datatables.sub-menu', compact('row'))->render();
                })
                ->editColumn('created_at', function ($row){
                    return $row->created_at->diffForHumans();
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }

        return view('superadmin.organization.index');
    }

    public function create(){
        return view('superadmin.organization.create');
    }

    public function store(OrganizationStoreRequest $request){
        $organization = new Organization();
        $organization->name = $request->name;
        $organization->save();

        return redirect(route('admin.organization.index'))->with('success', 'Berhasil menambahkan organisasi');
    }

    public function edit($id){
        $organization = Organization::find($id);

        if(!$organization){
            return redirect(route('admin.organization.index'))->with('error', 'Data tidak ditemukan');
        }

        return view('superadmin.organization.edit', compact('organization'));
    }

    public function update(Request $request, $id){
        $organization = Organization::find($id);

        if(!$organization){
            return redirect(route('admin.organization.index'))->with('error', 'Data tidak ditemukan');
        }

        $organization->name = $request->name;
        $organization->save();

        return redirect(route('admin.organization.index'))->with('success', 'Berhasil mengubah data organisasi');
    }

    public function destroy($id){
        $organization = Organization::find($id);

        if(!$organization){
            return redirect(route('admin.organization.index'))->with('error', 'Data tidak ditemukan');
        }

        $organization->delete();

        return redirect(route('admin.organization.index'))->with('success', 'Berhasil menghapus data organisasi');
    }
}
