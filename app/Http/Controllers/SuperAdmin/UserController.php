<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Model\Department;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index(Request $request){
        if($request->ajax()){
            Carbon::setLocale('id');

            $users = User::withTrashed()->role(['Mahasiswa', 'Sekretaris Dekan', 'Kasubag', 'Kabag', 'Wadek II', 'Dekan', 'Dosen'])->with('department')->orderBy('created_at', 'desc')->get();

            return DataTables::of($users)
                ->removeColumn('password', 'remember_token', 'updated_at')
                ->addIndexColumn()
                ->addColumn('action', function ($row){
                    return view('superadmin.user.datatables.action', compact('row'))->render();
                })
                ->addColumn('status', function ($row){
                    return empty($row->deleted_at) ? '<span class="badge badge-success">Aktif</span>' : '<span class="badge badge-danger">Tidak aktif</span>';
                })
                ->editColumn('department_id', function ($row){
                    return empty($row->department_id) ? '-' : $row->department->name;
                })
                ->editColumn('created_at', function ($row){
                    return $row->created_at->format('d M Y');
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }

        return view('superadmin.user.index');
    }

    public function updateStatus($id, Request $request){
        $user = User::withTrashed()->find($id);

        if($request->status == null){
            $user->delete();

            return redirect(route('admin.user.index'))->with('success', 'Berhasil menonaktifkan pengguna');
        }

        $user->restore();

        return redirect(route('admin.user.index'))->with('success', 'Berhasil mengaktifkan pengguna');
    }

    public function create(){
        $departments = Department::orderBy('name', 'asc')->pluck('name', 'id');
        $roles = Role::whereNotIn('name', ['Super Admin', 'Dosen'])->orderBy('name', 'asc')->pluck('name', 'id');

        return view('superadmin.user.create', compact('departments', 'roles'));
    }

    public function store(UserStoreRequest $request){
        $input = $request->all();

        $user = new User();
        $user->id = $input['id'];
        $user->department_id = $input['department'];
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = $input['password'];
        $user->save();

        if($input['role'] != "2"){
            $this->noRoleDuplication($input['role'], $input['id']);
        }
        $user = User::find($input['id']);
        $user->assignRole($input['role']);

        return redirect(route('admin.user.index'))->with('success', 'Berhasil menambahkan pengguna');
    }

    public  function destroy($id){
        $user = User::onlyTrashed()->find($id);

        if(!$user){
            return redirect(route('admin.user.index'))->with('error', 'Pengguna tidak ditemukan');
        }

        $user->forceDelete();

        return redirect(route('admin.user.index'))->with('success', 'Berhasil menghapus pengguna');
    }

    public function edit($id){
        $user = User::whereId($id)->with('roles')->first();

        if(!$user){
            return redirect(route('admin.user.index'))->with('error', 'Data pengguna tidak ditemukan');
        }

        $departments = Department::orderBy('name', 'asc')->pluck('name', 'id');
        $roles = Role::whereNotIn('name', ['Super Admin', 'Dosen'])->orderBy('name', 'asc')->pluck('name', 'id');

        $departmentId = $user->department_id;
        $roleId = $user->roles[0]->id;

        return view('superadmin.user.edit', compact('user', 'departments', 'roles', 'departmentId', 'roleId'));
    }

    public function update(User $user, UserUpdateRequest $request){
        $input = $request->all();

        if($input['role'] == 2 && $input['department'] == null){
            return redirect()->back()->withInput()->with('error', 'Pilihan jurusan tidak boleh kosong');
        }

        if($user->email != $input['email']){
            $duplicatedEmail = User::where('id', '!=', $user->id)->where('email', $input['email'])->first();
            if($duplicatedEmail){
                return redirect()->back()->withInput()->with('error', 'Email sudah digunakan pengguna lain');
            }
        }

        if($input['role'] != "2" && $user->roles[0]->id != $input['role']){
            $this->noRoleDuplication($input['role'], $input['id']);
        }

        $user->id = $input['id'];
        $user->department_id = $input['department'];
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->save();

        $user->syncRoles($input['role']);

        return redirect(route('admin.user.index'))->with('success', 'Berhasil mengubah data pengguna');
    }

    private function noRoleDuplication($role, $id){
        $user = User::where('id', '!=', $id)->whereHas('roles', function ($q) use ($role){
            $q->where('id',$role );
        })->first();

        if($user){
            $user->syncRoles('Dosen');
        }
    }

}
