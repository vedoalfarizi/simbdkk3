<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Model\Department;
use App\Model\Event;
use App\Model\Organization;
use App\User;
use Illuminate\Database\Eloquent\Builder;

class HomeController extends Controller
{
    public function index(){
        $user = User::whereHas('roles', function (Builder $query){
            $query->whereNotIn('name', ['Super Admin', 'Doosen']);
        })->count();
        $organization = Organization::all()->count();
        $department = Department::all()->count();
        $event = Event::all()->count();

        return view('superadmin.home', compact('user', 'organization', 'department', 'event'));
    }
}
