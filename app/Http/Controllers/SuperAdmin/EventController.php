<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventStoreRequest;
use App\Model\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class EventController extends Controller
{
    public function index(Request $request){
        if($request->ajax()){
            Carbon::setLocale('id');

            $events = Event::orderBy('name', 'ASC')->get();

            return DataTables::of($events)
                ->removeColumn('updated_at')
                ->addIndexColumn()
                ->addColumn('action', function ($row){
                    return view('superadmin.events.datatables.sub-menu', compact('row'))->render();
                })
                ->editColumn('created_at', function ($row){
                    return $row->created_at->diffForHumans();
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }

        return view('superadmin.events.index');
    }

    public function create(){
        return view('superadmin.events.create');
    }

    public function store(EventStoreRequest $request){
        $event = new Event();
        $event->name = $request->name;
        $event->save();

        return redirect(route('admin.events.index'))->with('success', 'Berhasil menambahkan jenis kegiatan');
    }

    public function edit($id){
        $event = Event::find($id);

        if(!$event){
            return redirect(route('admin.events.index'))->with('error', 'Data tidak ditemukan');
        }

        return view('superadmin.events.edit', compact('event'));
    }

    public function update(Request $request, $id){
        $event = Event::find($id);

        if(!$event){
            return redirect(route('admin.events.index'))->with('error', 'Data tidak ditemukan');
        }

        $event->name = $request->name;
        $event->save();

        return redirect(route('admin.events.index'))->with('success', 'Berhasil mengubah data jenis kegiatan');
    }

    public function destroy($id){
        $event = Event::find($id);

        if(!$event){
            return redirect(route('admin.events.index'))->with('error', 'Data tidak ditemukan');
        }

        $event->delete();

        return redirect(route('admin.events.index'))->with('success', 'Berhasil menghapus data jenis kegiatan');
    }

}
