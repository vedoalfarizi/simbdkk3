<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DepartmentStoreRequest;
use App\Model\Department;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class DepartmentController extends Controller
{
    private function findDepartment($id){
        $department = Department::find($id);

        if(!$department){
            return null;
        }

        return $department;
    }

    public function index(Request $request){
        if($request->ajax()){
            Carbon::setLocale('id');

            $departments = Department::orderBy('name', 'ASC')->get();

            return DataTables::of($departments)
                ->removeColumn('updated_at')
                ->addIndexColumn()
                ->addColumn('action', function ($row){
                    return view('superadmin.department.datatables.sub-menu', compact('row'))->render();
                })
                ->editColumn('created_at', function ($row){
                    return $row->created_at->diffForHumans();
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }

        return view('superadmin.department.index');
    }

    public function create(){
        return view('superadmin.department.create');
    }

    public function store(DepartmentStoreRequest $request){
        $department = new Department();
        $department->name = $request->name;
        $department->save();

        return redirect(route('admin.department.index'))->with('success', 'Berhasil menambahkan jurusan');
    }

    public function edit($id){
        $department = $this->findDepartment($id);

        if(!$department){
            return redirect(route('admin.department.index'))->with('error', 'Data tidak ditemukan');
        }

        return view('superadmin.department.edit', compact('department'));
    }

    public function update(Request $request, $id){
        $department = $this->findDepartment($id);

        if(!$department){
            return redirect(route('admin.department.index'))->with('error', 'Data tidak ditemukan');
        }

        $department->name = $request->name;
        $department->save();

        return redirect(route('admin.department.index'))->with('success', 'Berhasil mengubah data jurusan');
    }

    public function destroy($id){
        $department = $this->findDepartment($id);

        if(!$department){
            return redirect(route('admin.department.index'))->with('error', 'Data tidak ditemukan');
        }

        $department->delete();

        return redirect(route('admin.department.index'))->with('success', 'Berhasil menghapus data jurusan');
    }
}
