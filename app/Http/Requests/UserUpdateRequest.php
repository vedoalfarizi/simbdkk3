<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => array(
                'required',
                'max:20',
                'regex:/^[0-9 ]*$/',
                'unique:users,id,'.$this->user->id
            ),
            'name' => array(
                'required',
                'max:40',
                'regex:/[A-Z a-z]+/'
            ),
            'email' => array(
              'required',
              'email',
              'max:60'
            ),
            'role' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'please enter your NIM or NIP',
            'id.max' => 'please enter a valid NIM or NIP',
            'id.regex' => 'please enter a valid NIM or NIP',
            'id.unique' => 'this NIM or NIP has been taken',
            'name.required' => 'please enter your name',
            'name.max' => "name can't be more than 40 characters",
            'name.regex' => 'please enter a valid name',
            'email.required' => 'please enter your email',
            'email.email' => 'please input a valid email',
            'email.unique' => 'this email has been taken',
            'email.max' => "email can't be more than 60 characters",
            'role.required' => 'please select a role',
        ];
    }
}
