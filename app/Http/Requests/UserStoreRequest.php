<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => array(
                'required',
                'max:20',
                'regex:/^[0-9]*$/'
            ),
            'name' => array(
                'required',
                'max:40',
                'regex:/[A-Z a-z]+/'
            ),
            'email' => 'required|email|unique:users|max:60',
            'department' => 'nullable|numeric',
            'password' => 'required|min:6',
            'role' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'Tuliskan NIM atau NIP',
            'id.max' => 'NIM atau NIP tidak valid',
            'id.regex' => 'NIM atau NIP tidak valid',
            'name.required' => 'Tuliskan nama lengkap',
            'name.max' => "Nama lengkap terlalu panjang",
            'name.regex' => 'Nama lengkap tidak valid',
            'email.required' => 'Tuliskan email',
            'email.email' => 'Email tidak valid',
            'email.unique' => 'Email sudah terdaftar',
            'email.max' => "Email terlalu panjang",
            'password.required' => 'Tuliskan password',
            'role.required' => 'Pilih role',
        ];
    }
}
