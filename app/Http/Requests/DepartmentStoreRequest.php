<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:departments|max:64',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'tidak boleh kosong',
            'name.string' => 'tidak valid',
            'name.unique' => 'nama ini sudah terdaftar',
            'name.max' => 'tidak boleh lebih dari 64 karakter',
        ];
    }
}
