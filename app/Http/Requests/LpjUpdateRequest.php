<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LpjUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'realisation' => array(
                'required',
                'regex:/^[0-9]*$/'
            ),
            'lpj' => 'nullable|mimes:pdf|max:5182',
            'receipt' => 'nullable|mimes:pdf|max:5182',
        ];
    }

    public function messages()
    {
        return [
            'realisation.required' => 'Dana yang digunakan harus diisi',
            'realisation.regex' => 'Masukkan jumlah yang benar',
            'lpj.mimes' => 'LPJ harus berupa file PDF',
            'lpj.max' => 'File LPJ terlalu besar',
            'receipt.mimes' => 'Kwitansi harus berupa file PDF',
            'receipt.max' => 'File Kwitansi terlalu besar',
        ];
    }
}
