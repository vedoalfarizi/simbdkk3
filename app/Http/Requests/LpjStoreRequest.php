<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LpjStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'realisation' => array(
                'required',
                'regex:/^[0-9]*$/'
            ),
            'lpj' => 'required|file|mimes:pdf|max:5182',
            'receipt' => 'required|file|mimes:pdf|max:5182',
            'photo' => 'required',
            'photo.*' => 'image|max:5182',
            'desc' => 'required',
            'desc.*' => 'nullable|string|max:255',
            'achievement' => 'nullable',
            'achievement.*' => 'nullable|file|mimes:jpeg,png,pdf|max:2182',
            'nim' => 'nullable',
            'nim.*' => array(
                'nullable',
                'exists:users,id',
                'max:20',
                'regex:/^[0-9 ]*$/'
            ),
            'reward' => 'nullable',
            'reward.*' => 'nullable|string|max:20'
        ];
    }

    public function messages()
    {
        return [
            'realisation.required' => 'Dana yang digunakan harus diisi',
            'realisation.regex' => 'Masukkan jumlah yang benar',
            'lpj.required' => 'File LPJ belum dipilih',
            'lpj.file' => 'LPJ harus berupa file PDF',
            'lpj.mimes' => 'LPJ harus berupa file PDF',
            'lpj.max' => 'File LPJ terlalu besar',
            'receipt.required' => 'File kwitansi belum dipilih',
            'receipt.file' => 'Kwitansi harus berupa file PDF',
            'receipt.mimes' => 'Kwitansi harus berupa file PDF',
            'receipt.max' => 'File Kwitansi terlalu besar',
            'photo.required' => 'Dokumentasi kegiatan tidak boleh kosong',
            'photo.*.image' => 'Dokumentasi harus berupa file foto',
            'photo.*.max' => 'Foto dokumentasi terlalu besar',
            'desc.required' => 'Deskripsi dokumentasi tidak boleh kosong',
            'desc.*.string' => 'Masukkan deskripsi yang valid',
            'desc.*.max' => 'Deskripsi terlalu panjang, maksimal 255 karakter',
            'achievement.*.file' => 'Sertifikat harus berupa file PDF, PNG atau JPG',
            'achievement.*.mimes' => 'Sertifikat harus berupa file PDF, PNG atau JPG',
            'achievement.*.max' => 'File sertifikat terlalu besar',
            'nim.*.exists' => 'NIM belum terdaftar pada sistem',
            'nim.*.max' => 'Masukkan NIM yang valid',
            'nim.*.regex' => 'Masukkan NIM yang valid',
            'reward.required' => 'Jenis penghargaan belum dipilih',
            'reward.*.string' => 'Jenis penghargaan tidak valid',
            'reward.*.max' => 'Jenis penghargaan tidak valid',
        ];
    }
}
