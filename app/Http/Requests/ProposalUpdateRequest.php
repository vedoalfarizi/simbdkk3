<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProposalUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'title' => 'required|string|max:255',
            'desc' => 'required|string|max:500',
            'event_id' => 'required',
            'level' => 'required',
            'start' => 'required|date',
            'end' => 'nullable|date|after:start',
            'location' => 'required|string|max:255',
        ];
    }

    public function messages()
    {
        return [
            'type.required' => 'Pilih jenis proposal',
            'title.required' => 'Nama kegiatan tidak boleh kosong',
            'title.string' => 'Nama kegiatan tidak valid',
            'title.max' => 'Nama kegiatan tidak boleh lebih dari 255 karakter',
            'desc.required' => 'Deskripsi kegiatan tidak boleh kosong',
            'desc.string' => 'Deskripsi kegiatan tidak valid',
            'desc.max' => 'Deskripsi kegiatan tidak boleh lebih dari 500 karakter',
            'event_id.required' => 'Jenis kegiatan tidak boleh kosong',
            'level.required' => 'Tingkat kegiatan tidak boleh kosong',
            'start.required' => 'Tanggal mulai kegiatan tidak boleh kosong',
            'start.date' => 'Tanggal tidak valid',
            'end.date' => 'Tanggal tidak valid',
            'end.after' => 'Tanggal tidak valid',
            'location.required' => 'Lokasi kegiatan tidak boleh kosong',
            'location.string' => 'Lokasi kegiatan tidak valid',
            'location.max' => 'Lokasi kegiatan tidak boleh lebih dari 255 karakter',
        ];
    }
}
