## Sistem Informasi Manajemen Bantuan Dana Kegiatan Kemahasiswaan (SIMBAKWA)

SIMBAKWA merupakan aplikasi yang digunakan untuk memudahkan prosedur pengajuan bantuan dana kegiatan kemahasiswaan dan pelaporan kegiatan.
Aplikasi ini menerapkan digital signature sebagai pengganti tanda tangan basah pada surat keterangan, izin atau surat tugas.
Dibangun dengan framework [Laravel](https://laravel.com/) dan database [MySQL](https://www.mysql.com/).

## Prerequisites
- PHP `7.2` or greater [PHP](https://www.php.net/downloads)
- MySql `5.7.9` or greater [MySQL](https://downloads.mysql.com/archives/installer/)
- Alternatively, if you don't want to install PHP & MySQL separately you can use [XAMPP](https://www.apachefriends.org/download.html) or [Laragon](https://laragon.org/download/index.html)
- Composer `2.0.0` or greater [Composer](https://getcomposer.org/download/)

## Steps
- create database locally
- Rename .env.example file to .env (or copy it) and fill the database information
- Open the console and cd your project root directory
- Run `composer install` or `php composer.phar install`
- Run `php artisan key:generate`
- Run `php artisan migrate`
- Run `php artisan db:seed` to run seeders
- Run `php artisan serve`

Access SIMBDKK  at localhost:8000
