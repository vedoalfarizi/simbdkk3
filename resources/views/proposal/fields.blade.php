<div class="row">
    {!! Form::hidden('type', $type) !!}
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::label('title', 'Nama kegiatan') !!}
            {!! Form::text('title', null, ['id' => 'title', 'class' => 'form-control '.($errors->has('title') ? 'is-invalid' : ''), 'placeholder' => 'Tuliskan nama kegiatan', 'autocomplete' => 'title']) !!}
            @error('title')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('desc', 'Ringkasan deskripsi kegiatan') !!}
            {!! Form::textarea('desc', null, ['id' => 'desc', 'class' => 'form-control '.($errors->has('desc') ? 'is-invalid' : ''), 'autocomplete' => 'name', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
            @error('desc')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('event_id', 'Jenis kegiatan') !!}
            {!! Form::select('event_id', $events, null, ['id' => 'event_id', 'class' => 'form-control '.($errors->has('event_id') ? 'is-invalid' : ''), 'placeholder' => 'Pilih jenis kegiatan']) !!}
            @error('event_id')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('level', 'Tingkat kegiatan') !!}
            {!! Form::select('level', Config::get('value.level'), null, ['id' => 'level', 'class' => 'form-control '.($errors->has('level') ? 'is-invalid' : ''), 'placeholder' => 'Pilih tingkat kegiatan']) !!}
            @error('level')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('start', 'Mulai kegiatan') !!}
            {!! Form::date('start', $proposal->held_on ?? null, ['id' => 'start', 'class' => 'form-control '.($errors->has('start') ? 'is-invalid' : ''), 'autocomplete' => 'start', 'min' => date('Y-m-d')]) !!}
            @error('start')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('end', 'Berakhir kegiatan') !!}
            {!! Form::date('end', $proposal->end_on ?? null, ['id' => 'end', 'class' => 'form-control '.($errors->has('end') ? 'is-invalid' : ''), 'autocomplete' => 'end', 'min' => date('Y-m-d', strtotime(now() . "+1 days"))]) !!}
            @error('end')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
            <small class="text-muted">*diisi jika lebih dari 1 hari</small>
        </div>
        <div class="form-group">
            {!! Form::label('location', 'Lokasi kegiatan') !!}
            {!! Form::text('location', null, ['id' => 'location', 'class' => 'form-control '.($errors->has('location') ? 'is-invalid' : ''), 'placeholder' => 'Tuliskan lokasi kegiatan', 'autocomplete' => 'location']) !!}
            @error('location')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        @if($type == 'manage')
        <div class="form-group">
            {!! Form::label('fund', 'Anggaran kebutuhan dana', ['class' => 'col-form-label']) !!}
            <div class="controls">
                <div class="input-prepend input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Rp</span>
                    </div>
                    {!! Form::text('fund', isset($proposal) ? $proposal->funds_needed : null, ['id' => 'fund', 'class' => 'form-control '.($errors->has('fund') ? 'is-invalid' : ''), 'placeholder' => 'Tuliskan jumlah dana', 'autocomplete' => 'fund']) !!}
                    @error('fund')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
        @endif

        <div class="form-group">
            {!! Form::label('cover_letter', 'Scan surat pengantar') !!} <br>
            {!! Form::file('cover_letter', null, ['id' => 'cover_letter', 'class' => 'form-control-file is-invalid', 'autocomplete' => 'cover_letter']) !!}
            <br> <small class="text-muted">*hanya format PDF, max: 2 MB</small>
            @if(isset($proposal))
                @if($proposal->cover_letter_url)
                    <br> <small class="text-info"><a target="_blank" href="{!! url('storage/'.$proposal->cover_letter_url) !!}">Lihat surat pengantar</a></small>
                @endif
            @endif
            @error('cover_letter')
            <br> <small class="text-danger">{{$message}}</small>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('proposal', 'Scan proposal') !!} <br>
            {!! Form::file('proposal', null, ['id' => 'proposal', 'class' => 'form-control-file is-invalid', 'autocomplete' => 'proposal']) !!}
            <br> <small class="text-muted">*hanya format PDF, max: 5 MB</small>
            @if(isset($proposal))
                @if($proposal->proposal_url)
                    <br> <small class="text-info"><a target="_blank" href="{!! url('storage/'.$proposal->proposal_url) !!}">Lihat proposal</a></small>
                @endif
            @endif
            @error('proposal')
            <br> <small class="text-danger">{{$message}}</small>
            @enderror
        </div>
    </div>

    <div class="col-sm-6">

    </div>
</div>