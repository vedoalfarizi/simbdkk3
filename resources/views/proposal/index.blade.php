@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mahasiswa.proposal.index') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>Data proposal dalam proses</strong>
            <div class="card-header-actions">
                <a class="btn btn-primary" href="{{route('proposal.history.index')}}">
                    Riwayat
                </a>
            </div>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover table-sm table-bordered data-table"
                   style="border-collapse: collapse !important">
                <thead class="thead-dark text-center small">
                <tr>
                    <th width="col">No</th>
                    <th scope="col">Kode</th>
                    <th scope="col">Judul</th>
                    <th scope="col">Jadwal pelaksanan</th>
                    <th scope="col">Status</th>
                    <th width="col">Aksi</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach($proposals as $proposal)
                    <tr>
                        <td>{!! $no++ !!}</td>
                        <td>
                            {!! $proposal->id !!}
                            {!! $proposal->funds_receive === null ? '' : "<br><span class='badge badge-success'>Didanai: $proposal->funds_receive_rupiah</span>" !!}
                        </td>
                        <td>
                            <button class="btn btn-sm btn-link" type="button" data-toggle="collapse"
                                    data-target="#rows-{{$proposal->id}}" aria-expanded="false"
                                    aria-controls="rows-{{$proposal->id}}">
                                <i class="fa fa-info-circle"></i> {{$proposal->title}}
                            </button>
                        </td>
                        <td>
                                <span class="small">
                                    {{ $proposal->date }}
                                </span>
                        </td>
                        <td style="text-align: center">
                            <button type="button" class="btn btn-sm btn-link" data-toggle="modal"
                                    data-target="#status-{{$proposal->id}}">
                                {{ $proposal->status_detail['label']  }}
                            </button>
                            <div class="modal fade" id="status-{{$proposal->id}}">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Informasi status proposal</h4>
                                            <button type="button" class="close" data-dismiss="modal">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <img width="100%"
                                                 src="{{URL::to('/')}}{!! $proposal->status_detail['img'] !!}"
                                                 style="margin-bottom: 5%" alt="">
                                            <p>
                                                <small>{!! $proposal->status_detail['message'] !!}</small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($proposal->status === config('value.progress.mail-done'))
                                <br>
                                <a href="{{route('mahasiswa.lpj.create', $proposal->id)}}"
                                   class="btn btn-sm btn-primary">Kirim LPJ</a>
                            @endif

                            @if($proposal->status === config('value.progress.revised-dekan'))
                                {!! Form::model($proposal, ['route' => ['mahasiswa.proposal.revision', $proposal->id], 'method' => 'put', 'class' => 'revision-form']) !!}
                                {{ Form::button('Konfirmasi revisi', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit', 'style' => 'margin-top: 5px']) }}
                                {!! Form::close() !!}
                            @elseif($proposal->status === config('value.progress.lpj-revised'))
                                <br>
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal"
                                        data-target="#lpj-revision-{{$proposal->id}}">
                                    Kirim revisi LPJ
                                </button>

                                {{--modal revisi lpj--}}
                                <div class="modal fade" id="lpj-revision-{{$proposal->id}}">
                                    <div class="modal-dialog modal-dialog-scrollable">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Perbaikan LPJ</h4>
                                                <button type="button" class="close" data-dismiss="modal">×</button>
                                            </div>

                                            {!! Form::model($proposal, ['route' => ['mahasiswa.lpj.update', $proposal->id], 'method' => 'put', 'enctype' => 'multipart/form-data']) !!}
                                            <div class="modal-body">
                                                {!! Form::label('message', 'Perbaikan', ['class' => 'float-left']) !!}
                                                {!! Form::textarea('message', $proposal->revision, ['class' => 'form-control', 'readonly' => 'true', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
                                                <hr>
                                                <div class="form-group">
                                                    {!! Form::label('realisation', 'Dana yang digunakan', ['class' => 'col-form-label float-left']) !!}
                                                    <div class="controls">
                                                        <div class="input-prepend input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Rp</span>
                                                            </div>
                                                            {!! Form::text('realisation', $proposal->funds_realisation, ['id' => 'realisation', 'class' => 'form-control '.($errors->has('realisation') ? 'is-invalid' : ''), 'autocomplete' => 'realisation']) !!}
                                                            @error('realisation')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('lpj', 'Laporan Pertanggungjawaban', ['class' => 'float-left']) !!}
                                                    <br>
                                                    {!! Form::file('lpj', null, ['id' => 'lpj', 'class' => 'form-control-file is-invalid', 'autocomplete' => 'lpj']) !!}
                                                    <br>
                                                    <small class="text-warning float-left">*hanya format PDF, max: 5
                                                        MB
                                                    </small>
                                                    @if($proposal->LPJ_url)
                                                        <br>
                                                        <small class="text-info float-left"><a target="_blank"
                                                                                               href="{!! url('storage/'.$proposal->LPJ_url) !!}">Lihat
                                                                LPJ saat ini</a></small>
                                                    @endif
                                                    @error('lpj')
                                                    <br>
                                                    <small class="text-danger">{{$message}}</small>
                                                    @enderror
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('receipt', 'Scan kwitansi', ['class' => 'float-left']) !!}
                                                    <br>
                                                    {!! Form::file('receipt', null, ['id' => 'receipt', 'class' => 'form-control-file is-invalid', 'autocomplete' => 'receipt']) !!}
                                                    <br>
                                                    <small class="text-warning float-left">*hanya format PDF, max: 5
                                                        MB
                                                    </small>
                                                    @if($proposal->receipt_url)
                                                        <br>
                                                        <small class="text-info float-left"><a target="_blank"
                                                                                               href="{!! url('storage/'.$proposal->receipt_url) !!}">Lihat
                                                                kwitansi saat ini</a></small>
                                                    @endif
                                                    @error('receipt')
                                                    <br>
                                                    <small class="text-danger">{{$message}}</small>
                                                    @enderror
                                                </div>
                                                <hr>

                                                <div class="form-group">
                                                    <a href="{{route('documentation.index', $proposal->id)}}"
                                                       class="btn btn-sm btn-outline-primary float-left"><i
                                                                class="fa fa-eye"></i> Dokumentasi</a>
                                                </div>

                                                @if(!$proposal->organization_id)
                                                    <div class="form-group">
                                                        <a href="{{route('achievement.index', $proposal->id)}}"
                                                           class="btn btn-sm btn-outline-primary float-left"><i
                                                                    class="fa fa-eye"></i> Pengahrgaan</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="modal-footer">
                                                <small class="text-danger">Pastikan semua data sudah benar</small>
                                                {{ Form::button('<i class="fa fa-check"></i> Kirim revisi', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </td>
                        <td align="right">
                            @if(!$proposal->organization_id)
                                <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal"
                                        data-target="#team-{{$proposal->id}}">
                                    <i class="fa fa-eye"></i> Tim
                                </button>
                            @else
                                <a target="_blank" href="{!! url('storage/'.$proposal->committee_url) !!}"
                                   class="btn btn-sm btn-outline-primary"><i class="fa fa-eye"></i> Kepanitiaan</a>
                            @endif
                            <a target="_blank" href="{!! url('storage/'.$proposal->cover_letter_url) !!}"
                               class="btn btn-sm btn-outline-primary"><i class="fa fa-file-pdf-o"></i> Surat
                                Pengantar</a>
                            <a target="_blank" href="{!! url('storage/'.$proposal->proposal_url) !!}"
                               class="btn btn-sm btn-outline-primary"><i class="fa fa-file-pdf-o"></i> Proposal</a>
                            @if($proposal->status >= config('value.progress.mail-done'))
                                <br>
                                @foreach($proposal->mails as $mail)
                                    <a target="_blank" href="{{route('mail.show', [$mail->slug, 'print' => 'true'])}}"
                                       class="btn btn-sm btn-outline-success"><i
                                                class="fa fa-print"> {{ucfirst(strtolower($mail->type->title))}}</i></a>
                                @endforeach
                                <br>
                                @if($proposal->status >= config('value.progress.lpj-submitted'))
                                    <a target="_blank" href="{!! url('storage/'.$proposal->LPJ_url) !!}"
                                       class="btn btn-sm btn-outline-primary"><i class="fa fa-file-pdf-o"></i> LPJ</a>
                                    <a target="_blank" href="{!! url('storage/'.$proposal->receipt_url) !!}"
                                       class="btn btn-sm btn-outline-primary"><i class="fa fa-file-pdf-o"></i> Bukti
                                        transaksi</a>
                                    <a href="{{route('documentation.index', $proposal->id)}}"
                                       class="btn btn-sm btn-outline-primary"><i class="fa fa-eye"></i> Dokumentasi</a>
                                    @if(!$proposal->organization_id)
                                        <a href="{{route('achievement.index', $proposal->id)}}"
                                           class="btn btn-sm btn-outline-primary"><i class="fa fa-eye"></i> Penghargaan</a>
                                    @endif
                                @endif
                            @elseif($proposal->status === config('value.progress.revised-dekan'))
                                <button class="btn btn-sm" type="button" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    <i class="fa fa-sort-desc"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    @if(!$proposal->organization_id)
                                        <a class="dropdown-item"
                                           href="{{route('mahasiswa.proposal.edit', [$proposal->id, 'option' => 'attend'])}}">Ubah</a>
                                        <a class="dropdown-item"
                                           href="{{route('mahasiswa.proposal.edit.team', $proposal->id)}}">Ubah
                                            pengusul</a>
                                    @else
                                        <a class="dropdown-item"
                                           href="{{route('mahasiswa.proposal.edit',[$proposal->id, 'option' => 'manage'])}}">Ubah</a>
                                    @endif
                                    {!! Form::open(array('method'=>'delete', 'route' => array('mahasiswa.proposal.destroy',"$proposal->id"))) !!}
                                    {!! Form::submit('Batalkan', array('class'=>'dropdown-item')) !!}
                                    {!! Form::close() !!}
                                </div>
                            @endif
                        </td>
                    </tr>
                    <tr class="collapse" id="rows-{{$proposal->id}}">
                        <td colspan="100%">
                            <div class="card-body small">
                                <div class="bd-example">
                                    <dl class="row">
                                        <dt class="col-sm-2">Deskripsi</dt>
                                        <dd class="col-sm-10">{!! $proposal->desc !!}</dd>
                                        <dt class="col-sm-2">Jenis</dt>
                                        <dd class="col-sm-10">{{$proposal->event->name}}</dd>
                                        <dt class="col-sm-2">Level</dt>
                                        <dd class="col-sm-10">{{$proposal->level}}</dd>
                                        <dt class="col-sm-2">Lokasi</dt>
                                        <dd class="col-sm-10">{{$proposal->location}}</dd>
                                        @if(isset($proposal->organization->name))
                                            <dt class="col-sm-2">Kebutuhan anggaran</dt>
                                            <dd class="col-sm-10">{{$proposal->funds_needed_rupiah}}</dd>
                                            <dt class="col-sm-2">Organisasi penyelenggara</dt>
                                            <dd class="col-sm-10">{{$proposal->organization->name}}</dd>
                                        @else
                                            <dt class="col-sm-2">Kebutuhan anggaran</dt>
                                            <dd class="col-sm-10">
                                                <dl class="row">
                                                    @foreach($proposal->budgets as $budget)
                                                        <dd class="col-sm-3"><i>{!! config('value.budget.'.$budget->type) !!}</i></dd>
                                                        <dd class="col-sm-9">{!! $budget->amount_rupiah !!}</dd>
                                                    @endforeach
                                                    <dt class="col-sm-3"><i>Total</i></dt>
                                                    <dt class="col-sm-9">{!! $proposal->funds_needed_rupiah !!}</dt>
                                                </dl>
                                            </dd>
                                        @endif
                                        <dt class="col-sm-2">Diajukan sejak</dt>
                                        <dd class="col-sm-10">{{$proposal->created_at->diffForHumans()}}</dd>
                                    </dl>
                                </div>
                            </div>
                        </td>
                    </tr>

                    {{--modal team--}}
                    <div class="modal fade" id="team-{{$proposal->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="teamCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Data tim</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col">
                                        <div class="form-group row m-0">
                                            <label class="col-md-3 col-form-label"><strong>NIM</strong></label>
                                            <label class="col-md-6 col-form-label"><strong>Nama</strong></label>
                                        </div>
                                        @foreach($proposal->teams as $team)
                                            <div class="form-group row m-0">
                                                <label class="col-md-3 col-form-label">{!! $team->user_id !!}</label>
                                                <label class="col-md-6 col-form-label">{!! $team->user->name !!}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        $('.revision-form').on('submit', function () {
            return confirm('Kirim revisi proposal?')
        });
    </script>
@endsection
