<div class="card-header">
    <strong>Detail data pelaksana</strong>
</div>
<div class="card-body">
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                {!! Form::label('organization_id', 'Organisasi pelaksana') !!}
                {!! Form::select('organization_id', $organizations, null,['id' => 'organization_id', 'class' => 'form-control '.($errors->has('organization_id') ? 'is-invalid' : ''), 'placeholder' => 'Pilih organisasi', 'autocomplete' => 'organization_id']) !!}
                @error('organization')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                {!! Form::label('committee', 'Scan struktur kepanitiaan') !!} <br>
                {!! Form::file('committee', null, ['id' => 'committee', 'class' => 'form-control-file is-invalid', 'autocomplete' => 'committee']) !!}
                <br> <small class="text-muted">*hanya format PDF, max: 5 MB</small>
                @if(isset($proposal))
                    @if($proposal->committee_url)
                        <br> <small class="text-info"><a target="_blank" href="{!! url('storage/'.$proposal->committee_url) !!}">Lihat struktur kepanitiaan</a></small>
                    @endif
                @endif
                @error('committee')
                <br> <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
        </div>
    </div>
</div>