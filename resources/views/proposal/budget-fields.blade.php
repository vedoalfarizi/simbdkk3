<div class="row">
    <div class="col-6">
        <div class="form-group">
            {!! Form::label('travel', 'Biaya perjalanan', ['class' => 'col-form-label']) !!}
            <div class="controls">
                <div class="input-prepend input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Rp</span>
                    </div>
                    {!! Form::text('travel', isset($budgets) ? $budgets['travel'] : null, ['id' => 'travel', 'class' => 'form-control '.($errors->has('travel') ? 'is-invalid' : ''), 'placeholder' => 'Tuliskan jumlah dana', 'autocomplete' => 'travel']) !!}
                    @error('travel')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group">
            {!! Form::label('accommodation', 'Biaya akomodasi', ['class' => 'col-form-label']) !!}
            <div class="controls">
                <div class="input-prepend input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Rp</span>
                    </div>
                    {!! Form::text('accommodation', isset($budgets) ? $budgets['accommodation'] : null, ['id' => 'accommodation', 'class' => 'form-control '.($errors->has('accommodation') ? 'is-invalid' : ''), 'placeholder' => 'Tuliskan jumlah dana', 'autocomplete' => 'accommodation']) !!}
                    @error('accommodation')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group">
            {!! Form::label('daily', 'Biaya harian', ['class' => 'col-form-label']) !!}
            <div class="controls">
                <div class="input-prepend input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Rp</span>
                    </div>
                    {!! Form::text('daily', isset($budgets) ? $budgets['daily'] : null, ['id' => 'daily', 'class' => 'form-control '.($errors->has('daily') ? 'is-invalid' : ''), 'placeholder' => 'Tuliskan jumlah dana', 'autocomplete' => 'daily']) !!}
                    @error('daily')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group">
            {!! Form::label('registration', 'Biaya pendaftaran', ['class' => 'col-form-label']) !!}
            <div class="controls">
                <div class="input-prepend input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Rp</span>
                    </div>
                    {!! Form::text('registration', isset($budgets) ? $budgets['registration'] : null, ['id' => 'registration', 'class' => 'form-control '.($errors->has('registration') ? 'is-invalid' : ''), 'placeholder' => 'Tuliskan jumlah dana', 'autocomplete' => 'registration']) !!}
                    @error('registration')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group">
            {!! Form::label('equipment', 'Biaya perlengkapan', ['class' => 'col-form-label']) !!}
            <div class="controls">
                <div class="input-prepend input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Rp</span>
                    </div>
                    {!! Form::text('equipment', isset($budgets) ? $budgets['equipment'] : null, ['id' => 'equipment', 'class' => 'form-control '.($errors->has('equipment') ? 'is-invalid' : ''), 'placeholder' => 'Tuliskan jumlah dana', 'autocomplete' => 'equipment']) !!}
                    @error('equipment')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
    </div>
</div>