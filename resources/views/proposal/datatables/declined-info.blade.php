{{$row->id }}<br> {!! $row->result !!}
@if($row->status === config('value.progress.declined-dekan'))
<div class="modal fade" id="declined-{{$row->id}}">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alasan ditolak</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <p><small>{!! $row->revision !!}</small></p>
            </div>
        </div>
    </div>
</div>
@endif