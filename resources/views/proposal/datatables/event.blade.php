<b> ({{$row->level}})</b> {{$row->titleWithStatus}}
<br>
<small><i>{{$row->held_on->format('d/M/Y')}} {{$row->end_on != null ? 's.d. '.$row->end_on->format('d/M/Y') : null }} - {{$row->location}}</i></small>