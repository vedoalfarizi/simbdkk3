@if(!$row->organized_by)
    @foreach($row->mails as $mail)
        <a target="_blank" href="{{route('mail.show', [$mail->slug, 'print' => 'true'])}}" class="btn btn-sm btn-outline-success"><i class="fa fa-print"> {{ucfirst(strtolower($mail->type->title))}}</i></a>
    @endforeach

    <br>
    <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#team-{{$row->id}}">
        <i class="fa fa-eye"></i> Tim
    </button>
@else
    <a target="_blank" href="{!! url('storage/'.$row->committee_url) !!}" class="btn btn-sm btn-outline-primary"><i class="fa fa-eye"></i> Kepanitiaan</a>
@endif

<a target="_blank" href="{!! url('storage/'.$row->cover_letter_url) !!}" class="btn btn-sm btn-outline-primary"><i class="fa fa-file-pdf-o"></i> Surat Pengantar</a>
<a target="_blank" href="{!! url('storage/'.$row->proposal_url) !!}" class="btn btn-sm btn-outline-primary"><i class="fa fa-file-pdf-o"></i> Proposal</a>

@if($row->status !== config('value.progress.canceled') && $row->status !== config('value.progress.declined-dekan'))
    <br>
    <a target="_blank" href="{!! url('storage/'.$row->LPJ_url) !!}" class="btn btn-sm btn-outline-primary"><i class="fa fa-file-pdf-o"></i> LPJ</a>
    <a target="_blank" href="{!! url('storage/'.$row->receipt_url) !!}" class="btn btn-sm btn-outline-primary"><i class="fa fa-file-pdf-o"></i> Bukti transaksi</a>
    <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#documentation-{{$row->id}}"><i class="fa fa-eye"></i> Dokumentasi</button>

    @if(!$row->organized_by)
        <button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#achievement-{{$row->id}}"><i class="fa fa-eye"></i> Penghargaan</button>
    @endif
@endif

{{--modal team--}}
<div class="modal fade" id="team-{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="teamCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Data tim</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col">
                    <div class="form-group row m-0">
                        <label class="col-md-3 col-form-label"><strong>NIM</strong></label>
                        <label class="col-md-6 col-form-label"><strong>Nama</strong></label>
                    </div>
                    @foreach($row->team as $team)
                        <div class="form-group row m-0">
                            <label class="col-md-3 col-form-label">{!! $team->user_id !!}</label>
                            <label class="col-md-6 col-form-label">{!! $team->user->name !!}</label>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

{{--modal documentation--}}
<div id="documentation-{{$row->id}}" class="modal fade" tabindex="-1" role="dialog"aria-labelledby="dokumentasi" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="carousel-documentation" class="carousel switch-slider" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($row->documentations as $index => $documentation)
                        <div class="carousel-item {{$index == 0 ? 'active' : ''}} ">
                            <img class="img-fluid" src="{!! url('storage/'.$documentation->photo_url) !!}" alt="{{$documentation->desc}}">
                            <div class="carousel-caption d-none d-md-block">
                                <p style="background-color: black">{!! $documentation->desc !!}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carousel-documentation" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-documentation" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>

@if(!$row->organized_by)
    {{--modal achievement--}}
    <div id="achievement-{{$row->id}}" class="modal fade" tabindex="-1" role="dialog"aria-labelledby="penghargaan" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="carousel-achievement" class="carousel switch-slider" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($row->achievements as $index => $achievement)
                            <div class="carousel-item {{$index == 0 ? 'active' : ''}} ">
                                <img class="img-fluid" src="{!! url('storage/'.$achievement->certificate_url) !!}" alt="{{$achievement->reward}}">
                                <div class="carousel-caption d-none d-md-block">
                                    <p style="background-color: black">
                                        {!! $achievement->reward !!}
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carousel-achievement" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-achievement" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endif