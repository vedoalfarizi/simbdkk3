@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('sekdek.proposal.review') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>Data proposal</strong>
            <div class="card-header-actions">
                <a class="btn btn-primary" href="{{route('sekdek.proposal.on-review')}}">
                    Menunggu revisi
                </a>
            </div>
        </div>
        <div class="card-body table-responsive ">
            <table class="table table-hover table-bordered data-table" style="border-collapse: collapse !important">
                <thead class="thead-dark small text-center">
                <tr>
                    <th width="5px">No</th>
                    <th scope="col">Kode</th>
                    <th scope="col">Judul</th>
                    <th scope="col">Diajukan sejak</th>
                    <th width="10px"></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        $(function () {

            const table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('sekdek.proposal.review') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'id', name: 'id'},
                    {data: 'title', name: 'title'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', searchable: false, orderable: false},
                ]
            });

        });
    </script>
@endsection