@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('sekdek.proposal.review.detail', $proposal->id) }}
@endsection

@section('content')
    <div class="card">
        {!! Form::open(['route' => ['sekdek.proposal.review.store', $proposal->id], 'method' => 'put']) !!}
        <div class="card-header">
            <strong>Detail proposal</strong>
        </div>
        <div class="card-body">
            <div class="bd-example">
                <dl class="row">
                    <dt class="col-sm-3">Kegiatan</dt>
                    <dd class="col-sm-9">{!! $proposal->title !!}</dd>
                    <dt class="col-sm-3">Deskripsi</dt>
                    <dd class="col-sm-9">{!! $proposal->desc !!}</dd>
                    <dt class="col-sm-3">Jenis</dt>
                    <dd class="col-sm-9">{!! $proposal->event->name !!}</dd>
                    <dt class="col-sm-3">Tingkat</dt>
                    <dd class="col-sm-9">{!! $proposal->level !!}</dd>
                    <dt class="col-sm-3">Waktu</dt>
                    <dd class="col-sm-9">
                        <dl class="row">
                            <dt class="col-sm-2">Mulai</dt>
                            <dd class="col-sm-10">{!! $proposal->held_on->formatLocalized("%d %B %Y") !!}</dd>
                            <dt class="col-sm-2">Selesai</dt>
                            <dd class="col-sm-10">{!! $proposal->end_on == null ? $proposal->held_on->formatLocalized("%d %B %Y") : $proposal->end_on->formatLocalized("%d %B %Y") !!}</dd>
                        </dl>
                    </dd>
                    <dt class="col-sm-3">Lokasi</dt>
                    <dd class="col-sm-9">{!! $proposal->location !!}</dd>
                    <dt class="col-sm-3">Surat pengantar</dt>
                    <dd class="col-sm-9"><a target="_blank" href="{!! url('storage/'.$proposal->cover_letter_url) !!}">Lihat surat pengantar</a></dd>
                    <dt class="col-sm-3">Proposal</dt>
                    <dd class="col-sm-9"><a target="_blank" href="{!! url('storage/'.$proposal->proposal_url) !!}">Lihat proposal</a></dd>
                    @if(isset($proposal->organization_id))
                        <dt class="col-sm-3">Anggaran kebutuhan dana</dt>
                        <dd class="col-sm-9">{!! $proposal->funds_needed_rupiah !!}</dd>
                        <dt class="col-sm-3">Organisasi pelaksana</dt>
                        <dd class="col-sm-9">{!! $proposal->organization->name !!}</dd>
                        <dt class="col-sm-3">Struktur kepanitiaan</dt>
                        <dd class="col-sm-9"><a target="_blank" href="{!! url('storage/'.$proposal->committee_url) !!}">Lihat kepanitiaan</a></dd>
                    @else
                        <dt class="col-sm-3">Anggaran kebutuhan dana</dt>
                        <dd class="col-sm-9">
                            <dl class="row">
                                @foreach($proposal->budgets as $budget)
                                    <dd class="col-sm-3"><i>{!! config('value.budget.'.$budget->type) !!}</i></dd>
                                    <dd class="col-sm-9">{!! $budget->amount_rupiah !!}</dd>
                                @endforeach
                                <dt class="col-sm-3"><i>Total</i></dt>
                                <dt class="col-sm-9">{!! $proposal->funds_needed_rupiah !!}</dt>
                            </dl>
                        </dd>
                    @endif
                </dl>
            </div>
        </div>
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Verifikasi', ['class' => 'btn btn-sm btn-success', 'type' => 'submit', 'name' => 'reviewBtn', 'value' => 'verify']) }}
            {{ Form::button('<i class="fa fa-times"></i> Revisi', ['class' => 'btn btn-sm btn-danger', 'type' => 'button', 'data-toggle' => 'modal', 'data-target' => "#proposal-revision-$proposal->id"]) }}
            <a class="btn btn-sm btn-primary" href="{{route('sekdek.proposal.review')}}"><i class="fa fa-arrow-left"></i> Kembali</a>
        </div>

        <div class="modal fade" id="proposal-revision-{{$proposal->id}}">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Revisi proposal</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>

                    <div class="modal-body">
                        {!! Form::label('message', 'Catatan revisi') !!}
                        {!! Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Tuliskan catatan revisi', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
                    </div>

                    <div class="modal-footer">
                        {{ Form::button('Kirim', ['class' => 'btn btn-success', 'type' => 'submit', 'name' => 'reviewBtn', 'value' => 'revision']) }}
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        CKEDITOR.replace('message');
    </script>
@endsection
