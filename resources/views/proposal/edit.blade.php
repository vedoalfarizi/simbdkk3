@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mahasiswa.proposal.edit', $proposal) }}
@endsection

@section('content')
    <div class="card">
        {!! Form::model($proposal, ['route' => ['mahasiswa.proposal.update', $proposal->id], 'method' => 'put', 'enctype' => 'multipart/form-data']) !!}
        <div class="card-header">
            <strong>Form ubah data proposal</strong>
        </div>
        <div class="card-body">
            @include('proposal.fields')
        </div>
        @if($type === 'manage')
            @include('proposal.organize-detail')
        @elseif($type ==='attend')
            <div class="card-header">
                <strong>Rincian anggaran kebutuhan dana</strong>
                <small class="text-muted">(Tulis 0 jika tidak ada)</small>
            </div>
            <div class="card-body">
                @include('proposal.budget-fields')
            </div>
        @endif
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            <a class="btn btn-sm btn-danger" href="{{route('mahasiswa.proposal.index')}}"><i class="fa fa-ban"></i> Batal</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        CKEDITOR.replace('desc');
    </script>
@endsection