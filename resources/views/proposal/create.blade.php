@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mahasiswa.proposal.create') }}
@endsection

@section('content')
    <div class="card">
        {!! Form::open(['route' => 'mahasiswa.proposal.store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        <div class="card-header">
            @if($type == 'attend')
                <strong>Form pengajuan proposal mengikuti kegiatan</strong>
            @else
                <strong>Form pengajuan proposal melaksanakan kegiatan</strong>
            @endif
        </div>
        <div class="collapse show" id="join" style="">
            <div class="card-body">
                @include('proposal.fields')
            </div>
            @if($type == 'attend')
                <div class="card-header">
                    <strong>Rincian anggaran kebutuhan dana</strong>
                    <small class="text-muted">(Tulis 0 jika tidak ada)</small>
                </div>
                <div class="card-body">
                    @include('proposal.budget-fields')
                </div>
                <div class="card-header">
                    <strong>Data anggota tim</strong>
                    <small class="text-muted">(Kosongkan jika bukan sebagai tim)</small>
                </div>
                <div class="card-body">
                    @include('team.fields')
                </div>
            @else
                @include('proposal.organize-detail')
            @endif
            <div class="card-footer">
                {{ Form::button('<i class="fa fa-check"></i> Kirim', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
                <a class="btn btn-sm btn-danger" href="{{route('mahasiswa.home')}}"><i class="fa fa-ban"></i> Batal</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection


@section('js-script')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script type="text/javascript">
        CKEDITOR.replace('desc');

        $(document).ready(function(){
            $('#nim').select2({
                minimumInputLength: 3,
                placeholder: 'Tuliskan NIM atau nama',
                ajax: {
                    url: "{!! url('/users-json') !!}",
                    dataType: 'json',
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.id + ' - ' + item.name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    cache: true
                }
            });
        });
    </script>
@endsection