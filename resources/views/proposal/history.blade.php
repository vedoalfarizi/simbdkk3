@extends('layouts.dashboard')

@section('breadcrumb')
    @role('Mahasiswa|Sekretaris Dekan|Kabag|Wadek II|Dekan')
        {{ Breadcrumbs::render('proposal.history.index') }}
    @endrole
    @role('Kasubag')
        {{ Breadcrumbs::render('kasubag.lpj.history') }}
    @endrole
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>Riwayat proposal</strong>
            @role('Mahasiswa')
            <div class="card-header-actions">
                <a class="btn btn-primary" href="{{route('mahasiswa.proposal.index')}}">
                    Kembali
                </a>
            </div>
            @endrole
            @role('Kasubag')
            <div class="card-header-actions">
                <a class="btn btn-outline-primary" href="{{route('kasubag.lpj.index')}}">
                    Kembali
                </a>
            </div>
            @endrole
        </div>
        <div class="card-body table-responsive ">
            <table class="table table-hover table-bordered data-table" style="border-collapse: collapse !important" width="100%">
                <thead class="thead-dark small text-center">
                <tr>
                    <th width="5px">No</th>
                    <th scope="col">Kode</th>
                    <th scope="col">Kegiatan</th>
                    <th scope="col">Besar bantuan</th>
                    <th scope="col">Berkas</th>
                </tr>
                </thead>

                @unlessrole('Mahasiswa')
                <div class="panel-body">
                    <label for="filter-year"> Filter Berdasarkan Tahun : </label>
                    <select data-column="2" class="form-control col-sm-4 filter-year">
                        <option value=""> Pilih Tahun </option>
                        @foreach($years as $year)
                            <option value="{{ $year }}"> {{ $year }} </option>
                        @endforeach
                    </select>

                    <label for="filter-level"> Filter Berdasarkan Tingkat : </label>
                    <select data-column="2" class="form-control col-sm-4 filter-level">
                        <option value=""> Pilih Tingkat </option>
                        @foreach($levels as $level)
                            <option value="{{ $level }}"> {{ $level }} </option>
                        @endforeach
                    </select>

                    <form method="GET" id="filter-form" class="form-inline" role="form">
                        <label for="start">Filter berdasarkan besar bantuan</label>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" name="startRp" id="startRp" placeholder="Mulai dari">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="endRp" id="endRp" placeholder="sampai dengan">
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-primary">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
                <br>
                @endrole
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')
    @unlessrole('Mahasiswa')

    <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"> </script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"> </script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"> </script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"> </script>
    @endrole
    <script type="text/javascript">
        $(function () {

            const title = 'Data Proposal Cetakan '+ "{{ now()->year }}";
            let table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                dom: '<"html5buttons">Blfrtip',
                buttons : [
                    {
                        extend:'csv',
                        title: title,
                        exportOptions: {
                            columns: [ 0, 2, 3 ]
                        }
                    },
                    {
                        extend: 'pdf',
                        title: title,
                        exportOptions: {
                            columns: [ 0, 2, 3 ]
                        }
                    },
                    {
                        extend: 'excel',
                        title: title,
                        exportOptions: {
                            columns: [ 0, 2, 3 ]
                        }
                    },
                    {
                        extend: 'print',
                        title: title,
                        exportOptions: {
                            columns: [ 0, 2, 3 ]
                        }
                    },
                ],
                ajax: {
                    url:  "{{ route('proposal.history.index') }}",
                    data: function(d){
                        d.startRp = $('input[name=startRp]').val();
                        d.endRp = $('input[name=endRp]').val();
                    }
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'id', name: 'id'},
                    {data: 'event', name: 'event'},
                    {data: 'fundingRupiah', name: 'fundingRupiah'},
                    {data: 'files', name: 'files', searchable: false, orderable: false},
                    {data: 'funding', name: 'funding', visible:false},
                ]
            });

            $('#filter-form').on('submit', function(e) {
                table.draw();
                e.preventDefault();
            });

            $('.filter-year').change(function () {
                table.column( $(this).data('column'))
                    .search( $(this).val() )
                    .draw();
            });

            $('.filter-level').change(function () {
                table.column( $(this).data('column'))
                    .search( $(this).val() )
                    .draw();
            });
        });
    </script>
@endsection
