@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mahasiswa.proposal.option') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <a class="btn btn-outline-light" href="{{route('mahasiswa.proposal.create', ['option' => 'attend'])}}">
                <img src="{{asset('img/join.png')}}" width="20%" alt="">
                <br> <span style="color: black">Mengikuti Kegiatan</span>
            </a>
            <a class="btn btn-outline-light" href="{{route('mahasiswa.proposal.create', ['option' => 'manage'])}}">
                <img src="{{asset('img/held.png')}}" width="20%" alt="">
                <br> <span style="color: black">Melaksanakan Kegiatan (Kepanitiaan)</span>
            </a>
        </div>
        <div class="card-footer">
            <i>*Maksimal waktu pengajuan proposal adalah 5 hari kerja sebelum pelaksanaan kegiatan <br>
                terhitung setelah proposal diajukan </i>
        </div>
    </div>
@endsection