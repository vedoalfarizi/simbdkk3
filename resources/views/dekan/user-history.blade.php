@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('dekan.user.history.index', $proposal_id) }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>Riwayat pengusul</strong>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover table-sm table-bordered data-table" style="border-collapse: collapse !important">
                <thead class="thead-dark text-center small">
                <tr>
                    <th width="col">No</th>
                    <th width="col">Kegiatan</th>
                    <th scope="col">Pendanaan</th>
                    <th width="col">Status proposal</th>
                    <th width="col">Prestasi</th>
                </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @forelse($proposals as $proposal)
                        <tr>
                            <td>{!! $no++ !!}</td>
                            <td>
                                <b> {{$proposal->level}} -</b> {{$proposal->titleWithStatus}}
                            </td>
                            <td>
                                {{$proposal->funds_receive_rupiah}}
                            </td>
                            <td>
                                {{ $proposal->status <= config('value.progress.lpj-verified') ? 'Dalam proses' : 'Selesai' }}
                            </td>
                            <td>
                                @if(!$proposal->achievements->isEmpty())
                                    <button class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#rows-{{$proposal->id}}" aria-expanded="false" aria-controls="rows-{{$proposal->id}}">
                                        Lihat
                                    </button>
                                @else
                                    Belum ada prestasi
                                @endif

                            </td>
                        </tr>
                        <tr class="collapse" id="rows-{{$proposal->id}}">
                            <td colspan="100%">
                                <div class="card-body" style="padding-bottom: 0px">
                                    <div class="bd-example">
                                        <dl class="row">
                                            @php $noAch = 1 @endphp
                                            @foreach($proposal->achievements as $achievement)
                                                <dd class="col-sm-8">{{$achievement->reward}} -
                                                    @if($achievement->user_id)
                                                        {{ $achievement->user->name }} ( {{ $achievement->user_id }} )
                                                    @else
                                                        Grup
                                                    @endif
                                                </dd>
                                            @endforeach
                                        </dl>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5"><p><small>Belum ada riwayat</small></p></td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection