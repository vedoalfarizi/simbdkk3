@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('dekan.home') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-3">
            <div class="card text-white bg-primary">
                <div class="card-header text-center">SURAT</div>
                <div class="card-body row text-center">
                    <div class="col">
                        <div class="text-value-xl">{{ $mail->waiting }}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('mail.index')}}">
                            <div class="text-uppercase text-muted small">Permintaan</div>
                        </a>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $mail->revision }}</div>
                        <div class="text-uppercase text-muted small">Revisi</div>

                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $mail->done }}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('mail.history.index')}}">
                            <div class="text-uppercase text-muted small">Selesai</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-4">
            <div class="card text-white bg-primary">
                <div class="card-header text-center">DISPOSISI PROPOSAL</div>
                <div class="card-body row text-center">
                    <div class="col">
                        <div class="text-value-xl">{{ $proposal->request}}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('disposition.index')}}">
                            <div class="text-uppercase text-muted small">Permintaan</div>
                        </a>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $proposal->revised}}</div>
                        <div class="text-uppercase text-muted small">Revisi</div>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $proposal->disposed }}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('disposition.history.index')}}">
                            <div class="text-uppercase text-muted small">Disposisi</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="card">
                <div class="card-body p-0 d-flex align-items-center">
                    <i class="fa fa-envelope-o bg-primary p-4 font-2xl mr-3"></i>
                    <div>
                        <div class="text-value-sm text-primary">Rp.{!! number_format($budget, 0, ',', '.') !!}</div>
                        <div class="text-muted text-uppercase font-weight-bold small">
                            @if($budget <= 0)
                                Pagu belum diatur
                            @else
                                Sisa anggaran tersedia
                            @endif</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <canvas id="fundStatistic" width="300" height="100"></canvas>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <canvas id="fundProposalType" width="300" height="100"></canvas>
        </div>
    </div>

    <div class="card">
        <div class="card-body table-responsive">
            <table class="table table-hover table-sm table-bordered data-table" style="border-collapse: collapse !important">
                <caption style="caption-side: top; text-align: center"><b>Jumlah proposal berdasarkan jenis kegiatan per tahun</b></caption>
                <thead class="thead-dark text-center small">
                <tr>
                    <th width="col">Tahun</th>
                    <th scope="col">Kompetisi</th>
                    <th scope="col">Konferensi</th>
                    <th scope="col">Seminar</th>
                    <th scope="col">Pertukaran mahasiswa</th>
                    <th width="col">Pengabdian masyarakat (tanpa dosen)</th>
                </tr>
                </thead>
                <tbody>
                @forelse($proposalEvents as $event)
                    <tr align="center">
                        <td>{{$event->year}}</td>
                        <td>{{$event->competition}}</td>
                        <td>{{$event->conference}}</td>
                        <td>{{$event->seminar}}</td>
                        <td>{{$event->exchange}}</td>
                        <td>{{$event->dedication}}</td>
                    </tr>
                @empty
                    <tr align="center"><td colspan="6">Belum ada data</td></tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>

    <div class="card">
        <div class="card-body table-responsive">
            <table class="table table-hover table-sm table-bordered data-table" style="border-collapse: collapse !important">
                <caption style="caption-side: top; text-align: center"><b>Jumlah prestasi berdasarkan tingkat kegiatan per tahun</b></caption>
                <thead class="thead-dark text-center small">
                <tr>
                    <th width="col">Tahun</th>
                    <th scope="col">Lokal</th>
                    <th scope="col">Kota</th>
                    <th scope="col">Provinsi</th>
                    <th scope="col">Wilayah</th>
                    <th width="col">Nasional</th>
                    <th width="col">Internasional</th>
                </tr>
                </thead>
                <tbody>
                @forelse($achievementLevels as $year => $level)
                    <tr align="center">
                        <td>{{$year}}</td>
                        <td>{{$level["Lokal"] ?? 0}}</td>
                        <td>{{$level["Kota"] ?? 0}}</td>
                        <td>{{$level["Provinsi"] ?? 0}}</td>
                        <td>{{$level["Wilayah"] ?? 0}}</td>
                        <td>{{$level["Nasional"] ?? 0}}</td>
                        <td>{{$level["Internasional"] ?? 0}}</td>
                    </tr>
                @empty
                    <tr align="center"><td colspan="7">Belum ada data</td></tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <canvas id="achievementDepartment" width="300" height="100"></canvas>
        </div>
    </div>

    <div class="card">
        <div class="card-body table-responsive">
            <table class="table table-hover table-sm table-bordered data-table" style="border-collapse: collapse !important">
                <caption style="caption-side: top; text-align: center"><b>Mahasiswa berprestasi tahun {{ now()->year }}</b></caption>
                <thead class="thead-dark text-center small">
                <tr>
                    <th width="col">No</th>
                    <th scope="col">NIM</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Poin</th>
                </tr>
                </thead>
                <tbody>
                @php $no = 1; @endphp
                @forelse($bestStudents as $student)
                    <tr>
                        <td>{{$no++}}</td>
                        <td>{{$student['nim']}}</td>
                        <td>{{$student['name']}}</td>
                        <td>{{$student['point']}}</td>
                    </tr>
                @empty
                    <tr align="center"><td colspan="4">Belum ada data</td></tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')
    <script>
        function toRupiah(nStr) {
            nStr += '';
            const x = nStr.split('.');
            let x1 = x[0];
            const x2 = x.length > 1 ? '.' + x[1] : '';
            const rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            const total = x1 + x2;
            return `Rp${total}`;
        }
    </script>

    @include('chart.fund-statistic')
    @include('chart.fund-proposal-type')
    @include('chart.achievement-department')

@endsection
