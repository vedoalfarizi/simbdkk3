@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('achievement.index', $proposalId) }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-header-actions">
                <a class="btn btn-sm btn-primary float-right mr-1 d-print-none" href="{{route('mahasiswa.achievement.create', $proposalId)}}">Tambah</a>
            </div>
        </div>
        <div class="card-body">
            <div class="row col-12">
                @foreach($achievements as $achievement)
                <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">
                    {!! Form::open(array('method'=>'delete', 'route' => array('mahasiswa.achievement.destroy', $proposalId, $achievement->id))) !!}
                    <a class="fancybox" rel="lightbox" href="{!! url('storage/'.$achievement->certificate_url) !!}">
                        <img src="{!! url('storage/'.$achievement->certificate_url) !!}" alt="achievement image" class="img-thumbnail">
                    </a>
                    {{--<a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>--}}
                    <a class="btn btn-sm btn-primary" href="{!! route('file.download', [$achievement->id, 'type' => 'achievement']) !!}">
                        <i class="fa fa-download"></i>
                    </a>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                    <br>
                    <p align="center">
                        <b>{!! $achievement->reward !!}</b> - {{$achievement->user_id == null ? 'Grup' : $achievement->user->name}}
                    </p>
                    {!! Form::close() !!}
                </div>
                @endforeach
            </div>
        </div>
        <div class="card-footer">
            <a class="btn btn-sm btn-info" href="{{route('mahasiswa.proposal.index')}}"><i class="fa fa-rotate-left"></i> Kembali</a>
        </div>
    </div>
@endsection

@section('js-script')
    <script>
        $(document).ready(function(){
            $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none"
            });
        });
    </script>
@endsection