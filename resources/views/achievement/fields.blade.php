<div class="row">
    <div class="col-sm-12">
        @error('achievement')
            <small class="text-danger">{{$message}}</small>
        @enderror
        @error('nim.*')
            <br><small class="text-danger">{{$message}}</small>
        @enderror
        @error('reward.*')
            <br><small class="text-danger">{{$message}}</small>
        @enderror
        <div class="form-group achievement">
            <div class="input-group">
                {!! Form::label('achievement', 'Scan sertifikat') !!} <br>
                {!! Form::file('achievement[]', null, ['id' => 'achievement', 'class' => 'form-control-file is-invalid', 'autocomplete' => 'achievement']) !!}
                {!! Form::text('nim[]', null, ['id' => 'nim', 'class' => 'form-control ', 'placeholder' => 'NIM Penerima (Kosongkan jika tim)', 'autocomplete' => 'nim']) !!}
                {!! Form::select('reward[]', Config::get('value.achievement'), null, ['id' => 'reward', 'class' => 'form-control', 'placeholder' => 'Pilih perolehan pernghargaan..']) !!}
                <div class="input-group-addon">
                    <a href="javascript:void(0)" class="btn btn-success addMore"><i class="fa fa-plus"></i></a>
                </div>
            </div>
        </div>

        <div class="form-group achievementCopy" style="display: none;">
            <div class="input-group">
                {!! Form::label('achievement', 'Scan sertifikat ') !!} <br>
                {!! Form::file('achievement[]', null, ['id' => 'achievement', 'class' => 'form-control-file is-invalid', 'autocomplete' => 'achievement']) !!}
                {!! Form::text('nim[]', null, ['id' => 'nim', 'class' => 'form-control ', 'placeholder' => 'NIM Penerima (Kosongkan jika tim)', 'autocomplete' => 'nim']) !!}
                {!! Form::select('reward[]', Config::get('value.achievement'), null, ['id' => 'reward', 'class' => 'form-control', 'placeholder' => 'Pilih perolehan pernghargaan..']) !!}
                <div class="input-group-addon">
                    <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fa fa-minus"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>