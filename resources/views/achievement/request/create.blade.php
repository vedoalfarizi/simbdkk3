@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mahasiswa.achievement-request') }}
@endsection

@section('content')
    <div class="card">
        {!! Form::open(['route' => ['mahasiswa.achievement-upload'], 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        <div class="collapse show" id="join" style="">
            <div class="card-header">
                <strong><small class="text-info">*Pastikan setiap file berformat PDF atau PNG/JPG dan berukuran maks 2 MB</small></strong>
            </div>
            <div class="card-body">
                @include('achievement.request.fields')
            </div>
            <div class="card-footer">
                {{ Form::button('Ajukan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection