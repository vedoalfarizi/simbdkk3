<div class="row">
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::label('title', 'Judul kegiatan') !!}
            {!! Form::text('title', null, ['id' => 'title', 'class' => 'form-control '.($errors->has('title') ? 'is-invalid' : ''), 'placeholder' => 'Tuliskan judul kegiatan...', 'autocomplete' => 'title']) !!}
            @error('title')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('level', 'Tingkat') !!}
            {!! Form::select('level', Config::get('value.level'), null, ['id' => 'level', 'class' => 'form-control '.($errors->has('level') ? 'is-invalid' : ''), 'placeholder' => 'Pilih tingkat...']) !!}
            @error('level')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('achievement', 'Scan sertifikat') !!} <br>
            {!! Form::file('achievement', null, ['id' => 'achievement', 'class' => 'form-control-file '.($errors->has('achievement') ? 'is-invalid' : ''), 'autocomplete' => 'achievement']) !!}
            @error('achievement')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('reward', 'Prestasi') !!} <br>
            {!! Form::select('reward', Config::get('value.achievement'), null, ['id' => 'reward', 'class' => 'form-control '.($errors->has('reward') ? 'is-invalid' : ''), 'placeholder' => 'Pilih perolehan penghargaan...']) !!}
            @error('reward')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>
</div>