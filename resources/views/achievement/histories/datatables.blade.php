<script type="text/javascript">
$(function () {

    const title = 'Data Prestasi Cetakan '+ "{{ now()->year }}";
    let table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        dom: '<"html5buttons">Blfrtip',
        buttons : [
            {extend:'csv', title: title,},
            {
                extend: 'pdf',
                title: title,
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {extend: 'excel', title: title},
            {
                extend: 'print',
                title: title,
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
        ],
        ajax: "{{ route('user-achievements') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'year', name: 'year'},
            {data: 'level', name: 'level'},
            {data: 'title', name: 'title'},
            {data: 'users', name: 'users'},
            {data: 'reward', name: 'reward'},
            {data: 'certificate', name: 'certificate', searchable: false, orderable: false},
        ]
    });

    $('.filter-year').change(function () {
        table.column( $(this).data('column'))
            .search( $(this).val() )
            .draw();
    });

    $('.filter-level').change(function () {
        table.column( $(this).data('column'))
            .search( $(this).val() )
            .draw();
    });
});
</script>