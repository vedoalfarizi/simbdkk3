@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('user-achievements') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>Data Prestasi Mahasiswa</strong>
        </div>
        <div class="card-body table-responsive ">
            <table class="table table-hover table-bordered data-table" style="border-collapse: collapse !important">
                <thead class="thead-dark small text-center">
                <tr>
                    <th width="5px">No</th>
                    <th scope="col">Tahun</th>
                    <th scope="col">Tingkat</th>
                    <th scope="col">Kegiatan</th>
                    <th scope="col">Mahasiswa</th>
                    <th scope="col">Prestasi</th>
                    <th scope="col">Sertifikat</th>
                </tr>
                </thead>

                <div class="panel-body">
                    <label for="filter-year"> Filter Berdasarkan Tahun : </label>
                    <select data-column="1" class="form-control col-sm-4 filter-year">
                        <option value=""> Pilih Tahun </option>
                        @foreach($years as $year)
                            <option value="{{ $year }}"> {{ $year }} </option>
                        @endforeach
                    </select>

                    <label for="filter-level"> Filter Berdasarkan Tingkat : </label>
                    <select data-column="2" class="form-control col-sm-4 filter-level">
                        <option value=""> Pilih Tingkat </option>
                        @foreach($levels as $level)
                            <option value="{{ $level }}"> {{ $level }} </option>
                        @endforeach
                    </select>

                    <br /> <br />
                </div>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"> </script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"> </script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"> </script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"> </script>

    @include('achievement.histories.datatables')
@endsection
