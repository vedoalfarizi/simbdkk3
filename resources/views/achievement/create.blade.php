@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mahasiswa.achievement.create', $proposalId) }}
@endsection

@section('content')
    <div class="card">
        {!! Form::open(['route' => ['mahasiswa.achievement.store', $proposalId], 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        <div class="card-header">
            <strong>Tambah sertifikat penghargaan kegiatan</strong>
        </div>
        <div class="collapse show" id="join" style="">
            <div class="card-header">
                <strong><small class="text-info">*Pastikan setiap file berformat PDF atau PNG/JPG dan berukuran maks 2 MB</small></strong>
            </div>
            <div class="card-body">
                @include('achievement.fields')
            </div>
            <div class="card-footer">
                {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
                <a class="btn btn-sm btn-danger" href="{{route('achievement.index', $proposalId)}}"><i class="fa fa-ban"></i> Batal</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        $(document).ready(function() {
            // achievements
            $(".addMore").click(function(){
                let fieldHTML = '<div class="form-group achievement">'+$(".achievementCopy").html()+'</div>';
                $('body').find('.achievement:last').after(fieldHTML);
            });
            $("body").on("click",".remove",function(){
                $(this).parents(".achievement").remove();
            });
        });
    </script>
@endsection