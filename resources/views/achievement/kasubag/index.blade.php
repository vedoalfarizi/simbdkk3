@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('achievement.kasubag.index') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row col-12">
                @foreach($achievements as $achievement)
                <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3 img-button">
                    <a class="fancybox" rel="lightbox" href="{!! url('storage/'.$achievement->certificate_url) !!}">
                        <img src="{!! url('storage/'.$achievement->certificate_url) !!}" alt="documentation image" class="img-thumbnail">
                    </a>
                    <p align="center">
                        <b>{!! $achievement->reward !!}</b> - {{$achievement->user_id == null ? 'Grup' : $achievement->user->name}}
                    </p>
                </div>
                @endforeach
            </div>
        </div>
        <div class="card-footer">
            <a class="btn btn-sm btn-info" href="{{route('kasubag.lpj.index')}}"><i class="fa fa-rotate-left"></i> Kembali</a>
        </div>
    </div>
@endsection

@section('js-script')
    <script>
        $(document).ready(function(){
            $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none"
            });
        });
    </script>
@endsection