@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mahasiswa.home') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-3">
            <div class="card text-white bg-primary">
                <div class="card-header text-center">PROPOSAL</div>
                <div class="card-body row text-center">
                    <div class="col">
                        <div class="text-value-xl">{!! $reportProposal['total'] !!}</div>
                        <div class="text-uppercase text-muted small">Semua</div>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{!! $reportProposal['process'] !!}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('mahasiswa.proposal.index')}}">
                            <div class="text-uppercase text-muted small">Proses</div>
                        </a>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{!! $reportProposal['done'] !!}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('proposal.history.index')}}">
                            <div class="text-uppercase text-muted small">Selesai</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card text-white bg-primary">
                <div class="card-header text-center">KEGIATAN</div>
                <div class="card-body row text-center">
                    <div class="col">
                        <div class="text-value-xl">{!! $reportProposal['competition'] !!}</div>
                        <div class="text-uppercase text-muted small">Kompetisi</div>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{!! $reportProposal['conference'] !!}</div>
                        <div class="text-uppercase text-muted small">Konferensi</div>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{!! $reportProposal['seminar'] !!}</div>
                        <div class="text-uppercase text-muted small">Seminar</div>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{!! $reportProposal['exchange'] !!}</div>
                        <div class="text-uppercase text-muted small">Exchanges</div>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{!! $reportProposal['dedication'] !!}</div>
                        <div class="text-uppercase text-muted small">Pengabdian</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card text-white bg-primary">
                <div class="card-header text-center">PENDANAAN DITERIMA</div>
                <div class="card-body row text-center">
                    <div class="col">
                        <div class="text-value-lg">Rp {!! number_format($reportProposal['funding'], 0, ',', '.') !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Catatan prestasi</div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-hover table-outline ">
                        <thead class="thead-light small">
                        <tr>
                            <th>Tahun
                            </th>
                            <th>Tingkat</th>
                            <th>Jenis</th>
                            <th>Kegiatan</th>
                            <th>Pencapaian</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($achievements as $achievement)
                                <tr>
                                    <td>{!! $achievement->created_at->format('Y') !!}</td>
                                    <td>{!! $achievement->proposal->level !!}</td>
                                    <td>{!! $achievement->proposal->event->name !!}</td>
                                    <td>{!! $achievement->proposal->title !!}</td>
                                    <td>{!! $achievement->reward !!}</td>
                                    <td>
                                        <a target="_blank" href="{!! url('storage/'.$achievement->certificate_url) !!}"><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-sm btn-primary" href="{!! route('file.download', [$achievement->id, 'type' => 'achievement']) !!}">
                                            <i class="fa fa-download"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6"><p><small>Belum ada catatan prestasi</small></p></td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection