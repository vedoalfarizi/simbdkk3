@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('sekdek.home') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body p-0 d-flex align-items-center">
                    <i class="fa fa-envelope-o bg-primary p-4 font-2xl mr-3"></i>
                    <div>
                        <div class="text-value-sm text-primary">{{$lastMailNo->id ?? '-'}}</div>
                        <div class="text-muted text-uppercase font-weight-bold small">NO SURAT TERAKHIR</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-body p-0 d-flex align-items-center">
                    <i class="fa fa-envelope-o bg-primary p-4 font-2xl mr-3"></i>
                    <div>
                        <div class="text-value-sm text-primary">Rp.{!! number_format($nowBudget, 0, ',', '.') !!}</div>
                        <div class="text-muted text-uppercase font-weight-bold small">
                            @if($nowBudget <= 0)
                                Pagu tahun {{$nowYear}} belum diatur
                                <a style="margin-left: 10em" href='{{route('sekdek.budget.create')}}' class='btn btn-sm btn-primary'>Atur Pagu</a>
                            @else
                                Pagu tahun {{$nowYear}}
                                <a style="margin-left: 20em" href='{{route('sekdek.budget.edit', $nowYear)}}' class='btn btn-sm btn-warning'>Ubah Pagu</a>
                            @endif</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card text-white bg-primary">
                <div class="card-header text-center">SURAT</div>
                <div class="card-body row text-center">
                    <div class="col">
                        <div class="text-value-xl">{{ $mail->request }}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('sekdek.mail.request')}}">
                            <div class="text-uppercase text-muted small">Permintaan</div>
                        </a>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col-3">
                        <div class="text-value-xl">{{ $mail->waiting }}</div>
                        <div class="text-uppercase text-muted small">Menunggu TTD</div>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $mail->revision }}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('mail.index', ['status' => 'revision'])}}">
                            <div class="text-uppercase text-muted small">Revisi</div>
                        </a>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $mail->done }}</div>
                        <div class="text-uppercase text-muted small">Selesai</div>
                    </div>
                </div>
            </div>
        </div>

{{--        <div class="col-md-4">--}}
{{--            <div class="card text-white bg-primary">--}}
{{--                <div class="card-header text-center">PROPOSAL</div>--}}
{{--                <div class="card-body row text-center">--}}
{{--                    <div class="col">--}}
{{--                        <div class="text-value-xl">{{ $proposal->total }}</div>--}}
{{--                        <div class="text-uppercase text-muted small">Semua</div>--}}
{{--                    </div>--}}
{{--                    <div class="c-vr"></div>--}}
{{--                    <div class="col">--}}
{{--                        <div class="text-value-xl">{{ $proposal->request }}</div>--}}
{{--                        <a style="text-decoration: none" class="btn-link" href="{{route('sekdek.proposal.review')}}">--}}
{{--                            <div class="text-uppercase text-muted small">Permintaan</div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                    <div class="c-vr"></div>--}}
{{--                    <div class="col">--}}
{{--                        <div class="text-value-xl">{{ $proposal->revision}}</div>--}}
{{--                        <a style="text-decoration: none" class="btn-link" href="{{route('sekdek.proposal.on-review')}}">--}}
{{--                            <div class="text-uppercase text-muted small">Revisi</div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                    <div class="c-vr"></div>--}}
{{--                    <div class="col">--}}
{{--                        <div class="text-value-xl">{{ $proposal->done }}</div>--}}
{{--                        <div class="text-uppercase text-muted small">Direview</div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="col-md-3">
            <div class="card text-white bg-primary">
                <div class="card-header text-center">ARSIP</div>
                <div class="card-body row text-center">
                    <div class="col">
                        <div class="text-value-xl">{{ $archive["proposal"] }}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('proposal.history.index')}}">
                            <div class="text-uppercase text-muted small">Proposal</div>
                        </a>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $archive["mail"] }}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('mail.history.index')}}">
                            <div class="text-uppercase text-muted small">Surat</div>
                        </a>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $archive["achievement"]}}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('user-achievements')}}">
                            <div class="text-uppercase text-muted small">Prestasi</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
