@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('admin.home') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-6 col-lg-3">
            <div class="card">
                <div class="card-body p-0 d-flex align-items-center">
                    <i class="fa fa-users bg-primary p-4 px-5 font-2xl mr-3"></i>
                    <div>
                        <div class="text-value-sm text-primary">{{ $user }}</div>
                        <div class="text-muted text-uppercase font-weight-bold small">Pengguna</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3">
            <div class="card">
                <div class="card-body p-0 d-flex align-items-center">
                    <i class="fa fa-sitemap bg-primary p-4 px-5 font-2xl mr-3"></i>
                    <div>
                        <div class="text-value-sm text-primary">{{ $organization }}</div>
                        <div class="text-muted text-uppercase font-weight-bold small">Organisasi</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3">
            <div class="card">
                <div class="card-body p-0 d-flex align-items-center">
                    <i class="fa fa-building bg-primary p-4 px-5 font-2xl mr-3"></i>
                    <div>
                        <div class="text-value-sm text-primary">{{ $department }}</div>
                        <div class="text-muted text-uppercase font-weight-bold small">Jurusan</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3">
            <div class="card">
                <div class="card-body p-0 d-flex align-items-center">
                    <i class="fa fa-calendar bg-primary p-4 px-5 font-2xl mr-3"></i>
                    <div>
                        <div class="text-value-sm text-primary">{{ $event }}</div>
                        <div class="text-muted text-uppercase font-weight-bold small">Kegiatan</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection