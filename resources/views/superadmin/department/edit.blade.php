@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('admin.department.edit', $department) }}
@endsection

@section('content')
    <div class="card">
        {!! Form::model($department, ['route' => ['admin.department.update', $department->id], 'method' => 'put']) !!}
        <div class="card-header">
            <strong>Form ubah data jurusan</strong>
        </div>
        <div class="card-body">
            @include('superadmin.department.fields')
        </div>
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            <a class="btn btn-sm btn-danger" href="{{route('admin.department.index')}}"><i class="fa fa-rotate-left"></i> Kembali</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection