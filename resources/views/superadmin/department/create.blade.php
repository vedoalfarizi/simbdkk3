@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('admin.department.create') }}
@endsection

@section('content')
    <div class="card">
        {!! Form::open(['route' => 'admin.department.store', 'method' => 'post']) !!}
        <div class="card-header">
            <strong>Form tambah jurusan</strong>
        </div>
        <div class="card-body">
            @include('superadmin.department.fields')
        </div>
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            <a class="btn btn-sm btn-danger" href="{{route('admin.department.index')}}"><i class="fa fa-rotate-left"></i> Kembali</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection