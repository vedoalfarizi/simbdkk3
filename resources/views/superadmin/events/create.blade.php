@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('admin.events.create') }}
@endsection

@section('content')
    <div class="card">
        {!! Form::open(['route' => 'admin.events.store', 'method' => 'post']) !!}
        <div class="card-header">
            <strong>Form tambah kegiatan</strong>
        </div>
        <div class="card-body">
            @include('superadmin.events.fields')
        </div>
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            <a class="btn btn-sm btn-danger" href="{{route('admin.events.index')}}"><i class="fa fa-rotate-left"></i> Kembali</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection