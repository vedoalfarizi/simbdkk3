@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('admin.events.edit', $event) }}
@endsection

@section('content')
    <div class="card">
        {!! Form::model($event, ['route' => ['admin.events.update', $event->id], 'method' => 'put']) !!}
        <div class="card-header">
            <strong>Form ubah data kegiatan</strong>
        </div>
        <div class="card-body">
            @include('superadmin.events.fields')
        </div>
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            <a class="btn btn-sm btn-danger" href="{{route('admin.events.index')}}"><i class="fa fa-rotate-left"></i> Kembali</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection