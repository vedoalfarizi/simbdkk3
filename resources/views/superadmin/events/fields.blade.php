<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('name', 'Nama kegiatan') !!}
            {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control '.($errors->has('name') ? 'is-invalid' : ''), 'placeholder' => 'Tuliskan nama kegiatan', 'autocomplete' => 'name']) !!}
            @error('name')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>
</div>