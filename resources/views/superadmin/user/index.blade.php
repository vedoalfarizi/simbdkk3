@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('admin.user') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>Data Pengguna</strong>
            <a class="btn btn-sm btn-primary float-right mr-1 d-print-none" href="{{route('admin.user.create')}}">tambah</a>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover table-bordered data-table" style="border-collapse: collapse !important">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">NIM/ NIP</th>
                    <th scope="col">Jurusan</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <th scope="col">status</th>
                    <th scope="col">Terdaftar pada</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        $(function () {

            const table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.user.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'id', name: 'id'},
                    {data: 'department_id', name: 'department_id'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'status', name: 'status'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', searchable: false, orderable: false},
                ]
            });

        });
    </script>
@endsection