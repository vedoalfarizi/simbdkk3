 <button class="btn btn-block dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fa angle-double-down"></i>
</button>
<div class="dropdown-menu dropdown-menu-right">

    <a class="dropdown-item" href="{{route('admin.user.edit', $row->id)}}">Ubah</a>

    {!! Form::open(array('method'=>'PUT', 'route' => array('admin.user.update.status',"$row->id"))) !!}
        {!! Form::hidden('status', $row->deleted_at) !!}
        @if($row->deleted_at)
            {!! Form::submit('Aktifkan', array('class'=>'dropdown-item')) !!}
        @else
            {!! Form::submit('Nonaktifkan', array('class'=>'dropdown-item')) !!}
        @endif
    {!! Form::close() !!}

    @if($row->deleted_at)
        {!! Form::open(array('method'=>'DELETE', 'route' => array('admin.user.destroy',"$row->id"))) !!}
            {!! Form::submit('Hapus', array('class'=>'dropdown-item')) !!}
        {!! Form::close() !!}
    @endif

</div>
