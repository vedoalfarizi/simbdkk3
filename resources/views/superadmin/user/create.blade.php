@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('admin.user.create') }}
@endsection

@section('content')
    <div class="card">
        {!! Form::open(['route' => 'admin.user.store', 'method' => 'post']) !!}
        <div class="card-header">
            <strong>Form tambah pengguna</strong>
        </div>
        <div class="card-body">
            @include('superadmin.user.fields')
        </div>
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            <a class="btn btn-sm btn-danger" href="{{route('admin.user.index')}}"><i class="fa fa-rotate-left"></i> Kembali</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection