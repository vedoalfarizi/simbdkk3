<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('id', 'NIM atau NIP') !!}
            {!! Form::number('id', null, ['id' => 'id', 'class' => 'form-control '.($errors->has('id') ? 'is-invalid' : ''), 'placeholder' => 'contoh: 1511xxx', 'autocomplete' => 'id']) !!}
            @error('id')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Nama lengkap') !!}
            {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control '.($errors->has('name') ? 'is-invalid' : ''), 'placeholder' => 'Nama Lengkap', 'autocomplete' => 'name']) !!}
            @error('name')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('email', 'Email') !!}
            {!! Form::email('email', null, ['id' => 'email', 'class' => 'form-control '.($errors->has('email') ? 'is-invalid' : ''), 'placeholder' => 'contoh: mahasiswa@unand.ac.id', 'autocomplete' => 'email']) !!}
            @error('email')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('department', 'Jurusan') !!}
            {!! Form::select('department', $departments, $departmentId, ['id' => 'department', 'class' => 'form-control '.($errors->has('department') ? 'is-invalid' : ''), 'placeholder' => 'pilih jurusan']) !!}
            @error('department')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('role', 'Role') !!}
            {!! Form::select('role', $roles, $roleId, ['id' => 'role', 'class' => 'form-control '.($errors->has('role') ? 'is-invalid' : ''), 'placeholder' => 'pilih role']) !!}
            @error('role')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>
</div>