@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('admin.user.create') }}
@endsection

@section('content')
    <div class="card">
        {!! Form::model($user, ['route' => ['admin.user.update', $user->id], 'method' => 'put']) !!}
        <div class="card-header">
            <strong>Form ubah data pengguna</strong>
        </div>
        <div class="card-body">
            @include('superadmin.user.fields-edit')
        </div>
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            <a class="btn btn-sm btn-danger" href="{{route('admin.user.index')}}"><i class="fa fa-rotate-left"></i> Kembali</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection