@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('admin.organization') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>Data Organisasi</strong>
            <a class="btn btn-sm btn-primary float-right mr-1 d-print-none" href="{{route('admin.organization.create')}}">Tambah</a>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover table-bordered data-table" style="border-collapse: collapse !important">
                <thead class="thead-dark">
                <tr>
                    <th width="10px">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Ditambahkan sejak</th>
                    <th width="10px"></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        $(function () {

            const table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.organization.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', searchable: false, orderable: false},
                ]
            });

        });
    </script>
@endsection