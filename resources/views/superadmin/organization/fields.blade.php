<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('name', 'Nama organisasi') !!}
            {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control '.($errors->has('name') ? 'is-invalid' : ''), 'placeholder' => 'Tulis nama organisasi', 'autocomplete' => 'name']) !!}
            @error('name')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>
</div>