<button class="btn btn-block dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fa angle-double-down"></i>
</button>
<div class="dropdown-menu dropdown-menu-right">
    <a class="dropdown-item" href="{{route('admin.organization.edit',$row->id)}}">Ubah</a>

    {!! Form::open(array('method'=>'DELETE', 'route' => array('admin.organization.destroy',"$row->id"))) !!}
        {!! Form::submit('Hapus', array('class'=>'dropdown-item')) !!}
    {!! Form::close() !!}
</div>
