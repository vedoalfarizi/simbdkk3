@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('admin.organization.edit', $organization) }}
@endsection

@section('content')
    <div class="card">
        {!! Form::model($organization, ['route' => ['admin.organization.update', $organization->id], 'method' => 'put']) !!}
        <div class="card-header">
            <strong>Form ubah data organisasi</strong>
        </div>
        <div class="card-body">
            @include('superadmin.organization.fields')
        </div>
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            <a class="btn btn-sm btn-danger" href="{{route('admin.organization.index')}}"><i class="fa fa-rotate-left"></i> Batal</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection