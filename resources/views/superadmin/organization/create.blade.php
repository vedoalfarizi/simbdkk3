@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('admin.organization.create') }}
@endsection

@section('content')
    <div class="card">
        {!! Form::open(['route' => 'admin.organization.store', 'method' => 'post']) !!}
        <div class="card-header">
            <strong>Form tambah data organisasi</strong>
        </div>
        <div class="card-body">
            @include('superadmin.organization.fields')
        </div>
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            <a class="btn btn-sm btn-danger" href="{{route('admin.organization.index')}}"><i class="fa fa-rotate-left"></i> Kembali</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection