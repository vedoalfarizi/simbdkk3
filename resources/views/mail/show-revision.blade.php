@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mail.show.revision', $mail) }}
@endsection

@section('content')
    @if($mail->is_manual_sign == false)
    <div class="card">
        <div class="card-header">
            <strong>Isi revisi</strong>
        </div>
        <div class="card-body">
            {!! $mail->revision !!}
        </div>
    </div>
    @endif

    <div class="card">
        {!! Form::model($mail, ['route' => ['mail.update', $mail->slug], 'method' => 'put']) !!}
        <div class="card-header">
            <strong>{{ucfirst(strtolower($mail->type->title))}}</strong>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::label('content', 'Isi') !!}
                        {!! Form::textarea('content', $mail->content, ['id' => 'content', 'class' => 'form-control'.($errors->has('content') ? 'is-invalid' : ''), 'autocomplete' => 'content']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-success', 'type' => 'submit', 'name' => 'submit', 'value' => 'submit']) }}
            {{ Form::button('<i class="fa fa-external-link"></i> Pratinjau', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit', 'name' => 'submit', 'value' => 'preview', 'formtarget' => '_blank']) }}
            @if($mail->is_manual_sign == false)
                <a class="btn btn-sm btn-danger" href="{{route('mail.index', ['status' => 'revision'])}}"><i class="fa fa-ban"></i> Batal</a>
            @else
                <a class="btn btn-sm btn-danger" href="{{route('mail.history.index')}}"><i class="fa fa-ban"></i> Batal</a>
            @endif
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        CKEDITOR.replace('content');
    </script>
@endsection