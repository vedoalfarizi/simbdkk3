@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('sekdek.mail.request') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>Permintaan surat</strong>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover table-bordered data-table" style="border-collapse: collapse !important">
                <thead class="thead-dark small text-center">
                <tr>
                    <th scope="col">Kode</th>
                    <th scope="col">Kegiatan</th>
                    <th scope="col">Pelaksanaan</th>
                    <th width="10px"></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        $(function () {

            const table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('sekdek.mail.request') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'event', name: 'event'},
                    {data: 'deadline', name: 'deadline'},
                    {data: 'action', name: 'action', searchable: false, orderable: false},
                ]
            });
        });
    </script>
@endsection