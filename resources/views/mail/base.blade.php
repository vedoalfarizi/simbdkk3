<html>
<head>
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: #FAFAFA;
            font: 12pt "Times New Roman";
        }
        h6, h4 {
            margin: 0;
        }
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }
        .page {
            width: 21cm;
            min-height: 29.7cm;
            padding: 2cm;
            margin: 1cm auto;
            border: 1px #D3D3D3 solid;
            border-radius: 5px;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        }

        @page {
            size: A4;
            margin: 0;
        }
        @media print {
            .page {
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
        }
    </style>
</head>
<body>

    <div class="book">
        <div class="page">
            <div class="row">
                <div class="col-md-2">
                    <img src="{{asset('img/logo-unand.png')}}" alt="logo unand" width="80px">
                </div>
                <div class="col-md-10 text-center">
                    <h6>KEMENTRIAN PENDIDIKAN DAN KEBUDAYAAN</h6>
                    <h6>UNIVERSITAS ANDALAS</h6>
                    <h4>FAKULTAS TEKNOLOGI INFORMASI</h4>
                    <small>KAMPUS UNAND LIMAU MANIS, PADANG-25163</small>
                    <br>
                    <small>Telp. 0751-9824667, website: http://fti.unand.ac.id email: sekretariat@fti.unand.ac.id</small>
                </div>
            </div>
            <hr style="border-top: 3px double;">

            <div class="row">
                <div class="col-md-12 text-center">
                    <b><u>@yield('title')</u></b>
                </div>
                <div class="col-md-12 text-center">
                    Nomor: @yield('mail_number')
                </div>
            </div>

            <br><br><br>

            @yield('content')

        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    @yield('script')

</body>
</html>