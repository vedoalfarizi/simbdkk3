@extends('mail.base')

@section('title')
    {{$mail_type->title ?? $mail->type->title}}
@endsection

@section('mail_number')
    {{$mail_no ?? $mail->id}}
@endsection

@section('content')
    <div class="row">
        Yang bertanda tangan di bawah ini,
        <br><br>

        <dl class="row">
            <dd class="col-sm-2">Nama</dd>
            <dd class="col-sm-10">: {{$signer->name}}</dd>

            <dd class="col-sm-2">NIP</dd>
            <dd class="col-sm-10">: {{$signer->id}}</dd>

            <dd class="col-sm-2">Jabatan</dd>
            <dd class="col-sm-10">: Dekan Fakultas Teknologi Informasi Universitas Andalas</dd>
        </dl>

        dengan ini menerangkan bahwa,
        <br><br>

        <dl class="row">
            <dd class="col-sm-2">Nama</dd>
            <dd class="col-sm-10">: {{$delegation->proposer->name}}</dd>

            <dd class="col-sm-2">NIM</dd>
            <dd class="col-sm-10">: {{$delegation->proposer->id}}</dd>

            <dd class="col-sm-2">Jurusan</dd>
            <dd class="col-sm-10">: {{$delegation->proposer->department->name}}</dd>
        </dl>

        {!! $content ?? $mail->content !!}

        <br>

        Demikian {{strtolower($mail_type->title ?? $mail->type->title)}} ini dibuat untuk dapat dipergunakan sebagaimana mestinya.
    </div>

    <br><br><br>

    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            @if(isset($date))
                Padang, {{$date->formatLocalized("%d %B %Y")}}
            @else
                Padang, {{$mail->created_at->formatLocalized("%d %B %Y")}}
            @endif
            <br>
            Dekan
                <br>
                @if(isset($mail) && $mail->sign !== null)
                    @if($verified)
                        {!! QrCode::encoding('UTF-8')->errorCorrection('M')->size(150)->generate($mail->slug) !!}
                    @else
                        <small><br><br><b>Tanda tangan tidak valid</b><br><br></small>
                    @endif
                @else
                    <br><br><br><br><br>
                @endif
                <br>
            <u>{{$signer->name}}</u> <br>
            NIP. {{$signer->id}}
        </div>
    </div>

    @role('Dekan')
    <br><br>
    <hr>
    <div class="row">
        <div class="col-2">
            {!! Form::open(['route' => ['dekan.mail.sign', $mail->slug], 'method' => 'post']) !!}
            {{ Form::button('Tandatangan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            {!! Form::close() !!}
        </div>
        <div class="col-2">
            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#revision-{{$mail->slug}}">
                Revisi
            </button>
        </div>
    </div>

    {{--modal revisi--}}
    <div class="modal fade" id="revision-{{$mail->slug}}">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            {!! Form::open(['route' => ['mail.revision', $mail->slug], 'method' => 'put']) !!}
            <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Catatan revisi surat</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    {!! Form::textarea('revision', null, ['class' => 'form-control', 'placeholder' => 'Isi revisi..', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    {{ Form::button('Kirim', ['class' => 'btn btn-success', 'type' => 'submit']) }}
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @endrole

@endsection