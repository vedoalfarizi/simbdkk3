@extends('mail.print.kop')

@section('content')
    <br>

    <table style="font-size: 14px" align="center">
        <tr>
            <td align="center" style="font-weight: bold;"><u>{{$mail_type->title ?? $mail->type->title}}</u></td>
        </tr>
        <tr>
            <td align="center">Nomor: {{$mail_no ?? $mail->id}}</td>
        </tr>
    </table>

    <br><br>

    <table style="font-size: 14px">
        <tr>
            <td colspan="2">Yang bertanda tangan di bawah ini,</td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td width="30%">Nama</td>
            <td>: {{$signer->name}}</td>
        </tr>
        <tr>
            <td width="30%">NIP</td>
            <td>: {{$signer->id}}</td>
        </tr>
        <tr>
            <td width="30%">Jabatan</td>
            <td>: Dekan Fakultas Teknologi Informasi Universitas Andalas</td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2">Dengan ini menerangkan bahwa,</td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td width="30%">Nama</td>
            <td>: {{$delegation->proposer->name}}</td>
        </tr>
        <tr>
            <td width="30%">NIM</td>
            <td>: {{$delegation->proposer->id}}</td>
        </tr>
        <tr>
            <td width="30%">Jurusan</td>
            <td>: {{$delegation->proposer->department->name}}</td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2">{!! $content ?? $mail->content !!}</td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2">Demikian {{strtolower($mail_type->title ?? $mail->type->title)}} ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</td>
        </tr>
    </table>

    <br><br>

    <table style="font-size: 14px" align="right">
        <tr>
            <td align="left">
                @if(isset($date))
                    Padang, {{$date->formatLocalized("%d %B %Y")}}
                @else
                    Padang, {{$mail->created_at->formatLocalized("%d %B %Y")}}
                @endif
            </td>
        </tr>
        <tr>
            <td align="left">
                Dekan
            </td>
        </tr>
        <tr>
            <td align="left">
                @if(isset($mail) && $mail->sign !== null)
                    @if($verified)
                        {!! QrCode::encoding('UTF-8')->errorCorrection('M')->size(150)->generate($mail->slug) !!}
                    @else
                        <small><br><br><b>Tanda tangan tidak valid</b><br><br></small>
                    @endif
                @else
                    <br><br><br><br><br>
                @endif
            </td>
        </tr>
        <tr>
            <td align="left">
                <u>{{$signer->name}}</u>
            </td>
        </tr>
        <tr>
            <td align="left">
                NIP. {{$signer->id}}
            </td>
        </tr>
    </table>
@endsection