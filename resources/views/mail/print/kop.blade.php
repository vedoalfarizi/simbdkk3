<html>
<head>
    <title>{{$mail_no ?? $mail->id}}</title>
</head>

<body style="margin: 3% 3% 3% 7%;">

<table align="center" style="line-height: 0px;">
    <tr>
        <td align="right" width="10%" rowspan="5" style="margin-right: -20px;"><img src="{{asset('img/logo-unand.png')}}" width="80px" alt=""></td>
        <td align="center" style="font-size: 18px;">KEMENTRIAN PENDIDIKAN DAN KEBUDAYAAN</td>
    </tr>
    <tr>
        <td align="center" style="font-size: 18px;">UNIVERSITAS ANDALAS</td>
    </tr>
    <tr>
        <td align="center" style="font-size: 20px; font-weight: bold;">FAKULTAS TEKNOLOGI INFORMASI</td>
    </tr>
    <tr>
        <td align="center" style="font-size: 14px;">KAMPUS UNAND LIMAU MANIS, PADANG-25163</td>
    </tr>
    <tr>
        <td align="center" style="font-size: 14px;">Telp. 0751-9824667, website: http://fti.unand.ac.id email: sekretariat@fti.unand.ac.id</td>
    </tr>
</table>

<hr border-top="3 px double" color="black">

@yield('content')

<script>
    window.print();
</script>
