@extends('mail.print.kop')

@section('content')
    <br>

    <table style="font-size: 14px" align="center">
        <tr>
            <td align="center" style="font-weight: bold;"><u>{{$mail_type->title ?? $mail->type->title}}</u></td>
        </tr>
        <tr>
            <td align="center">Nomor: {{$mail_no ?? $mail->id}}</td>
        </tr>
    </table>

    <br><br>

    <table style="font-size: 14px">
        <tr>
            @if(isset($mail->type))
                <td>Dekan Fakultas Teknologi Informasi Universitas Andalas {{$mail->type->id == 'KM.05.02' ? 'menugaskan' : 'mengizinkan'}} mahasiswa yang tersebut dibawah ini:</td>
            @else
                <td>Dekan Fakultas Teknologi Informasi Universitas Andalas {{$mail_type == 'KM.05.02' ? 'menugaskan' : 'mengizinkan'}} mahasiswa yang tersebut dibawah ini:</td>
            @endif
        </tr>
    </table>

    <br>

    <table border="1" align="center" width="80%" cellpadding="3" style="border-collapse: collapse">
        <thead>
        <th>No</th>
        <th>Nama</th>
        <th>NIM</th>
        <th>Jurusan</th>
        </thead>
        <tbody>
        @php $no = 1; @endphp
        @foreach($delegation as $delegate)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$delegate->user->name}}</td>
                <td>{{$delegate->user->id}}</td>
                <td>{{$delegate->user->department->name}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <br>

    <table style="font-size: 14px">
        <tr>
            <td>{!! $content ?? $mail->content !!}</td>
        </tr>
        <tr><td></td></tr>
        <tr>
            <td>
                {{ucfirst(strtolower($mail_type->title ?? $mail->type->title))}} ini dibuat untuk dilaksanakan dengan penuh tanggung jawab.
            </td>
        </tr>
    </table>

    <br><br>

    <table style="font-size: 14px" align="right">
        <tr>
            <td align="left">
                @if(isset($date))
                    Padang, {{$date->formatLocalized("%d %B %Y")}}
                @else
                    Padang, {{$mail->created_at->formatLocalized("%d %B %Y")}}
                @endif
            </td>
        </tr>
        <tr>
            <td align="left">
                Dekan
            </td>
        </tr>
        <tr>
            <td align="left">
                @if(isset($mail) && $mail->sign !== null)
                    @if($verified)
                        {!! QrCode::encoding('UTF-8')->errorCorrection('M')->size(150)->generate($mail->slug) !!}
                    @else
                        <small><br><br><b>Tanda tangan tidak valid</b><br><br></small>
                    @endif
                @else
                    <br><br><br><br><br>
                @endif
            </td>
        </tr>
        <tr>
            <td align="left">
                <u>{{$signer->name}}</u>
            </td>
        </tr>
        <tr>
            <td align="left">
                NIP. {{$signer->id}}
            </td>
        </tr>
    </table>
@endsection