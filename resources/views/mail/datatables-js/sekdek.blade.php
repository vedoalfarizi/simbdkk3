<script>
    $(function () {

        const table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ Request::get('status') == 'revision' ? route('mail.index', ['status' => 'revision']) : route('mail.index') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'proposal_id', name: 'proposal_id'},
                {data: 'type', name: 'type'},
                {data: 'status', name: 'status'},
                {data: 'deadline', name: 'deadline'},
                {data: 'action', name: 'action', searchable: false, orderable: false},
            ]
        });

    });
</script>