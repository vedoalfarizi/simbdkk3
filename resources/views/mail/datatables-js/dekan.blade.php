<script>
    $(function () {

        let table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('mail.index') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'jenis', name: 'jenis'},
                {data: 'deadline', name: 'deadline'},
                {data: 'action', name: 'action', searchable: false, orderable: false},
            ]
        });

    });
</script>