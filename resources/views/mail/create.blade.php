@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('sekdek.mail.create', $proposal) }}
@endsection

@section('content')
    <div class="card">
        {!! Form::open(['route' => 'mail.store', 'method' => 'post']) !!}
        <div class="card-header">
            <strong>Buat {{strtolower($type->title)}}</strong>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('mail_no', 'No') !!}
                        <div class="controls">
                            <div class="input-prepend input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">{{$type->type}}-</span>
                                </div>
                                {!! Form::text('mail_no', "$lastMailNumber", ['id' => 'mail_no', 'class' => 'form-control col-2'.($errors->has('mail_no') ? 'is-invalid' : ''), 'autocomplete' => 'mail_no']) !!}
                                <div class="input-group-append">
                                    <span class="input-group-text">/UN16.15/{{$type->signed_code}}/{{$type->id}}/{{$now->year}}</span>
                                </div>
                            </div>
                            <small>*penyesuaian nomor tidak boleh lebih kecil dari no yang sekarang</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::label('content', 'Isi') !!}
                        {!! Form::textarea('content', $content, ['id' => 'content', 'class' => 'form-control'.($errors->has('content') ? 'is-invalid' : ''), 'autocomplete' => 'content']) !!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::hidden('type_id', $type->id) !!}
        {!! Form::hidden('type_signed_code', $type->signed_code) !!}
        {!! Form::hidden('type_type', $type->type) !!}
        {!! Form::hidden('now', $now->year) !!}
        {!! Form::hidden('proposal_id', $proposal->id) !!}
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan untuk tandatangan digital', ['class' => 'btn btn-sm btn-success', 'type' => 'submit', 'name' => 'submit', 'value' => 'submit']) }}
            {{ Form::button('<i class="fa fa-check"></i> Simpan untuk tandatangan basah', ['class' => 'btn btn-sm btn-success', 'type' => 'submit', 'name' => 'submit', 'value' => 'submitManual']) }}
            {{ Form::button('<i class="fa fa-external-link"></i> Pratinjau', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit', 'name' => 'submit', 'value' => 'preview', 'formtarget' => '_blank']) }}
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        CKEDITOR.replace('content');
    </script>
@endsection