<div class="btn-group-vertical" role="group" aria-label="Vertical button group">
    <a target="_blank" href="{!! url('storage/'.$row->proposal_url) !!}" class="btn btn-info"><small>Proposal</small></a>
    @if($row->izin === 0)
    <a href="{{route('sekdek.mail.create', [$row->id, 'type' => 'izin'])}}" class="btn btn-behance"><small>Surat Izin</small></a>
    @endif
    @if($row->tugas === 0)
    <a href="{{route('sekdek.mail.create', [$row->id, 'type' => 'tugas'])}}" class="btn btn-behance"><small>Surat Tugas</small></a>
    @endif
    @if($row->keterangan === 0)
        <a href="{{route('sekdek.mail.create', [$row->id, 'type' => 'keterangan'])}}" class="btn btn-behance"><small>Surat Keterangan</small></a>
    @endif
</div>