<div class="btn-group-vertical" role="group" aria-label="Vertical button group">
    <a target="_blank" href="{{route('mail.show', [$row->slug, 'print' => 'true'])}}" class="btn btn-info"><small>Surat</small></a>
    <a target="_blank" href="{!! url('storage/'.$row->proposal->proposal_url) !!}" class="btn btn-info"><small>Proposal</small></a>
    @if(Request::get('status'))
        <a href="{{route('mail.show.revision', $row->slug)}}" class="btn btn-behance"><small>Revisi</small></a>
    @endif
</div>