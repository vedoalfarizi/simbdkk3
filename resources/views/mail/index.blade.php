@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mail.index') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>Data {{Request::get('status') !== null ? 'revisi': null}} surat</strong>
            @role('Dekan')
                <div class="card-header-actions">
                    <a class="btn btn-primary" href="{{route('mail.history.index')}}">
                        Riwayat
                    </a>
                </div>
            @endrole
            @role('Kasubag')
                <div class="card-header-actions">
                @if(!Request::get('status'))
                    <a class="btn btn-primary" href="{{route('mail.index', ['status' => 'revision'])}}">
                        Menunggu revisi
                    </a>
                @else
                    <a class="btn btn-primary" href="{{route('mail.index')}}">
                        Kembali
                    </a>
                @endif
                </div>
            @endrole
        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover table-bordered data-table" style="border-collapse: collapse !important">
                <thead class="thead-dark small text-center">
                <tr>
                    @role('Kasubag')
                        @include('mail.thead.kasubag')
                    @endrole
                    @role('Dekan')
                        @include('mail.thead.dekan')
                    @endrole
                    @role('Sekretaris Dekan')
                    @include('mail.thead.sekdek')
                    @endrole
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')
    @role('Kasubag')
    @include('mail.datatables-js.kasubag')
    @endrole
    @role('Dekan')
    @include('mail.datatables-js.dekan')
    @endrole
    @role('Sekretaris Dekan')
    @include('mail.datatables-js.sekdek')
    @endrole
@endsection