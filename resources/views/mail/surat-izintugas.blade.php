@extends('mail.base')

@section('title')
    {{$mail_type->title ?? $mail->type->title}}
@endsection

@section('mail_number')
    {{$mail_no ?? $mail->id}}
@endsection

@section('content')
    <div class="row">
        @if(isset($mail->type))
            Dekan Fakultas Teknologi Informasi Universitas Andalas {{$mail->type->id == 'KM.05.02' ? 'menugaskan' : 'mengizinkan'}} mahasiswa yang tersebut dibawah ini:
        @else
            Dekan Fakultas Teknologi Informasi Universitas Andalas {{$mail_type == 'KM.05.02' ? 'menugaskan' : 'mengizinkan'}} mahasiswa yang tersebut dibawah ini:
        @endif
        <br><br>

        <table border="1" align="center" width="80%" cellpadding="3" style="margin-bottom: 15px">
            <thead>
                <th>No</th>
                <th>Nama</th>
                <th>NIM</th>
                <th>Jurusan</th>
            </thead>
            <tbody>
            @php $no = 1; @endphp
            @foreach($delegation as $delegate)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$delegate->user->name}}</td>
                    <td>{{$delegate->user->id}}</td>
                    <td>{{$delegate->user->department->name}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>



        {!! $content ?? $mail->content !!}

        {{ucfirst(strtolower($mail_type->title ?? $mail->type->title))}} ini dibuat untuk dilaksanakan dengan penuh tanggung jawab.
    </div>

    <br><br><br>

    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            @if(isset($date))
                Padang, {{$date->formatLocalized("%d %B %Y")}}
            @else
                Padang, {{$mail->created_at->formatLocalized("%d %B %Y")}}
            @endif
            <br>
            Dekan
            <br>
                @if(isset($mail) && $mail->sign !== null)
                    @if($verified)
                        {!! QrCode::encoding('UTF-8')->errorCorrection('M')->size(150)->generate($mail->slug) !!}
                    @else
                        <small><br><br><b>Tanda tangan tidak valid</b><br><br></small>
                    @endif
                @else
                    <br><br><br><br><br>
                @endif
            <br>
            <u>{{$signer->name}}</u> <br>
            NIP. {{$signer->id}}
        </div>
    </div>

    @role('Dekan')
    <br><br>
    <hr>
    <div class="row">
        <div class="col-2">
            {!! Form::open(['route' => ['dekan.mail.sign', $mail->slug], 'method' => 'post']) !!}
                {{ Form::button('Tandatangan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            {!! Form::close() !!}
        </div>
        <div class="col-2">
            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#revision-{{$mail->slug}}">
                Revisi
            </button>
        </div>
    </div>

    {{--modal revisi--}}
    <div class="modal fade" id="revision-{{$mail->slug}}">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            {!! Form::open(['route' => ['mail.revision', $mail->slug], 'method' => 'put']) !!}
            <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Catatan revisi surat</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    {!! Form::textarea('revision', null, ['class' => 'form-control', 'placeholder' => 'Isi revisi..', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    {{ Form::button('Kirim', ['class' => 'btn btn-success', 'type' => 'submit']) }}
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @endrole

@endsection