<div class="btn-group-vertical" role="group" aria-label="Vertical button group">
    <a target="_blank" href="{{route('mail.show', [$row->slug, 'print' => 'true'])}}" class="{{$row->is_manual_sign == true ? 'btn btn-primary' : 'btn btn-info' }}">
        <small>{{$row->is_manual_sign == true ? 'Print' : 'Print(E-sign)' }}</small>
        @role('Sekretaris Dekan')
            @if($row->is_manual_sign == true && $row->proposal->status < config('value.progress.lpj-submitted'))
                <a href="{{route('mail.show.revision', $row->slug)}}" class="btn btn-info"><small>Edit</small></a>
            @endif
        @endrole
    </a>
</div>
