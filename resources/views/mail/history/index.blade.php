@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mail.history.index') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>Riwayat surat</strong>
            @role('Dekan')
            <div class="card-header-actions">
                <a class="btn btn-primary" href="{{route('mail.index')}}">
                    Kembali
                </a>
            </div>
            @endrole
        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover table-bordered data-table" style="border-collapse: collapse !important">
                <thead class="thead-dark small text-center">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">No surat</th>
                    <th scope="col">Jenis</th>
                    <th width="10px"></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')
    <script>
        $(function () {

            const table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('mail.history.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'id', name: 'id'},
                    {data: 'jenis', name: 'jenis'},
                    {data: 'action', name: 'action', searchable: false, orderable: false},
                ]
            });

        });
    </script>
@endsection