@extends('layouts.dashboard')

@section('breadcrumb')
    {{--{{ Breadcrumbs::render('admin.events.create') }}--}}
@endsection

@section('content')
    <div class="card">
        {!! Form::open(['route' => 'sekdek.budget.store', 'method' => 'post']) !!}
        <div class="card-header">
            <strong>Form Pagu tahun {{$year}}</strong>
        </div>
        <div class="card-body">
            @include('budget.fields')
        </div>
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            <a class="btn btn-sm btn-danger" href="{{route('sekdek.home')}}"><i class="fa fa-rotate-left"></i> Kembali</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection