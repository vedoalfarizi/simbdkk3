<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('amount', 'Jumlah Pagu') !!}
            {!! Form::text('amount', null, ['id' => 'amount', 'class' => 'form-control '.($errors->has('amount') ? 'is-invalid' : ''), 'placeholder' => 'Tuliskan jumlah pagu tahun ini', 'autocomplete' => 'amount']) !!}
            @error('amount')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>
</div>