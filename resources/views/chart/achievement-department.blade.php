<script>
    let url3 = "{{route('chart.achievement-department')}}";
    let years3 = [];
    let si = [];
    let tk = [];

    $(document).ready(function(){
        $.get(url3, function (response) {
            response.forEach(function (data) {
                years3.push(data.year);
                si.push(data.SistemInformasi);
                tk.push(data.TeknikKomputer);
            });

            let ctx = document.getElementById('achievementDepartment').getContext('2d');
            let achievementDepartment = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: years3,
                    datasets: [
                        {
                            label: 'Sistem Informasi',
                            data: si,
                            backgroundColor: 'rgba(255, 99, 132, 0.2)',
                            borderColor: 'rgba(255, 99, 132, 1)',
                            borderWidth: 1
                        },
                        {
                            label: 'Teknik Komputer',
                            data: tk,
                            backgroundColor: 'rgba(54, 162, 235, 0.2)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1
                        },
                    ]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Jumlah mahasiswa berprestasi berdasarkan jurusan per tahun'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                precision: 0
                            }
                        }]
                    },
                }
            });
        })
    })
</script>