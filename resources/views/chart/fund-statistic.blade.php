<script>
    let url = "{{route('chart.funding-statistic')}}";
    let years = [];
    let needs = [];
    let receiveds = [];
    let useds = [];

    const formatter = new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
    });

    $(document).ready(function(){
        $.get(url, function (response) {
            const budget = response.budget;
            response.funds.forEach(function (data) {
                years.push(data.year);
                needs.push(data.need);
                receiveds.push(data.received);
                useds.push(data.used);
            });
            const maxNeed = Math.max(...needs);
            let upBorder = budget + Math.ceil(0.10 * budget);
            if(budget <= 0 || budget < maxNeed){
                upBorder = maxNeed + Math.ceil(0.10 * maxNeed);
            }

            let ctx = document.getElementById('fundStatistic').getContext('2d');
            let fundStatistic = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: years,
                    datasets: [
                        {
                            label: 'Permintaan',
                            data: needs,
                            backgroundColor: 'rgba(255, 99, 132, 0.2)',
                            borderColor: 'rgba(255, 99, 132, 1)',
                            borderWidth: 1
                        },
                        {
                            label: 'Diterima',
                            data: receiveds,
                            backgroundColor: 'rgba(54, 162, 235, 0.2)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1
                        },
                        {
                            label: 'Realisasi',
                            data: useds,
                            backgroundColor: 'rgba(56, 255, 119, 0.2)',
                            borderColor: 'rgba(56, 255, 119, 1)',
                            borderWidth: 1
                        },
                    ]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Jumlah bantuan dana per tahun (dalam rupiah)'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                callback: function (value){
                                    return toRupiah(value);
                                },
                                max: upBorder
                            },
                        }]
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data){
                                return toRupiah(tooltipItem.yLabel);
                            },
                        }
                    },
                    annotation: {
                        annotations: [{
                            type: 'line',
                            mode: 'horizontal',
                            scaleID: 'y-axis-0',
                            value: budget,
                            borderColor: 'grey',
                            borderWidth: 1,
                            label: {
                                backgroundColor: 'grey',
                                content: formatter.format(budget),
                                enabled: true
                            },
                        }],
                        drawTime: "afterDraw" // (default)
                    }
                }
            });
        })
    })
</script>