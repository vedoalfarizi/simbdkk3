<script>
    let url2 = "{{route('chart.funding-proposal-type')}}";
    let years2 = [];
    let attends = [];
    let manages = [];

    $(document).ready(function(){
        $.get(url2, function (response) {
            response.forEach(function (data) {
                years2.push(data.year);
                attends.push(data.attend);
                manages.push(data.manage);
            });

            let ctx = document.getElementById('fundProposalType').getContext('2d');
            let fundProposalType = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: years2,
                    datasets: [
                        {
                            label: 'Mengikuti',
                            data: attends,
                            backgroundColor: 'rgba(255, 99, 132, 0.2)',
                            borderColor: 'rgba(255, 99, 132, 1)',
                            borderWidth: 1
                        },
                        {
                            label: 'Melaksanakan',
                            data: manages,
                            backgroundColor: 'rgba(54, 162, 235, 0.2)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1
                        },
                    ]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Jumlah proposal berdasarkan jenis per tahun'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                precision: 0
                            }
                        }]
                    },
                }
            });
        })
    })
</script>