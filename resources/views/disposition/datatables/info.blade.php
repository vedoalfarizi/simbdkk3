<button style="text-align: left" type="button" class="btn btn-sm btn-link" data-toggle="modal" data-target="#info-{{$row->id}}">
    <i class="fa fa-info-circle"></i> {{$row->title}}
</button>

<div class="modal fade" id="info-{{$row->id}}">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Deskripsi singkat kegiatan</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                {!! $row->desc !!}
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
