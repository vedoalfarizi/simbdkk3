<div class="btn-group-vertical" role="group" aria-label="Vertical button group">
    @if($row->organization_id)
        <a target="_blank" href="{!! url('storage/'.$row->committee_url) !!}" class="btn btn-info"><small>Struktur Kepanitiaan</small></a>
    @else
        <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#team-{{$row->id}}">
            Pengusul
        </button>
    @endif
    <a target="_blank" href="{!! url('storage/'.$row->cover_letter_url) !!}" class="btn btn-info"><small>Surat Pengantar</small></a>
    <a target="_blank" href="{!! url('storage/'.$row->proposal_url) !!}" class="btn btn-info"><small>Proposal</small></a>

    @if($row->dispositions->count() == 0)
        <button type="button" class="btn btn-behance" data-toggle="modal" data-target="#disposition-{{$row->id}}">
            Disposisi
        </button>
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#revision-{{$row->id}}">
            Revisi
        </button>
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#declined-{{$row->id}}">
            Tolak
        </button>

            <div class="modal fade" id="disposition-{{$row->id}}">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        {!! Form::open(['route' => 'disposition.store', 'method' => 'post']) !!}
                        <div class="modal-header">
                            <h4 class="modal-title">Disposisi proposal</h4>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>
                        <div class="modal-body">
                            {!! Form::hidden('proposal_id', $row->id) !!}

                            @if(!$row->organization_id)
                                <b>{!! Form::label('budget', 'Rincian permohonan anggaran kebutuhan dana') !!}</b>
                                <div class="bd-example m-0">
                                    <dl class="row small m-0">
                                        <dd class="col-sm-12">
                                            <dl class="row">
                                                @foreach($row->budgets as $budget)
                                                    <dd class="col-sm-4"><i>{!! config('value.budget.'.$budget->type) !!}</i></dd>
                                                    <dd class="col-sm-8">{!! $budget->amount_rupiah !!}</dd>
                                                @endforeach
                                            </dl>
                                        </dd>
                                    </dl>
                                </div>
                            @endif

                            <b>{!! Form::label('message', 'Besaran bantuan dana') !!}</b>
                            <div class="controls">
                                <div class="input-prepend input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp</span>
                                    </div>
                                    {!! Form::text('funds', $row->funds_needed, ['class' => 'form-control', 'autocomplete' => 'funds']) !!}
                                </div>
                            </div>
                            <br>
                            <b>{!! Form::label('dispositionTo', 'Disposisi kepada') !!}</b>
                            {!! Form::select('dispositionTo', config('value.disposition'), null, ['class' => 'form-control', 'autocomplete' => 'dispositionTo']) !!}
                            <br>
                            <b>{!! Form::label('message', 'Pesan') !!}</b>
                            {!! Form::select('templateMsg', config('value.dispositionMessageTemplate'), null, ['class' => 'form-control', 'autocomplete' => 'templateMsg']) !!}
                            {!! Form::textarea('message', null, ['id'=> "message-{{$row->id}}", 'class' => 'form-control mb-4', 'placeholder' => 'Isi pesan (opsional)', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
                        </div>
                        <div class="modal-footer">
                            <a target="_blank" class="btn btn-success" href="{{route('dekan.user.history.index', $row->id)}}">Lihat riwayat pengusul</a>
                            {{ Form::button('Kirim', ['class' => 'btn btn-success', 'type' => 'submit', 'name' => 'dispositionBtn', 'value' => 'disposition']) }}
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
    @else
        <button type="button" class="btn btn-behance" data-toggle="modal" data-target="#reDisposition-{{$row->id}}">
            Disposisikan kembali
        </button>

        <div class="modal fade" id="reDisposition-{{$row->id}}">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        {{--{!! Form::model($row->dispositions[0], ['route' => 'dekan.reDisposition', 'method' => 'put']) !!}--}}
                        {!! Form::open(['route' => 'dekan.reDisposition', 'method' => 'put']) !!}
                        <div class="modal-header">
                            <h4 class="modal-title">Disposisi proposal</h4>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>
                        <div class="modal-body">
                            {!! Form::hidden('proposal_id', $row->id) !!}

                            <b>Catatan Wakil Dekan II</b>
                            {!! Form::textarea('note', $row->dispositions[0]->message, ['id'=> "note-{{$row->id}}", 'class' => 'form-control mb-4', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none', 'disabled']) !!}

                            <b>{!! Form::label('budget', 'Rincian permohonan anggaran kebutuhan dana') !!}</b>
                            @if(!$row->organization_id)
                                <div class="bd-example m-0">
                                    <dl class="row small m-0">
                                        <dd class="col-sm-12">
                                            <dl class="row">
                                                @foreach($row->budgets as $budget)
                                                    <dd class="col-sm-4"><i>{!! config('value.budget.'.$budget->type) !!}</i></dd>
                                                    <dd class="col-sm-8">{!! $budget->amount_rupiah !!}</dd>
                                                @endforeach
                                            </dl>
                                        </dd>
                                    </dl>
                                </div>
                            @endif

                            <b>{!! Form::label('message', 'Besaran bantuan dana') !!}</b>
                            <div class="controls">
                                <div class="input-prepend input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp</span>
                                    </div>
                                    {!! Form::text('funds', $row->funds_receive, ['class' => 'form-control', 'autocomplete' => 'funds']) !!}
                                </div>
                            </div>
                            <br>
                            <b>{!! Form::label('dispositionTo', 'Disposisi kepada') !!}</b>
                            {!! Form::select('dispositionTo', config('value.disposition'), null, ['class' => 'form-control', 'autocomplete' => 'dispositionTo', 'disabled']) !!}
                            <br>
                            <b>{!! Form::label('message', 'Pesan') !!}</b>
                            {!! Form::select('templateMsg', config('value.dispositionMessageTemplate'), null, ['class' => 'form-control', 'autocomplete' => 'templateMsg']) !!}
                            {!! Form::textarea('message', null, ['id'=> "message-{{$row->id}}", 'class' => 'form-control mb-4', 'placeholder' => 'Isi pesan (opsional)', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
                        </div>
                        <div class="modal-footer">
                            <a target="_blank" class="btn btn-success" href="{{route('dekan.user.history.index', $row->id)}}">Lihat riwayat pengusul</a>
                            {{ Form::button('Kirim', ['class' => 'btn btn-success', 'type' => 'submit']) }}
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
    @endif
</div>

{{--modal team--}}
<div class="modal fade" id="team-{{$row->id}}">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data pengusul</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="col">
                    <div class="form-group row m-0">
                        <label class="col-md-4 col-form-label"><strong>NIM</strong></label>
                        <label class="col-md-6 col-form-label"><strong>Nama</strong></label>
                    </div>
                    @foreach($row->teams as $team)
                        <div class="form-group row m-0">
                            <label class="col-md-4 col-form-label">{!! $team->user_id !!}</label>
                            <label class="col-md-6 col-form-label">{!! $team->user->name !!}</label>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

{{--modal revision--}}
<div class="modal fade" id="revision-{{$row->id}}">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            {!! Form::open(['route' => 'disposition.store', 'method' => 'post']) !!}
            <div class="modal-header">
                <h4 class="modal-title">Revisi proposal</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                {!! Form::hidden('proposal_id', $row->id) !!}

                {!! Form::label('message', 'Catatan revisi') !!}
                {!! Form::textarea('message', null, ['id'=> "revision-{{$row->id}}", 'class' => 'form-control', 'placeholder' => 'Tuliskan catatan revisi', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
            </div>
            <div class="modal-footer">
                {{ Form::button('Kirim', ['class' => 'btn btn-success', 'type' => 'submit', 'name' => 'dispositionBtn', 'value' => 'revision']) }}
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--modal revision--}}
<div class="modal fade" id="declined-{{$row->id}}">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            {!! Form::open(['route' => 'disposition.store', 'method' => 'post']) !!}
            <div class="modal-header">
                <h4 class="modal-title">Tolak proposal</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                {!! Form::hidden('proposal_id', $row->id) !!}

                {!! Form::label('message', 'Alasan ditolak') !!}
                {!! Form::textarea('message', null, ['id'=> "reason-{{$row->id}}", 'class' => 'form-control', 'placeholder' => 'Tuliskan alasan ditolak', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}

            </div>
            <div class="modal-footer">
                {{ Form::button('Kirim', ['class' => 'btn btn-success', 'type' => 'submit', 'name' => 'dispositionBtn', 'value' => 'declined']) }}
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
