@extends('layouts.dashboard')

@section('breadcrumb')
    @role('Dekan|Kasubag|Kabag|Wadek II')
        {{ Breadcrumbs::render('disposition.history.index') }}
    @else
        {{ Breadcrumbs::render('disposition.index') }}
    @endrole
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            @role('Dekan|Kabag|Wadek II')
                <strong>Riwayat Disposisi</strong>
                <div class="card-header-actions">
                    <a class="btn btn-primary" href="{{route('disposition.index')}}">
                        Kembali
                    </a>
                </div>
            @else
                <strong>Disposisi oleh Dekan</strong>
            @endrole
        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover table-bordered data-table" style="border-collapse: collapse !important">
                <thead class="thead-dark small text-center">
                <tr>
                    <th width="5px">No</th>
                    <th width="col">Kode</th>
                    <th width="col">Kegiatan</th>
                    <th width="col">Pengusul</th>
                    <th scope="col">Isi disposisi</th>
                    <th scope="col">Jumlah bantuan</th>
                    <th scope="col">Disposisi pada</th>
                    <th width="10px"></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        $(function () {

            let table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('disposition.history.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'proposal_id', name: 'proposal_id'},
                    {data: 'name', name: 'name'},
                    {data: 'teams', name: 'teams'},
                    {data: 'message', name: 'message'},
                    {data: 'funded', name: 'funded'},
                    {data: 'disposition_at', name: 'disposition_at'},
                    {data: 'action', name: 'action', searchable: false, orderable: false},
                ]
            });

        });
    </script>
@endsection