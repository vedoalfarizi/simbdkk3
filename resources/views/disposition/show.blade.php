@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('disposition.show', $proposal_id) }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>Detail disposisi</strong>
        </div>
        <div class="card-body">
            <table class="table table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                <tr>
                    <th>Pengirim</th>
                    <th>Penerima</th>
                    <th>Isi</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dispositions as $disposition)
                <tr>
                    <td>
                        <div>{{$disposition->dispositionBy->name}}</div>
                        <div class="small text-muted">
                            sejak {{$disposition->created_at->diffForHumans()}}</div>
                    </td>
                    <td>
                        <div>{{$disposition->dispositionTo->name}}</div>
                    </td>
                    <td>
                        @if($loop->index == 0)
                            <small class="text-muted">Dibantu sebesar Rp{{number_format($disposition->proposal->funds_receive, 2, ',', '.')}}</small>
                        @endif
                        <small class="text-muted">{!! $disposition->message !!}</small> <br>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @role('Wadek II|Kabag')
    <div class="card">
        {!! Form::open(['route' => 'disposition.store', 'method' => 'post']) !!}
        <div class="card-header">
            <strong>Disposisi proposal</strong>
        </div>

        <div class="card-body">
            {!! Form::hidden('proposal_id', $proposal_id) !!}

            {!! Form::label('message', 'Pesan') !!}
            {!! Form::select('templateMsg', config('value.dispositionMessageTemplate'), null, ['class' => 'form-control', 'autocomplete' => 'templateMsg']) !!}
            {!! Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Isi disposisi..', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
        </div>

        <div class="card-footer">
            {{ Form::button('Disposisi', ['class' => 'btn btn-success', 'type' => 'submit', 'name' => 'dispositionBtn', 'value' => 'disposition']) }}
            <a href="{{route('disposition.index')}}" class="btn btn-danger">Kembali</a>
            @role('Wadek II')
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#back-{{$proposal_id}}">
                Kembalikan
            </button>
            @endrole
        </div>
        {!! Form::close() !!}
    </div>
    @endrole

    @role('Keuangan')
    <div class="card">
        {!! Form::open(['route' => 'disposition.store', 'method' => 'post']) !!}
        <div class="card-header">
            <strong>Konfirmasi proses berkas ?</strong>
        </div>
        <div class="card-footer">
            {!! Form::hidden('proposal_id', $proposal_id) !!}

            {{ Form::button('Ya', ['class' => 'btn btn-success', 'type' => 'submit', 'name' => 'dispositionBtn', 'value' => 'disposition']) }}
            <a href="{{route('disposition.index')}}" class="btn btn-danger">Tidak</a>
        </div>
        {!! Form::close() !!}
    </div>
    @endrole

    @role('Wadek II')
    {{--modal kembalikan--}}
    <div class="modal fade" id="back-{{$proposal_id}}">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                {!! Form::open(['route' => 'disposition.store', 'method' => 'post']) !!}
                <div class="modal-header">
                    <h4 class="modal-title">Kembalikan disposisi</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('proposal_id', $proposal_id) !!}

                    {!! Form::label('message', 'Alasan pengembalian') !!}
                    {!! Form::textarea('message', null, ['id'=> "back-{{$proposal_id}}", 'class' => 'form-control', 'placeholder' => 'Mulai diskusi', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
                </div>
                <div class="modal-footer">
                    {{ Form::button('Kembalikan', ['class' => 'btn btn-success', 'type' => 'submit', 'name' => 'dispositionBtn', 'value' => 'revision']) }}
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @endrole

@endsection

@section('js-script')
    <script type="text/javascript">
        CKEDITOR.replace('content');
    </script>
@endsection
