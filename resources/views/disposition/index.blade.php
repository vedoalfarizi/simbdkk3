@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('disposition.index') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>Data proposal</strong>
            @unlessrole('Kasubag|Keuangan')
            <div class="card-header-actions">
                <a class="btn btn-primary" href="{{route('disposition.history.index')}}">
                    Riwayat
                </a>
            </div>
            @endunlessrole
        </div>
        <div class="card-body table-responsive ">
            <table class="table table-hover table-bordered data-table" style="border-collapse: collapse !important">
                <thead class="thead-dark small text-center">
                <tr>
                    @role('Dekan')
                        @include('disposition.thead.dekan')
                    @endrole
                    @role('Wadek II|Kabag|Kasubag|Keuangan')
                        @include('disposition.thead.kasubag')
                    @endrole
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')
    @role('Dekan')
        @include('disposition.datatables-js.dekan')
    @endrole
    @role('Wadek II|Kabag|Kasubag|Keuangan')
    @include('disposition.datatables-js.kasubag')
    @endrole
@endsection
