<script type="text/javascript">
$(function () {

    const table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('disposition.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'proposal_id', name: 'proposal_id'},
            {data: 'title', name: 'title'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', searchable: false, orderable: false},
        ]
    });

});
</script>