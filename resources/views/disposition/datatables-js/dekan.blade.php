<script type="text/javascript">
$(function () {

    let table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('disposition.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'id', name: 'id'},
            {data: 'event', name: 'event'},
            {data: 'level', name: 'level'},
            {data: 'info', name: 'info'},
            {data: 'duration', name: 'duration'},
            {data: 'funds_needed', name: 'funds_needed'},
            {data: 'deadline', name: 'deadline'},
            {data: 'action', name: 'action', searchable: false, orderable: false},
        ]
    });

});
</script>