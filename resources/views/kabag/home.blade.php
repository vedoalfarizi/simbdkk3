@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('kabag.home') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body p-0 d-flex align-items-center">
                    <i class="fa fa-envelope-o bg-primary p-4 font-2xl mr-3"></i>
                    <div>
                        <div class="text-value-sm text-primary">{{$lastMailNo->id ?? '-'}}</div>
                        <div class="text-muted text-uppercase font-weight-bold small">NO SURAT TERAKHIR</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-5">
            <div class="card text-white bg-primary">
                <div class="card-header text-center">DISPOSISI PROPOSAL</div>
                <div class="card-body row text-center">
                    <div class="col">
                        <div class="text-value-xl">{{ $dispositions->total}}</div>
                        <div class="text-uppercase text-muted small">Semua</div>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $dispositions->request}}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('disposition.index')}}">
                            <div class="text-uppercase text-muted small">Permintaan</div>
                        </a>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $dispositions->done }}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('disposition.history.index')}}">
                            <div class="text-uppercase text-muted small">Selesai</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-7">
            <div class="card text-white bg-primary">
                <div class="card-header text-center">SURAT</div>
                <div class="card-body row text-center">
                    <div class="col">
                        <div class="text-value-xl">{{ $total  }}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('mail.history.index')}}">
                            <div class="text-uppercase text-muted small">Total</div>
                        </a>
                    </div>
                    @foreach($mails as $mail)
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $mail->total }}</div>
                        <div class="text-uppercase text-muted small">{{ $mail->type->title }}</div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection