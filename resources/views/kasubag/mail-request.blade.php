@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('kasubag.mail.request') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>Data permintaan surat keterangan dibantu fakultas</strong>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover table-bordered data-table" style="border-collapse: collapse !important">
                <thead class="thead-dark small text-center">
                <tr>
                    <th width="5px">No</th>
                    <th scope="col">Kode proposal</th>
                    <th scope="col">Kegiatan</th>
                    <th width="10px"></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')

@endsection