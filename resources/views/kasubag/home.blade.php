@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('kabag.home') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-5">
            <div class="card text-white bg-primary">
                @role('Kasubag')
                    <div class="card-header text-center">DISPOSISI PROPOSAL</div>
                @else
                    <div class="card-header text-center">PERMINTAAN BERKAS</div>
                @endrole
                <div class="card-body row text-center">
                    <div class="col">
                        <div class="text-value-xl">{{ $dispositions->total}}</div>
                        <div class="text-uppercase text-muted small">Semua</div>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $dispositions->request}}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('disposition.index')}}">
                            <div class="text-uppercase text-muted small">Permintaan</div>
                        </a>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $dispositions->done }}</div>
                        <div class="text-uppercase text-muted small">Selesai</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="card text-white bg-primary">
                <div class="card-header text-center">LPJ</div>
                <div class="card-body row text-center">
                    <div class="col">
                        <div class="text-value-xl">{{ $lpj->request }}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('kasubag.lpj.index')}}">
                            <div class="text-uppercase text-muted small">Permintaan</div>
                        </a>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $lpj->revision }}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('kasubag.lpj.index', ['status' => 'revision'])}}">
                            <div class="text-uppercase text-muted small">Revisi</div>
                        </a>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col-4">
                        <div class="text-value-xl">{{ $lpj->waiting}}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('kasubag.lpj.index')}}">
                            <div class="text-uppercase text-muted small">Menunggu berkas</div>
                        </a>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $lpj->done}}</div>
                        <a style="text-decoration: none" class="btn-link" href="{{route('kasubag.lpj.index', ['status' => 'done'])}}">
                            <div class="text-uppercase text-muted small">Selesai</div>
                        </a>
                    </div>
                    <div class="c-vr"></div>
                    <div class="col">
                        <div class="text-value-xl">{{ $lpj->total}}</div>
                        <div class="text-uppercase text-muted small">Semua</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
