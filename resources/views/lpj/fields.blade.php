<div class="row">
    <div class="col-sm-6">
        {!! Form::hidden('proposal_id', $proposal->id) !!}
        <div class="form-group">
            {!! Form::label('realisation', 'Dana yang digunakan', ['class' => 'col-form-label']) !!}
            <div class="controls">
                <div class="input-prepend input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Rp</span>
                    </div>
                    {!! Form::text('realisation', $proposal->funds_realisation == 0 ?
                        $proposal->funds_receive :
                        $proposal->funds_realisation,
                        ['id' => 'realisation', 'class' => 'form-control '.($errors->has('realisation') ? 'is-invalid' : ''), 'autocomplete' => 'realisation']) !!}
                    @error('realisation')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('lpj', 'Laporan Pertanggungjawaban') !!} <br>
            {!! Form::file('lpj', null, ['id' => 'lpj', 'class' => 'form-control-file is-invalid', 'autocomplete' => 'lpj']) !!}
            <br> <small class="text-info">*hanya format PDF, max: 5 MB</small>
            @if($proposal->LPJ_url)
                <br> <small class="text-info"><a target="_blank" href="{!! url('storage/'.$proposal->LPJ_url) !!}">Lihat LPJ</a></small>
            @endif
            @error('lpj')
            <br> <small class="text-danger">{{$message}}</small>
            @enderror
        </div>
        <div class="form-group">
            {!! Form::label('receipt', 'Scan kwitansi') !!} <br>
            {!! Form::file('receipt', null, ['id' => 'receipt', 'class' => 'form-control-file is-invalid', 'autocomplete' => 'receipt']) !!}
            <br> <small class="text-info">*hanya format PDF, max: 5 MB</small>
            @if($proposal->receipt_url)
                <br> <small class="text-info"><a target="_blank" href="{!! url('storage/'.$proposal->receipt_url) !!}">Lihat kwitansi</a></small>
            @endif
            @error('receipt')
            <br> <small class="text-danger">{{$message}}</small>
            @enderror
        </div>
    </div>
</div>