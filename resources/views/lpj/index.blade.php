@extends('layouts.dashboard')

@section('breadcrumb')
    @if(Request::get('status'))
        {{ Breadcrumbs::render('kasubag.lpj.revision') }}
    @else
        {{ Breadcrumbs::render('kasubag.lpj.index') }}
    @endif
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <strong>
                Data
                @if(Request::get('status'))
                    {{ Request::get('status') === 'revision' ? ' revisi ' : ' riwayat ' }}
                @endif
                LPJ
            </strong>
            <div class="card-header-actions">
                @if(!Request::get('status'))
                <a class="btn btn-primary" href="{{ route('kasubag.lpj.index', ['status' => 'revision']) }}">
                    Menunggu revisi
                </a>
                <a class="btn btn-primary" href="{{ route('kasubag.lpj.index', ['status' => 'done']) }}">
                    Riwayat
                </a>
                @else
                    <a class="btn btn-primary" href="{{ route('kasubag.lpj.index')}}">
                        Kembali
                    </a>
                @endif
            </div>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover table-sm table-bordered data-table" style="border-collapse: collapse !important">
                <thead class="thead-dark text-center small">
                <tr>
                    <th width="col">No</th>
                    <th scope="col">Kode</th>
                    <th scope="col">Judul</th>
                    <th scope="col">Pengunaan dana</th>
                    <th scope="col">Berkas</th>
                    @if(!Request::get('status') && Auth::user()->hasRole('Keuangan'))
                        <th width="col">Aksi</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($lpjs as $lpj)
                        <tr>
                            <td>{!! $no++ !!}</td>
                            <td>
                                <p style="text-align: left">
                                    {{$lpj->id}}
                                    @if(Request::get('status') === 'revision')
                                        <br>
                                        <button type="button" class="btn badge badge-danger" data-toggle="modal" data-target="#revision-{{ $lpj->id }}">
                                            Revisi
                                        </button>
                                    @endif
                                </p>
                            </td>
                            <td>
                                <p> {{$lpj->titleWithStatus}}</p>
                            </td>
                            <td>
                                {{$lpj->funds_realisation_rupiah}}
                            </td>
                            <td align="right">
                                <a target="_blank" href="{!! url('storage/'.$lpj->proposal_url) !!}" class="btn btn-sm btn-outline-primary"><i class="fa fa-file-pdf-o"></i> Proposal</a>
                                <a target="_blank" href="{!! url('storage/'.$lpj->LPJ_url) !!}" class="btn btn-sm btn-outline-primary"><i class="fa fa-file-pdf-o"></i> LPJ</a>
                                <a target="_blank" href="{!! url('storage/'.$lpj->receipt_url) !!}" class="btn btn-sm btn-outline-primary"><i class="fa fa-file-pdf-o"></i> Bukti transaksi</a>
                                <a @if(Request::get('status')) target="_blank" @endif href="{{route('documentation.index', $lpj->id)}}" class="btn btn-sm btn-outline-primary"><i class="fa fa-eye"></i> Dokumentasi</a>
                                @if(!$lpj->organization_id)
                                    <a @if(Request::get('status')) target="_blank" @endif href="{{route('achievement.index', $lpj->id)}}" class="btn btn-sm btn-outline-primary"><i class="fa fa-eye"></i> Penghargaan</a>
                                @endif
                            </td>

                            @if(!Request::get('status') && Auth::user()->hasRole('Keuangan'))
                                <td>
                                    @if($lpj->status === config('value.progress.lpj-submitted'))
                                    {!! Form::open(['route' => ['kasubag.lpj.confirm', $lpj->id], 'method' => 'post']) !!}
                                        {{ Form::button('Konfirmasi', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
                                    {!! Form::close() !!}
                                    <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#lpj-{{$lpj->id}}">
                                        Revisi
                                    </button>
                                    @elseif($lpj->status === config('value.progress.lpj-verified'))
                                        {!! Form::open(['route' => ['kasubag.lpj.confirm-complete', $lpj->id], 'method' => 'put']) !!}
                                            {{ Form::button('Terima berkas', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
                                        {!! Form::close() !!}
                                    @endif
                                </td>

                                {{--modal lpj--}}
                                <div class="modal fade" id="lpj-{{$lpj->id}}">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            {!! Form::open(['route' => ['kasubag.lpj.revision', $lpj->id], 'method' => 'post']) !!}

                                            <div class="modal-header">
                                                <h4 class="modal-title">Revisi LPJ</h4>
                                                <button type="button" class="close" data-dismiss="modal">×</button>
                                            </div>

                                            <div class="modal-body">
                                                {!! Form::label('message', 'Perbaikan') !!}
                                                {!! Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Isi perbaikan..', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none']) !!}
                                            </div>

                                            <div class="modal-footer">
                                                {{ Form::button('<i class="fa fa-check"></i> Kirim', ['class' => 'btn btn-sm btn-success', 'type' => 'submit']) }}
                                                <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
                                            </div>

                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            @elseif(Request::get('status') === 'revision')
                                <div class="modal fade" id="revision-{{$lpj->id}}">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Catatan revisi</h4>
                                                <button type="button" class="close" data-dismiss="modal">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <p>{!! $lpj->revision !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
