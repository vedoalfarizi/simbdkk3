@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mahasiswa.lpj.create', $proposal->id) }}
@endsection

@section('content')
    <div class="card">
        {!! Form::open(['route' => 'mahasiswa.lpj.store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        <div class="card-header">
            <strong>Form laporan pertanggungjawaban kegiatan</strong>
            <div class="card-header-actions">
                <a class="card-header-action btn-minimize" href="#" data-toggle="collapse" data-target="#join" aria-expanded="true">
                    <i class="icon-arrow-up"></i>
                </a>
            </div>
        </div>
        <div class="collapse show" id="join" style="">
            <div class="card-body">
                @include('lpj.fields')
            </div>
            <div class="card-header">
                <strong>Dokumentasi kegiatan</strong> (<small class="text-info">*Pastikan setiap file berformat PNG/JPG dan berukuran maks 5 MB</small>)
            </div>
            <div class="card-body">
                @include('documentation.fields')
            </div>
            @if(!$proposal->organization_id)
            <div class="card-header">
                <strong>Penghargaan</strong> (<small class="text-info">*Pastikan setiap file berformat PDF atau PNG/JPG dan berukuran maks 2 MB</small>)
            </div>
            <div class="card-body">
                @include('achievement.fields')
            </div>
            @endif
            <div class="card-footer">
                {{ Form::button('<i class="fa fa-check"></i> Kirim', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
                <a class="btn btn-sm btn-danger" href="{{route('mahasiswa.proposal.index')}}"><i class="fa fa-rotate-left"></i> Batal</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        $(document).ready(function() {
            // photos
            $(".add-photo").click(function(){
                let html = $(".copy").html();
                $(".after-add-photo").after(html);
            });
            $("body").on("click",".remove",function(){
                $(this).parents(".control-group-photo").remove();
            });

            // achievements
            $(".addMore").click(function(){
                let fieldHTML = '<div class="form-group achievement">'+$(".achievementCopy").html()+'</div>';
                $('body').find('.achievement:last').after(fieldHTML);
            });
            $("body").on("click",".remove",function(){
                $(this).parents(".achievement").remove();
            });
        });
    </script>
@endsection