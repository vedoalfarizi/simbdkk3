@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mahasiswa.lpj.edit', $proposal) }}
@endsection

@section('content')
    <div class="card">
        {!! Form::model($proposal, ['route' => ['mahasiswa.lpj.update', $proposal->id], 'method' => 'put', 'enctype' => 'multipart/form-data']) !!}
        <div class="card-header">
            <strong>Form ubah data lpj</strong>
        </div>
        <div class="card-body">
            @include('lpj.fields')
        </div>
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            <a class="btn btn-sm btn-danger" href="{{route('mahasiswa.proposal.index')}}"><i class="fa fa-rotate-left"></i> Batal</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        CKEDITOR.replace('desc');
    </script>
@endsection