{{--Only use for create proposal--}}
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {!! Form::label('nim', 'NIM', ['class' => 'col-md-4']) !!}
            <div class="col-md-6">
                <select id='nim' name="nim[]" class="form-control" multiple style="min-width: 600px"></select>
            </div>
        </div>
    </div>
</div>