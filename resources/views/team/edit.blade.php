@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mahasiswa.proposal.edit.team', $proposal_id) }}
@endsection

@section('content')
    <div class="card">
        {!! Form::open(['route' => ['mahasiswa.proposal.update.team', $proposal_id], 'method' => 'put']) !!}
        <div class="card-header">
            <strong>Form ubah data pengusul</strong>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('nim', 'NIM', ['class' => 'col-md-4']) !!}
                        <div class="col-md-6">
                            <select id='nim' name="nim[]" class="form-control" multiple style="min-width: 600px">
                                @foreach($team as $member)
                                    <option selected value={{$member->user_id}}> {{$member->user_id}} - {{$member->user->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
            <a class="btn btn-sm btn-danger" href="{{route('mahasiswa.proposal.index')}}"><i class="fa fa-ban"></i> Batal</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('js-script')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#nim').select2({
                minimumInputLength: 3,
                placeholder: 'Tuiskan NIM atau nama...',
                ajax: {
                    url: "{!! url('/users-json') !!}",
                    dataType: 'json',
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.id + ' - ' + item.name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    cache: true
                }
            });
        });
    </script>
@endsection