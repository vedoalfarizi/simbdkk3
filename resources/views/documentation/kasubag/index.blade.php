@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('documentation.kasubag.index') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row col-12">
                @foreach($documentations as $documentation)
                <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3 img-button">
                    <a class="fancybox" rel="lightbox" href="{!! url('storage/'.$documentation->photo_url) !!}">
                        <img src="{!! url('storage/'.$documentation->photo_url) !!}" alt="documentation image" class="img-thumbnail">
                    </a>
                    <p><small>{!! $documentation->desc !!}</small></p>
                </div>
                @endforeach
            </div>
        </div>
        <div class="card-footer">
            <a class="btn btn-sm btn-info" href="{{route('kasubag.lpj.index')}}"><i class="fa fa-rotate-left"></i> Kembali</a>
        </div>
    </div>
@endsection

@section('js-script')
    <script>
        $(document).ready(function(){
            $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none"
            });
        });
    </script>
@endsection