@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('mahasiswa.documentation.create', $proposalId) }}
@endsection

@section('content')
    <div class="card">
        {!! Form::open(['route' => ['mahasiswa.documentation.store', $proposalId], 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        <div class="card-header">
            <strong>Tambah dokumentasi kegiatan</strong>
        </div>
        <div class="collapse show" id="join" style="">
            <div class="card-header">
                <strong><small class="text-info">*Pastikan setiap file berformat PNG/JPG dan berukuran maks 5 MB</small></strong>
            </div>
            <div class="card-body">
                @include('documentation.fields')
            </div>
            <div class="card-footer">
                {{ Form::button('<i class="fa fa-check"></i> Simpan', ['class' => 'btn btn-sm btn-primary', 'type' => 'submit']) }}
                <a class="btn btn-sm btn-danger" href="{{route('documentation.index', $proposalId)}}"><i class="fa fa-rotate-left"></i> Batal</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('js-script')
    <script type="text/javascript">
        $(document).ready(function() {
            // photos
            $(".add-photo").click(function(){
                let html = $(".copy").html();
                $(".after-add-photo").after(html);
            });
            $("body").on("click",".remove",function(){
                $(this).parents(".control-group-photo").remove();
            });
        });
    </script>
@endsection