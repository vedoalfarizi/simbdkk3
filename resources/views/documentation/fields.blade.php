<div class="row">
    <div class="col-sm-12">
        @error('photo')
            <small class="text-danger">{{$message}}</small>
        @enderror
        @error('desc.*')
            <br> <small class="text-danger">{{$message}}</small>
        @enderror
        @error('achievement.*')
        <br> <small class="text-danger">{{$message}}</small>
        @enderror
        <div class="input-group control-group-photo after-add-photo">
            {!! Form::file('photo[]', null, ['id' => 'photo', 'class' => 'form-control-file is-invalid', 'autocomplete' => 'photo']) !!}
            {!! Form::text('desc[]', null, ['id' => 'desc', 'class' => 'form-control ', 'placeholder' => 'Deskripsi foto', 'autocomplete' => 'desc']) !!}
            <div class="input-group-btn">
                <button class="btn btn-success add-photo" type="button"><i class="fa fa-plus"></i></button>
            </div>
        </div>

        <div class="copy" style="display: none">
            <div class="control-group-photo input-group" style="margin-top:10px">
                {!! Form::file('photo[]', null, ['id' => 'photo', 'class' => 'form-control-file is-invalid', 'autocomplete' => 'photo']) !!}
                {!! Form::text('desc[]', null, ['id' => 'desc', 'class' => 'form-control ', 'placeholder' => 'Deskripsi foto', 'autocomplete' => 'desc']) !!}
                <div class="input-group-btn">
                    <button class="btn btn-danger remove" type="button"><i class="fa fa-minus"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>