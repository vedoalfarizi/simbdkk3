@extends('layouts.base')

@section('body')
    <body class="app flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card-group">
                    <div class="card p-4">
                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                            <h1>Login</h1>
                            <p class="text-muted">Masuk ke akun Anda</p>

                            @error('email')
                                <div class="input-group">
                                    <small class="text-danger">
                                    <div class="error">{{ $message }}</div>
                                    </small>
                                </div>
                            @enderror

                            @error('id')
                            <div class="input-group">
                                <small class="text-danger">
                                    <div class="error">{{ $message }}</div>
                                </small>
                            </div>
                            @enderror

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="icon-user"></i>
                                    </span>
                                </div>
                                <input id="identity" class="form-control @if($errors->has('email') || $errors->has('id')) is-invalid @endif" type="text" placeholder="NIM/ NIP/ Email" name="identity" value="{{ old('identity') }}" required autocomplete="identity" autofocus>
                            </div>

                            @error('password')
                            <div class="input-group">
                                <small class="text-danger">
                                    <div class="error">{{ $message }}</div>
                                </small>
                            </div>
                            @enderror

                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="icon-lock"></i>
                                    </span>
                                </div>
                                <input id="password" class="form-control @error('password') is-invalid @enderror" type="password" placeholder="Password" name="password" required autocomplete="current-password">
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button class="btn btn-primary px-4" type="submit">{{ __('Login') }}</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footer')

    </body>
@endsection
