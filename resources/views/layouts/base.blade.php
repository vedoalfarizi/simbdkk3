<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="SIMBAKWA - Sistem Informasi Manajemen Bantuan Dana Kegiatan Kemahasiswaan">
    <meta name="author" content="Vedo Alfarizi">
    <meta name="keyword" content="SIM,Unand,UKM">
    <title>{{ config('app.name', 'SIMBDKK') }}</title>
    <!-- Icons-->
    <link rel="icon" type="image/ico" href="{{asset('img/favicon.ico')}}" sizes="any" />

    <link rel="stylesheet" href="{{asset('css/coreui.min.css')}}">
    {{--<link rel="stylesheet" href="{{asset('css/icons/all.min.css')}}">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css">
    <!-- Main styles for this application-->
    {{--<link rel="stylesheet" href="{{asset('css/style.min.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/pace/pace-theme-minimal.min.css')}}">
    <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
</head>

@yield('body')

</html>

