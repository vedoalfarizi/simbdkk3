<li class="nav-item">
    <a class="nav-link" href="{!! route('kabag.home') !!}">
        <i class="nav-icon icon-home"></i> Dashboard
    </a>
</li>


<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#">
        <i class="nav-icon icon-folder-alt"></i> Arsip</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item">
            <a class="nav-link" href="{{route('proposal.history.index')}}">
                <i class="nav-icon icon-docs"></i> Proposal
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href={{route('mail.history.index')}}>
                <i class="nav-icon icon-envelope"></i> Surat
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('user-achievements')}}">
                <i class="nav-icon icon-trophy"></i> Prestasi
            </a>
        </li>
    </ul>
</li>