<li class="nav-item">
    <a class="nav-link" href="{!! route('dekan.home') !!}">
        <i class="nav-icon icon-home"></i> Dashboard
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{!! route('mail.index') !!}">
        <i class="nav-icon icon-docs"></i> Surat
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{route('proposal.history.index')}}">
        <i class="nav-icon icon-docs"></i> Arsip proposal
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{!! route('user-achievements') !!}">
        <i class="nav-icon icon-trophy"></i> Prestasi
    </a>
</li>