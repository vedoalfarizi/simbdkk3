<li class="nav-item">
    <a class="nav-link" href="{!! route('admin.home') !!}">
        <i class="nav-icon icon-home"></i> Dashboard
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{route('admin.user.index')}}">
        <i class="nav-icon icon-user"></i> Pengguna</a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{route('admin.department.index')}}">
        <i class="nav-icon icon-graduation"></i> Jurusan</a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{route('admin.organization.index')}}">
        <i class="nav-icon icon-organization"></i> Organisasi</a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{route('admin.events.index')}}">
        <i class="nav-icon icon-event"></i> Kegiatan</a>
</li>