<li class="nav-item">
    <a class="nav-link" href="{!! route('mahasiswa.home') !!}">
        <i class="nav-icon icon-home"></i> Dahsboard
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{!! route('mahasiswa.proposal.option') !!}">
        <i class="nav-icon icon-plus"></i> Buat pengajuan
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{!! route('mahasiswa.proposal.index') !!}">
        <i class="nav-icon icon-note"></i> Dalam proses
    </a>
</li>

{{--<li class="nav-item">--}}
    {{--<a class="nav-link" href="{!! route('mahasiswa.achievement-request') !!}">--}}
        {{--<i class="nav-icon icon-trophy"></i> Upload prestasi--}}
    {{--</a>--}}
{{--</li>--}}
