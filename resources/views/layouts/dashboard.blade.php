@extends('layouts.base')

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    @include('layouts.header')

    <div class="app-body">
        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                    @include('layouts.menu')
                </ul>
            </nav>
        </div>
        <main class="main">
            @yield('breadcrumb')

            <div class="container-fluid">
                <div class="animated fadeIn">

                    @include('layouts.flash-message')

                    @yield('content')

                </div>
            </div>
        </main>
    </div>

    <footer class="app-footer">
        <div>
            <a href="https://coreui.io">SIMBAKWA</a>
            <span>&copy; 2020 Universitas Andalas.</span>
        </div>
        <div class="ml-auto">
            <span>Powered by</span>
            <a href="https://coreui.io">CoreUI</a>
        </div>
    </footer>
    @include('layouts.footer')
</body>

@yield('js-script')