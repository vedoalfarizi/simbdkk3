@role('Super Admin')
    @include('layouts.menu.super-admin')
@endrole

@role('Mahasiswa')
    @include('layouts.menu.mahasiswa')
@endrole

@role('Dekan')
@include('layouts.menu.dekan')
@endrole

@role('Wadek II')
@include('layouts.menu.wadek2')
@endrole

@role('Wadek III')
@include('layouts.menu.wadek3')
@endrole

@role('Kabag')
@include('layouts.menu.kabag')
@endrole

@role('Kasubag|Keuangan')
@include('layouts.menu.kasubag')
@endrole

@role('Sekretaris Dekan')
@include('layouts.menu.sekdek')
@endrole

@role('Dekan|Wadek II|Kabag|Kasubag')
<li class="nav-item">
    <a class="nav-link" href="{!! route('disposition.index') !!}">
        <i class="nav-icon icon-paper-plane"></i> Disposisi proposal
    </a>
</li>
@endrole

@role('Keuangan')
<li class="nav-item">
    <a class="nav-link" href="{!! route('disposition.index') !!}">
        <i class="nav-icon icon-paper-plane"></i> Konfirmasi berkas
    </a>
</li>
@endrole


