<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="{!! asset('img/logo-unand.png') !!}" width="30px" height="40px" alt="CoreUI Logo">
        <img class="navbar-brand-minimized" src="{!! asset('img/logo-unand.png') !!}" width="10px" height="20px" alt="CoreUI Logo">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
            <a class="nav-link-primary">{{Auth::user()->name}} <b>|</b> {{Auth::user()->id}}</a>
        </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item d-md-down-none mr-3">
            <a class="nav-link" href="{{route('logout')}}"
               onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">
                <i class="icon-logout"></i><br>
                <small>Logout</small>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</header>