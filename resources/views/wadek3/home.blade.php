@extends('layouts.dashboard')

@section('breadcrumb')
    {{ Breadcrumbs::render('wadek3.home') }}
@endsection

@section('content')
    <div class="card">
        <div class="card-body table-responsive">
            <table class="table table-hover table-sm table-bordered data-table" style="border-collapse: collapse !important">
                <caption style="caption-side: top; text-align: center"><b>Jumlah prestasi berdasarkan tingkat kegiatan per tahun</b></caption>
                <thead class="thead-dark text-center small">
                <tr>
                    <th width="col">Tahun</th>
                    <th scope="col">Lokal</th>
                    <th scope="col">Kota</th>
                    <th scope="col">Provinsi</th>
                    <th scope="col">Wilayah</th>
                    <th width="col">Nasional</th>
                    <th width="col">Internasional</th>
                </tr>
                </thead>
                <tbody>
                @forelse($achievementLevels as $year => $level)
                    <tr align="center">
                        <td>{{$year}}</td>
                        <td>{{$level["Lokal"] ?? 0}}</td>
                        <td>{{$level["Kota"] ?? 0}}</td>
                        <td>{{$level["Provinsi"] ?? 0}}</td>
                        <td>{{$level["Wilayah"] ?? 0}}</td>
                        <td>{{$level["Nasional"] ?? 0}}</td>
                        <td>{{$level["Internasional"] ?? 0}}</td>
                    </tr>
                @empty
                    <tr align="center"><td colspan="7">Belum ada data</td></tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>

    <div class="card">
        <div class="card-body table-responsive">
            <table class="table table-hover table-sm table-bordered data-table" style="border-collapse: collapse !important">
                <caption style="caption-side: top; text-align: center"><b>Jumlah proposal berdasarkan jenis kegiatan per tahun</b></caption>
                <thead class="thead-dark text-center small">
                <tr>
                    <th width="col">Tahun</th>
                    <th scope="col">Kompetisi</th>
                    <th scope="col">Konferensi</th>
                    <th scope="col">Seminar</th>
                    <th scope="col">Pertukaran mahasiswa</th>
                    <th width="col">Pengabdian masyarakat (tanpa dosen)</th>
                </tr>
                </thead>
                <tbody>
                @forelse($proposalEvents as $event)
                    <tr align="center">
                        <td>{{$event->year}}</td>
                        <td>{{$event->competition}}</td>
                        <td>{{$event->conference}}</td>
                        <td>{{$event->seminar}}</td>
                        <td>{{$event->exchange}}</td>
                        <td>{{$event->dedication}}</td>
                    </tr>
                @empty
                    <tr align="center"><td colspan="6">Belum ada data</td></tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <canvas id="achievementDepartment" width="300" height="100"></canvas>
        </div>
    </div>

    <div class="card">
        <div class="card-body table-responsive">
            <table class="table table-hover table-sm table-bordered data-table" style="border-collapse: collapse !important">
                <caption style="caption-side: top; text-align: center"><b>Mahasiswa berprestasi tahun {{ now()->year }}</b></caption>
                <thead class="thead-dark text-center small">
                <tr>
                    <th width="col">No</th>
                    <th scope="col">NIM</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Poin</th>
                </tr>
                </thead>
                <tbody>
                @php $no = 1; @endphp
                @forelse($bestStudents as $student)
                    <tr>
                        <td>{{$no++}}</td>
                        <td>{{$student['nim']}}</td>
                        <td>{{$student['name']}}</td>
                        <td>{{$student['point']}}</td>
                    </tr>
                @empty
                    <tr align="center"><td colspan="3">Belum ada data</td></tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js-script')
    @include('chart.achievement-department')
@endsection